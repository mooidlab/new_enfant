<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directorio extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
        if(!is_logged()){
            $query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
            $redir = str_replace('/', '-', uri_string().$query);            
            // die(var_dump(is_logged()));
            redirect('admin/login/index/' . $redir);
        } // checamos si existe una sesión activa
		$this->load->helper(array('form', 'url'));
		$this->load->model('cliente_model');
		$this->load->model('file_model');
        $this->load->library('pagination');
    }
	
	function index() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/show_clients_view';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['pestana'] = 2;
		$data['clientes'] = $this->cliente_model->getClientesForAdmin();
		$this->load->view('admin/main_view',$data);
	}
	


	function agregar(){
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/new_client_view';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['pestana'] = 2;
		$data['categorias'] = $this->cliente_model->getCategorias();
		$this->load->view('admin/main_view',$data);
	}
    
    function ordenarcategoria(){
        $data = array();
        $data['SYS_metaTitle']          = 'Enfant';
        $data['SYS_metaKeyWords']       = 'Directorio Infantil, Infantes';
        $data['SYS_metaDescription']    = 'Directorio Infantil';
		$data['contador'] =  $this->cliente_model->getContador(); 
        $data['module'] = 'admin/ordenar_categoria_view';
        $data['pestana'] = 4;
        $data['categorias'] = $this -> cliente_model -> getCategorias();           
        $this->load->view('admin/main_view',$data);
    }
	

    function ordenar_do(){
        $data = array();
        $idCategoria = $this -> input -> get('categoria');
        $data['SYS_metaTitle']          = 'Enfant';
        $data['SYS_metaKeyWords']       = 'Directorio Infantil, Infantes';
        $data['SYS_metaDescription']    = 'Directorio Infantil';
		$data['contador'] =  $this->cliente_model->getContador(); 
        $data['module'] = 'admin/do_ordenar_categoria_view';
        $data['pestana'] = 4;
        $data['categorias'] = $this -> cliente_model -> getCategorias();
        $data['clientes'] = $this -> cliente_model -> getClientesCategoria($idCategoria,1000000, 0);                           
        $this->load->view('admin/main_view',$data);        
    }

    function do_ordenar(){
        $data = array();        
        $data['response'] = 'false';        
        if($this -> input -> post('action') == 'updateRecordsListings'):
            
            $recordsArray = array();
            $recordsArray = $this -> input -> post('recordsArray');
            
            $dafoc = '';
            
            for($idSort = 0; $idSort < count($recordsArray); $idSort++):
              $updateArr = array(                
                'idSort'    => $idSort                
              );            
              $this -> cliente_model -> updateIdSort($recordsArray[$idSort], $updateArr);
              
              print_r($recordsArray[$idSort] . ' - ' . $idSort . ' | ');
            endfor;
        else:
            echo ' ';            
        endif;
                                  
    }
	
	
	function agregar_do(){
		$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
		$imagen = $this -> file_model -> uploadItem('cliente', $file_data, 'imagen', false);
		$logo = $this -> file_model -> uploadItem('cliente', $file_data, 'logo', false);
		if (is_array($imagen)) {
			// $data['response'] = 'false';
			// $data['error'] = $imagen['error'];
			die($imagen['error']);
			// $this -> session -> set_flashdata('custom_error', $imagen['error']);
			
		} else {
			if(is_array($imagen)){
				die($logo['error']);
			}
			else{
				if($this->input->post('destacado')!=false){
					$destacado = 1;
				}
				else {
					$destacado = 0;
				}
				$permalink = guioner($this->input->post('nombre'));
				if ($this -> input -> post('twitter') != false) {
					$twitter = 'http://www.twitter.com/' . $this -> input -> post('twitter');
				} else {
					$twitter = null;
				}
				if ($this -> input -> post('facebook') != false) {
					$facebook = 'http://www.facebook.com/' . $this -> input -> post('facebook');
				} else{
					$facebook = null;
				}
				$arrInsert = array(
				'idCategoria' => $this->input->post('categoria'), 
				'nombre'      => $this->input->post('nombre'),
				'descripcion' => $this->input->post('descripcion'),
				'tags'        => $this->input->post('tags'),
				'direccion'   => $this->input->post('direccion'),
				'colonia'     => $this->input->post('colonia'),
				'telefono'    => $this->input->post('telefono'),
				'correo'      => $this->input->post('correo'),
				'web'         => $this->input->post('web'),
				'p'           => $this->input->post('p'),
				'imagen'      => $imagen,
				'logo'        => $logo,
				'destacado'   => $destacado,
				'facebook'    => $facebook,
				'twitter'     => $twitter,
				'latitud'        => $this->input->post('latitud'),
		    	'longitud'        => $this->input->post('longitud'),
		    	'giro'        => $this->input->post('giro'),
				'perma_link'  => $permalink
				);
				
				$this->cliente_model->insertCliente($arrInsert);
				$this->session->set_flashdata('error','insertOk');
			}
		}
		// echo json_encode($data);
		redirect('admin/directorio');
	}

	function delete($idCliente){
		$this->cliente_model->deleteCliente($idCliente);
		$this->session->set_flashdata('error','deleteOk');
		redirect('admin/directorio');
	}
	function deleteImage($idCliente,$idImgCliente){
		$this->cliente_model->deleteImgCliente($idImgCliente);
		$this->session->set_flashdata('error','deleteOk');
		redirect('admin/directorio/imagenes/'.$idCliente.'');
	}
	
	function editar($idCliente){
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/edit_client_view';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['categorias'] = $this->cliente_model->getCategorias();
		$data['pestana'] = 2;
		$data['idCliente'] = $idCliente;
		$data['cliente'] = $this->cliente_model->getCliente($idCliente);
		$this->load->view('admin/main_view',$data);
	}
	
	function editar_do($idCliente){
	 	if (!empty($_FILES['imagen']['name'])) {
	 			$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
	 			$imagen = $this -> file_model -> uploadItem('cliente', $file_data, 'imagen', false);
			if (is_array($imagen)) {
				// $data['response'] = 'false';
				// $data['error'] = $imagen['error'];
				$this -> session -> set_flashdata('custom_error', $image['error']);
			} else {
				if($this->input->post('destacado')!=false){
					$destacado = 1;
				}
				else {
					$destacado = 0;
				}
				if ($this -> input -> post('twitter') != false) {
					$twitter = 'http://www.twitter.com/' . $this -> input -> post('twitter');
				} else {
					$twitter = null;
				}
				if ($this -> input -> post('facebook') != false) {
					$facebook = 'http://www.facebook.com/' . $this -> input -> post('facebook');
				} else{
					$facebook = null;
				}
				$arrInsert = array(
				'idCliente'   => $idCliente,
				'nombre'      => $this->input->post('nombre'),
				'descripcion' => $this->input->post('descripcion'),
				'direccion'   => $this->input->post('direccion'),
				'tags'        => $this->input->post('tags'),
				'colonia'   => $this->input->post('colonia'),
				'telefono'    => $this->input->post('telefono'),
				'correo'      => $this->input->post('correo'),
				'web'         => $this->input->post('web'),
				'p'           => $this->input->post('p'),
				'imagen'      => $imagen,
				'destacado'   => $destacado,
				'facebook'    => $facebook,
                'twitter'     => $twitter,
                'latitud'        => $this->input->post('latitud'),
		    	'longitud'        => $this->input->post('longitud'),
		    	'giro'        => $this->input->post('giro')
				);
				$this->cliente_model->updateCliente($idCliente,$arrInsert);
				$this->session->set_flashdata('error','insertOk');
			}
			redirect('admin/directorio');
		}
		if (!empty($_FILES['logo']['name'])) {
	 			$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
	 			$logo = $this -> file_model -> uploadItem('cliente', $file_data, 'logo', false);
			if (is_array($logo)) {
				// $data['response'] = 'false';
				// $data['error'] = $imagen['error'];
				$this -> session -> set_flashdata('custom_error', $logo['error']);
			} else {
				if($this->input->post('destacado')!=false){
					$destacado = 1;
				}
				else {
					$destacado = 0;
				}
				if ($this -> input -> post('twitter') != false) {
					$twitter = 'http://www.twitter.com/' . $this -> input -> post('twitter');
				} else {
					$twitter = null;
				}
				if ($this -> input -> post('facebook') != false) {
					$facebook = 'http://www.facebook.com/' . $this -> input -> post('facebook');
				} else{
					$facebook = null;
				}
				$arrInsert = array(
				'idCliente'   => $idCliente,
				'nombre'      => $this->input->post('nombre'),
				'descripcion' => $this->input->post('descripcion'),
				'direccion'   => $this->input->post('direccion'),
				'tags'        => $this->input->post('tags'),
				'colonia'     => $this->input->post('colonia'),
				'telefono'    => $this->input->post('telefono'),
				'correo'      => $this->input->post('correo'),
				'web'         => $this->input->post('web'),
				'p'           => $this->input->post('p'),
				'logo'        => $logo,
				'destacado'   => $destacado,
				'facebook'    => $facebook,
                'twitter'     => $twitter,
                'latitud'     => $this->input->post('latitud'),
		    	'longitud'    => $this->input->post('longitud'),
		    	'giro'        => $this->input->post('giro')
				);
				$this->cliente_model->updateCliente($idCliente, $arrInsert);
				$this->session->set_flashdata('error','insertOk');
			}
			redirect('admin/directorio');
		}
		else{
			
			if($this->input->post('destacado')!=false){
				$destacado = 1;
			}
			else {
				$destacado = 0;
			}
			if ($this -> input -> post('twitter') != false) {
					$twitter = 'http://www.twitter.com/' . $this -> input -> post('twitter');
				} else {
					$twitter = null;
				}
				if ($this -> input -> post('facebook') != false) {
					$facebook = 'http://www.facebook.com/' . $this -> input -> post('facebook');
				} else{
					$facebook = null;
				}
			$arrInsert = array(
			'idCliente'   => $idCliente,
			'nombre'      => $this->input->post('nombre'),
			'descripcion' => $this->input->post('descripcion'),
			'direccion'   => $this->input->post('direccion'),
			'tags'        => $this->input->post('tags'),
			'colonia'     => $this->input->post('colonia'),
			'telefono'    => $this->input->post('telefono'),
			'correo'      => $this->input->post('correo'),
			'web'         => $this->input->post('web'),
			'p'           => $this->input->post('p'),
			'destacado'   => $destacado,
			'facebook'    => $facebook,
		    'twitter'     => $twitter,
		    'latitud'        => $this->input->post('latitud'),
		    'longitud'        => $this->input->post('longitud'),
		    'giro'        => $this->input->post('giro')
			);
			$this->cliente_model->updateCliente($idCliente, $arrInsert);
			$this->session->set_flashdata('error','updateOk');
			redirect('admin/directorio');
		}
	
	}

/*IMAGENES*/

	function imagenes($idCliente){
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/imagenes_client_view';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['cliente'] = $this->cliente_model->getCliente($idCliente);
		$data['pestana'] = 2;
		$data['imagenes'] = $this->cliente_model->getImagenes($idCliente);
		$this->load->view('admin/main_view',$data);
	}
	
	function addImagen($idCliente){
		
		$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
		$imagen = $this -> file_model -> uploadItem('cliente', $file_data, 'imagen', false);
		if (is_array($imagen)) {
			// $data['response'] = 'false';
			// $data['error'] = $imagen['error'];
			$this -> session -> set_flashdata('custom_error', $image['error']);
		} else {
			$arrInsert = array(
				'idCliente' => $idCliente,
				'imagen' => $imagen
			);
			$this->cliente_model->insertImagen($arrInsert);
			$this->session->set_flashdata('error','insertOk');
		}
		redirect('admin/directorio/imagenes/'.$idCliente);
	}

    ///  de todo un poco !!!!
    function index_deTodo() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['module'] = 'admin/show_deTodo_view';
		$data['pestana'] = 8;
		$data['clientes'] = $this->cliente_model->getClientesSubcatego();
		$this->load->view('admin/main_view',$data);
	}
	  function index_escolar() {
	  	$idSubcategoria = 18;
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['module'] = 'admin/show_deTodo_view';
		$data['pestana'] = 13;
		$data['clientes'] = $this->cliente_model->getClientesGuiaForAdmin($idSubcategoria);
		$this->load->view('admin/main_view',$data);
	}
	function index_guia() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['module'] = 'admin/ver_guia_view';
		$data['pestana'] = 13;
		$data['categorias'] = $this -> cliente_model -> getSubCategoriasGuia(); 
        $data['dirDetodo'] = $this -> cliente_model -> getSubCategoriasDeTodo();  
		$this->load->view('admin/main_view',$data);
	}
	function ver_guia_for_admin(){
        $data = array();
        $idSubCategoria = $this -> input -> get('categoria');
        if($idSubCategoria == 0){
        	$idSubCategoria = $this -> input -> get('dirDeTodo');
        }

        $data['SYS_metaTitle']          = 'Enfant';
        $data['SYS_metaKeyWords']       = 'Directorio Infantil, Infantes';
        $data['SYS_metaDescription']    = 'Directorio Infantil';
        $data['module'] = 'admin/show_guia_view';
		$data['contador'] =  $this->cliente_model->getContador(); 
        $data['pestana'] = 4;
		$data['subcategoria']= $idSubCategoria;
        $data['categorias'] = $this -> cliente_model -> getSubCategoriasGuia(); 
        $data['dirDetodo'] = $this -> cliente_model -> getSubCategoriasDeTodo(); 
        $data['clientes'] = $this -> cliente_model -> getClienteSubCategoria($idSubCategoria);       
			                    
        $this->load->view('admin/main_view',$data);        
    }
	  function index_restaurante() {
	  	$idSubcategoria = 19;
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['module'] = 'admin/show_deTodo_view';
		$data['pestana'] = 14;
		$data['clientes'] = $this->cliente_model->getClientesGuiaForAdmin($idSubcategoria);
		$this->load->view('admin/main_view',$data);
	}
	  function index_emergencia() {
	  	$idSubcategoria = 17;
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['module'] = 'admin/show_deTodo_view';
		$data['pestana'] = 15;
		$data['clientes'] = $this->cliente_model->getClientesGuiaForAdmin($idSubcategoria);
		$this->load->view('admin/main_view',$data);
	}
    function agregarClienteSubCategoria(){
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/new_clientSubCatego_view';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['pestana'] = 8;
		$data['categorias'] = $this->cliente_model->getSubCategorias();
		$this->load->view('admin/main_view',$data);
	}
    function agregarSubcategoria() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['colores'] = $this->cliente_model->getColores(); 
		$data['module'] = 'admin/new_subCategoria_view';
		$data['pestana'] = 8;
		
		$this->load->view('admin/main_view',$data);
	}
	function EditarSubcategoria() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['colores'] = $this->cliente_model->getColores(); 
		$data['categorias'] = $this -> cliente_model -> getSubCategorias(); 
        $data['dirDetodo'] = $this -> cliente_model -> getSubCategoriasDeTodo();  
		$data['module'] = 'admin/edit_sub_categoria_view';
		$data['pestana'] = 8;
		
		$this->load->view('admin/main_view',$data);
	}
    function agregar_do_ClientSubCatego(){
    			$subsubcategoria = null;
				if ($this->input->post('idSubSubcategoria') != false) {
					$subsubcategoria = trim($this->input->post('idSubSubcategoria'),'"') ;

				}

				$arrInsert = array(
				'idSubCategoria' => $this->input->post('idSubcategoria'), 
				'idSort'      => 0,
				'sub_sub_categoria' => $subsubcategoria,
				'nombre' => $this->input->post('nombre'), 
				'dir1' => $this->input->post('dir1'), 
				'dir2' => $this->input->post('dir2'), 
				'telefono' => $this->input->post('telefono'), 
				'telefono2' => $this->input->post('telefono2'), 
				'email' => $this->input->post('email')
				);
				
				$this->cliente_model->insertClientesSubcatego($arrInsert);
				$this->session->set_flashdata('error','insertOk');
				
		// echo json_encode($data);
		redirect('admin/directorio/index_guia');
	}
	function agregar_do_subCategoria(){
		
		$sub_sub_cat = null;
				if ($this->input->post('sub_sub_cat') != false) {
					//$sub_sub_cat = json_encode($subsubcatego);
					$sub_sub_cat = json_encode($this->input->post('sub_sub_cat'));
				}
				$arrInsert = array(
				'idCategoria' => 7, 
				'subCategoria'      => $this->input->post('subCategoria'),
				'poner_guia' => $this->input->post('option'),
				'color' =>  $this->input->post('color'),
				'sub_sub_categoria' => $sub_sub_cat
				);
				
				$this->cliente_model->insertSubCategoria($arrInsert);
				$this->session->set_flashdata('error','insertOk');
				
		// echo json_encode($data);
		redirect('admin/directorio/index_guia');
	}
	function edit_do_subCategoria(){
				$idSubCategoria = $this->input->post('idSubCategoria');
		$sub_sub_cat = null;
				if ($this->input->post('sub_sub_cat') != false) {
					//$sub_sub_cat = json_encode($subsubcatego);
					$sub_sub_cat = json_encode($this->input->post('sub_sub_cat'));
				}
				$arrUpdate = array(
				'subCategoria'      => $this->input->post('subCategoria'),
				'poner_guia' => $this->input->post('option'),
				'color' =>  $this->input->post('color'),
				'sub_sub_categoria' => $sub_sub_cat
				);
				
				$this->cliente_model->updateSubCategoria($idSubCategoria,$arrUpdate);
				$this->session->set_flashdata('error','updateOk');
				
		// echo json_encode($data);
		redirect('admin/directorio/index_guia');
	}
	function deleteClientSubCatego($idClienteSubCatego){
		$this->cliente_model->deleteClienteSubcatego($idClienteSubCatego);
		$this->session->set_flashdata('error','deleteOk');
		redirect('admin/directorio/index_deTodo');
	}
	function editarClientSubCatego($idClienteSubCatego){
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/edit_clientSubCatego_view';
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['categorias'] = $this->cliente_model->getSubCategorias();
		$data['pestana'] = 7;
		$data['idCliente'] = $idClienteSubCatego;
		$cliente = $this->cliente_model->getClienteSubCatego($idClienteSubCatego);
		$objeto = $this->cliente_model->getSubCategoria($cliente->idSubcategoria);
		if ($objeto->sub_sub_categoria != null) {
			$sub_sub_catego = json_decode($objeto->sub_sub_categoria);
			$osh='';
			$data['html'] = '';
	    	for($i = 0 ; $i < count($sub_sub_catego);$i++) {
	    		$comparacion = '"'.$cliente->sub_sub_categoria.'"';
	    		if (htmlentities(json_encode($sub_sub_catego[$i])) == $comparacion) {
	    			$osh = 'selected = "selected"';

	    		}
	    		$data['html'].='<option '.$osh.'value="'.htmlentities(json_encode($sub_sub_catego[$i])).'">'.$sub_sub_catego[$i].'</option>';
	    	}
		}
		
		$data['cliente'] = $cliente ; 

		$this->load->view('admin/main_view',$data);
	}
	
	function editar_do_ClientSubCatego($idClienteSubCatego){
		$subsubcategoria = null;
				if ($this->input->post('idSubSubcategoria') != false) {
					$subsubcategoria = trim($this->input->post('idSubSubcategoria'),'"') ;
				}

	$arrInsert = array(
				'idSubCategoria' => $this->input->post('idSubcategoria'), 
				'sub_sub_categoria' => $subsubcategoria,
				'nombre' => $this->input->post('nombre'), 
				'dir1' => $this->input->post('dir1'), 
				'dir2' => $this->input->post('dir2'), 
				'telefono' => $this->input->post('telefono'), 
				'telefono2' => $this->input->post('telefono2'), 
				'email' => $this->input->post('email')
				);
				$this->cliente_model->updateClientesSubcatego($idClienteSubCatego,$arrInsert);
				$this->session->set_flashdata('error','insertOk');
			
			redirect('admin/directorio/index_guia');
	
	}
    function ordenarSubcategoria(){
        $data = array();
        $data['SYS_metaTitle']          = 'Enfant';
        $data['SYS_metaKeyWords']       = 'Directorio Infantil, Infantes';
        $data['SYS_metaDescription']    = 'Directorio Infantil';
        $data['module'] = 'admin/ordenar_subcategoria_view';
		$data['contador'] =  $this->cliente_model->getContador(); 
        $data['pestana'] = 4;
        $data['categorias'] = $this -> cliente_model -> getSubCategoriasGuia(); 
        $data['dirDetodo'] = $this -> cliente_model -> getSubCategoriasDeTodo();             
        $this->load->view('admin/main_view',$data);
    }
	function ordenar_do_subcategoria(){
        $data = array();
        $idSubCategoria = $this -> input -> get('categoria');
        if($idSubCategoria == 0){
        	$idSubCategoria = $this -> input -> get('dirDeTodo');
        }

        $data['SYS_metaTitle']          = 'Enfant';
        $data['SYS_metaKeyWords']       = 'Directorio Infantil, Infantes';
        $data['SYS_metaDescription']    = 'Directorio Infantil';
        $data['module'] = 'admin/do_ordenar_subcategoria_view';
		$data['contador'] =  $this->cliente_model->getContador(); 
        $data['pestana'] = 4;
		$data['subcategoria']= $idSubCategoria;
        $data['categorias'] = $this -> cliente_model -> getSubCategoriasGuia(); 
        $data['dirDetodo'] = $this -> cliente_model -> getSubCategoriasDeTodo(); 
        $data['clientes'] = $this -> cliente_model -> getClienteSubCategoria($idSubCategoria);       
			                    
        $this->load->view('admin/main_view',$data);        
    }
	 function do_ordenar_subcategoria($idSubcategoria){
        $data = array();        
        $data['response'] = 'false';        
        if($this -> input -> post('action') == 'updateRecordsListings'):
            
            $recordsArray = array();
            $recordsArray = $this -> input -> post('recordsArray');
            
            $dafoc = '';
            
            for($idSort = 0; $idSort < count($recordsArray); $idSort++):
              $updateArr = array(                
                'idSort'    => $idSort                
              );            
              $this -> cliente_model -> updateIdSortSubCategoria($recordsArray[$idSort], $updateArr);
              
              //print_r($recordsArray[$idSort] . ' - ' . $idSort . ' | ');
            endfor;
        else:
            echo ' ';            
        endif;
                               
    }
    function editSub(){
    	$data['response'] = 'false';
    	$data['obj'] = array();
    	$sub_categoria = $this->input->post('subCat');
    	$colores = $this->cliente_model->getColores(); 
    	$obj_color = array();
    	foreach ($colores as $row) {
    		# code...
    	}
    	$objeto = $this->cliente_model->getSubCategoria($sub_categoria);
    	if ($objeto->color != null) {
    		$data['colores'] = $this->cliente_model->getColores(); 
    	}
    	if ($objeto->sub_sub_categoria != null) {
    		$data['response'] = 'true';
    		$data['obj'] = $objeto;
    		$data['flag'] = '1';
    		$data['sub_sub_categoria'] = json_decode($objeto->sub_sub_categoria);
    		
    	} else{
    		$data['response'] = 'true';
    		$data['obj'] = $objeto;
    		$data['flag'] = '0';
    	}

    	echo json_encode($data);
    }
    function traemeSubSub(){
    	$sub_categoria = $this->input->post('subCat');
    	$objeto = $this->cliente_model->getSubCategoria($sub_categoria);
    	$data['response'] = 'false';
    	$data['html'] = '';
    	$sub_sub_catego = array();
    	//die(var_dump($objeto->sub_sub_categoria));
    	if ($objeto->sub_sub_categoria != null) {
    		$sub_sub_catego = json_decode($objeto->sub_sub_categoria);

    		$data['html'].='<option value="">---------</option>';
    		$data['response'] = 'true';
    		for($i = 0 ; $i < count($sub_sub_catego);$i++) {
    			$data['html'].='<option value="'.htmlentities(json_encode($sub_sub_catego[$i])).'">'.$sub_sub_catego[$i].'</option>';
    		}
    	}
    	
    	echo json_encode($data);
    }

    function guioner_a_todos(){
	    $todo = $this ->cliente_model->te_llevo_todo();
	    foreach ($todo as $row) {
	    	$array = array('perma_link'=> guioner($row ->nombre));
	    	$this->cliente_model->update_todos($array,$row->idCliente);
	    }
    }

    function deleteSubCat(){
    	$idSubCategoria = $this->input->post('idSub');
    	$data['response'] = 'false';
    	if ($this->cliente_model->deleteSubCat($idSubCategoria)) {
    		$data['response'] = 'true';
    	} 
    	echo json_encode($data);
    }
}