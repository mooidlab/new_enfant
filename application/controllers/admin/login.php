<?php
if (!defined('BASEPATH'))

	exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(is_logged()) // checamos si existe una sesión activa
			redirect('admin');
	}

	public function index($redir = null) {
		// die($redir);
		$data['SYS_metaTitle'] = 'Enfant | Sign In';
		$data['SYS_metaDescription'] = " 'Enfant - Directorio Infantil";
		$data['SYS_MetaKeywords'] = '';
		$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
		$data['redir'] = $redir.$query;
		// setMyToken('loginForm');		
		$this -> load -> view('admin/login_view', $data);

	}	
	
	function asdf($a){
		$this->load->model('auth_model');
		echo $this->auth_model->hashPassword($a, null);
	}
}
?>