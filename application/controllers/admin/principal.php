<?php if ( ! defined('BASEPATH')) 
	exit('No direct script access allowed');

class Principal extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('cliente_model');
		if(!is_logged()){
			$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
			$redir = str_replace('/', '-', uri_string().$query);
			redirect('admin/login/index/' . $redir);
		} // checamos si existe una sesión activa
			
		if(!is_authorized(array(0,1),null, $this->session->userdata('nivel'), null)){
			redirect('sesion/logout/index/userNotAutorized');
		}    }
	
	// Si no esta Sign In entonces muestro pantalla principal
	function index() {	
		$data['SYS_metaTitle'] 			= 'Enfant Administración';
		$data['SYS_metaDescription'] 	= 'Administracion | Dashboard';
		$data['pestana'] 				= 0;
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['module']					= 'admin/principal_view';
		$this->load->view('admin/main_view', $data);
	}
}