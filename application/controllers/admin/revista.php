<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revista extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
        if(!is_logged()){
            $query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
            $redir = str_replace('/', '-', uri_string().$query);            
            // die(var_dump(is_logged()));
            redirect('admin/login/index/' . $redir);
        } // checamos si existe una sesión activa
		$this->load->helper(array('form', 'url'));
		$this->load->model('revista_model');
		$this->load->model('file_model');
		$this->load->model('cliente_model');
        $this->load->library('pagination');
    }
	
	function index() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/show_revista_view';
		$data['pestana'] = 6;
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['revista'] = $this->revista_model->getRevista();
		$this->load->view('admin/main_view',$data);
	}

	function agregar(){
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/new_revista_view';
		$data['pestana'] = 5;
		$data['revista'] = $this->revista_model->getRevista();
		$data['contador'] =  $this->cliente_model->getContador(); 
		$this->load->view('admin/main_view',$data);
	}
    
    

    function ordenar_do(){
        $data = array();
       $data['contador'] =  $this->cliente_model->getContador(); 
        $data['SYS_metaTitle']          = 'Enfant';
        $data['SYS_metaKeyWords']       = 'Directorio Infantil, Infantes';
        $data['SYS_metaDescription']    = 'Directorio Infantil';
        $data['module'] = 'admin/do_ordenar_revista_view';
        $data['pestana'] = 4;
        $data['paginas'] = $this -> revista_model -> getRevista();
          $data['revista'] = $this->revista_model->getRevista();                    
        $this->load->view('admin/main_view',$data);        
    }

    function do_ordenar(){
        $data = array(); 
		$ajax = array();
        $data['response'] = 'false';        
        if($this -> input -> post('action') == 'updateRecordsListings'):
            
            $recordsArray = array();
            $recordsArray = $this -> input -> post('recordsArray');
            
            $dafoc = '';
            
            for($idSort = 0; $idSort < count($recordsArray); $idSort++):
              $updateArr = array(                
                'idSort'    => $idSort                
              );            
              $this -> revista_model -> updateIdSort($recordsArray[$idSort], $updateArr);
              
            //  print_r($recordsArray[$idSort] . ' - ' . $idSort . ' | ');
			  
            endfor;
        else:
            echo ' ';            
        endif;
        $data['paginas'] = $this -> revista_model -> getRevista();
        $this->load->view('admin/do_ordenar_revista_view',$data);                          
    }
	
	
	function agregar_do(){
		$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
		$imagen = $this -> file_model -> uploadItem('revista', $file_data, 'imagen', false);
		
		if (is_array($imagen)) {
			// $data['response'] = 'false';
			// $data['error'] = $imagen['error'];
			die($imagen['error']);
			// $this -> session -> set_flashdata('custom_error', $imagen['error']);
			
		} else {
			if(is_array($imagen)){
				die($logo['error']);
			}
			else{
				
				$arrInsert = array(
				'idSort'      => $this->input->post('idSort'),
				'imagen'      => $imagen
				
				
				);
				
				$this->revista_model->insertRevista($arrInsert);
				$this->session->set_flashdata('error','insertOk');
			}
		}
		// echo json_encode($data);
		redirect('admin/revista');
	}

	function delete($idRevista){
		$this->revista_model->deleteResvista($idRevista);
		$this->session->set_flashdata('error','deleteOk');
		redirect('admin/revista');
	}
	
	function editar($idBanner){
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/edit_banner_view';
		$data['clientes'] = $this->cliente_model->getClienteBanner();
		$data['pestana'] = 5;
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['idBanner'] = $idBanner;
		$data['revista'] = $this->revista_model->getRevista();
		$data['banner'] = $this->cliente_model->getBanner($idBanner);
		$this->load->view('admin/main_view',$data);
	}
	
	function editar_do($idBanner){
	 	if (!empty($_FILES['imagen']['name'])) {
	 			$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
	 			$imagen = $this -> file_model -> uploadItem('revista', $file_data, 'imagen', false);
			if (is_array($imagen)) {
				// $data['response'] = 'false';
				// $data['error'] = $imagen['error'];
				$this -> session -> set_flashdata('custom_error', $image['error']);
			} else {
				
				$arrInsert = array(
				'status'      => 1,
				'posicion' => $this->input->post('posicion'),
				'idCliente' => $this->input->post('nombre'), 
				'imagen'      => $imagen
				);
				$this->cliente_model->updateBanner($idBanner,$arrInsert);
				$this->session->set_flashdata('error','insertOk');
			}
			redirect('admin/banner');
		}
		
	
	}
    function ajaxRevista(){
        $data = array();
        $data['paginas'] = $this -> revista_model -> getRevista();
        $data['contador'] =  $this->cliente_model->getContador();    
    }
/*IMAGENES*/

	
}