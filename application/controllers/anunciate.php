<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anunciate extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('cliente_model');
    }

	function index() {
		$data['SYS_metaTitle'] 			= 'Enfant | Anunciate';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes, Enfant, Anunciate';
		$data['SYS_metaDescription'] 	= 'Anunciate en Enfant y obten beneficios';
		$data['module'] ='publico/anunciate_view';
		$data['banners'] =  $this->cliente_model->getBanners();   
		$this->load->view('publico/main_2_view',$data);
	}
}

/* End of file anunciate.php */
/* Location: ./application/controllers/anunciate.php */