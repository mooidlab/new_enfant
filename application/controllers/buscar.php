<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Buscar extends CI_Controller {

	

	public function __construct(){

		parent::__construct();

		$this->load->helper(array('form', 'url'));

		$this->load->model('cliente_model');

		$this->load->library('pagination');

    }
    function prebuscar(){
		$this->form_validation->set_rules('palabraClave', 'Palabra Clave', 'trim|xss_clean');
		$this->form_validation->set_message('xss_clean','Campo "%s" contiene posible ataque XSS');
		if(!$this->form_validation->run()){
			return false;
		}
		else{
			
		$palabraClave = $this->input->post('palabraClave');
				
			}
			if($palabraClave == NULL){
			$url = 'a'; //este número hace referencia a editar imagenes.
			}
			else{
			$url = $palabraClave; 	
			}
			redirect(base_url().'buscar/index/'.$url);
		
	}
	

	function index($palabraClave) {
		$data = array();
		
		
		$data['SYS_metaTitle'] 			= 'Enfant';

		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';

		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
         
		$data['module'] ='publico/buscar_view.php';
		$data['banners'] =  $this->cliente_model->getBanners();   
        
		$config['base_url'] = base_url().'buscar/index/'.$palabraClave.'/';

        $config['total_rows'] = $this->cliente_model->countBuscar($palabraClave);

        $config['per_page'] = 6;

        $config['uri_segment'] = 4;

        $config['display_pages'] = FALSE; 

        //$config['num_links'] = 1; //Numero de links mostrados en la paginación
        //$config['use_page_numbers'] = false;
       
        $config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
        $config['next_tag_open'] = '<div class="btn_next_tag">';
        $config['next_tag_close'] = '</div>';
		$config['prev_link'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '; 
        $config['prev_tag_open'] = '<div class="btn_back_tag">';
        $config['prev_tag_close'] = '</div>';
        $config['first_link'] = FALSE;
		//$config['first_tag_open'] = '';
		//$config['first_tag_close'] = '';
        $config['last_link'] = FALSE;
		//$config['last_tag_open'] = '';
		//$config['last_tag_close'] = '';

        $this->pagination->initialize($config);
	    $data['palabraClave'] = $palabraClave;
	    $data['resultado'] = $this->cliente_model->resultBuscar($palabraClave,$this->uri->segment(4),$config['per_page']);
		
		$this->load->view('publico/main_2_view',$data);

	}
	
}
?>