<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Clientes extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('cliente_model');
		$this->load->library('pagination');
    }

	function index() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] ='publico/cliente_view.php';
		$data['banners'] =  $this->cliente_model->getBanners();   
		$this->load->view('publico/main_2_view',$data);
	}

	function ver($idCategoria,$idCliente){
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['banners'] =  $this->cliente_model->getBanners();   
		$data['module'] ='publico/cliente_view';
		$permalink = $this->cliente_model->getCliente($idCliente);
		$config['base_url'] = base_url().'clientes/ver/'.$idCategoria.'/'.$idCliente;
        $config['total_rows'] = $this->cliente_model->countClientesCategoria($idCategoria);
        $config['per_page'] = 1;
		$config['uri_segment'] = 6;
		$config['display_pages'] = FALSE; 
        //$config['num_links'] = 1; //Numero de links mostrados en la paginación
        //$config['use_page_numbers'] = false;
		$config['next_link'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
        $config['next_tag_open'] = '<div class="btn_next_tag">';
        $config['next_tag_close'] = '</div>';
		$config['prev_link'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '; 
        $config['prev_tag_open'] = '<div class="btn_back_tag">';
        $config['prev_tag_close'] = '</div>';
        $config['first_link'] = FALSE;
		//$config['first_tag_open'] = '';
		//$config['first_tag_close'] = '';
        $config['last_link'] = FALSE;
		//$config['last_tag_open'] = '';
		//$config['last_tag_close'] = '';
        $data['rowCount'] =  $config['total_rows'] ; 
		$cliente = $this->cliente_model->getClientesCategoriaPaginado($idCategoria,$idCliente,$this->uri->segment(6),$config['per_page']);
		$imagenes = $this->cliente_model->getImgClientesCategoria ($idCategoria);
		foreach ($cliente as $row) {
			$url=$row->perma_link;
		}
		$config['base_url'] .='/'.$url.'';
		$this->pagination->initialize($config);
		$data['row'] = $cliente;  
		$data['rowimg'] = $imagenes;  
		switch($idCategoria){
		    case 1: 	
			$data['color'] = '#DF9E9E';
			break;
			case 2: 	
			$data['color'] = '#FDBB30';
			break;
			case 3: 	
			$data['color'] = '#75C7B9';
			break;
			case 4: 	
			$data['color'] = '#556292';
			break;
			case 5: 	
			$data['color'] = '#EB6D51';
			break;
			case 6: 	
			$data['color'] = '#E8941A';
			break;
			case 7: 	
			$data['color'] = '#956E8E';
			break;
		}   
		$this->load->view('publico/main_2_view',$data);
	}

	function clases($idCategoria){

		

		$data['SYS_metaTitle'] 			= 'Enfant';

		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';

		$data['SYS_metaDescription'] 	= 'Directorio Infantil';

		$data['module'] ='publico/clientes_view';

		
        $data['banners'] =  $this->cliente_model->getBanners();   
     

		$config['base_url'] = base_url() . 'clientes/clases/'.$idCategoria.'/';

        $config['total_rows'] = $this->cliente_model->countClientesCategoria($idCategoria);

        $config['per_page'] = 12;

        $config['uri_segment'] = 4;

        $config['num_links'] = 3; //Numero de links mostrados en la paginación

        $config['first_link'] = 'Inicio';

        $config['last_link'] = 'Fin';

        $this->pagination->initialize($config);

	
        $data['rowCount'] =  $config['total_rows'] ; 
	

		$cliente = $this->cliente_model->getClientesCategoria($idCategoria,$config['per_page'],$this->uri->segment(4));
        
		$imagenes = $this->cliente_model->getImgClientesCategoria ($idCategoria);

		

		$data['row'] = $cliente;  

		$data['rowimg'] = $imagenes;   

        

        switch($idCategoria){

			case 1: 	

		$data['color'] = '#DF9E9E';
        $data['categoria'] = 'Mis Clases';
		break;

		case 2: 	

		$data['color'] = '#FDBB30';
        $data['categoria'] = 'Mi Salud';
		break;

		case 3: 	

		$data['color'] = '#75C7B9';
        $data['categoria'] = 'Mi Mamá';
		break;

		case 4: 	

		$data['color'] = '#556292';
		$data['categoria'] = 'Mi Diversión';
		break;

		case 5: 	

		$data['color'] = '#EB6D51';
		$data['categoria'] = 'Mi Fiesta';
		break;

		case 6: 	

		$data['color'] = '#E8941A';
		$data['categoria'] = 'Mi Ropa y Accesorios';
		break;

		case 7: 	

		$data['color'] = '#956E8E';
		$data['categoria'] = 'De Todo un Poco';
		break;



		}    
		$this->load->view('publico/main_2_view',$data);


	}





}
