<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Contacto extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this -> load -> helper(array('form', 'url'));
        $this->load->model('cliente_model');
	}
	function index() {
		$data['SYS_metaTitle'] = 'Enfant';
		$data['SYS_metaKeyWords'] = 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] = 'Directorio Infantil';
		$data['banners'] =  $this->cliente_model->getBanners();   
		$data['module'] = 'publico/contacto_view.php';
		$this -> load -> view('publico/main_2_view', $data);
	}

	function enviar() {
		// $this -> load -> library('email');
		// $config = array(
		// 'language'  => 'english',
		// 'protocol'  => 'smtp',
		// 'smtp_host' => '127.0.0.1',
		// 'smtp_port' => 25,
		 // 'smtp_user' => 'postmaster@127.0.0.1',
		 // 'smtp_pass' => '123456',
		// 'mailtype'  => 'text',
		// 'crlf'      => "\r\n",
		 // 'newline'   => "\r\n"
		 // );
		// $this -> email -> initialize($config);
		$this -> load -> library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$this -> email -> initialize($config);
		$mensaje = '<p>Mensaje de: ' . $this -> input -> post('nombre') . '<br/>Teléfono:' . $this -> input -> post('telefono') . ' <br/>Correo: ' . $this -> input -> post('email') . '<br/>Mensaje: <br/>' . $this -> input -> post('comment') . '</p><br/><br/><p>Mensaje enviado desde www.enfant.com/contacto </p>';
		$this -> email -> from($this -> input -> post('email'), $this -> input -> post('nombre'));
		if($this -> input -> post('asunto') == 1){
			$this -> email -> to('info@enfant.com.mx'); 
		}else{
			$this -> email -> to('ventas@enfant.com.mx'); //ventas@enfant.com.mx
		}
		$this -> email -> subject('Contacto desde web');
		$this -> email -> message($mensaje);
		if ($this -> email -> send()) {
			$data['estatus'] = 'ok';
			if($this -> input -> post('asunto') == 1){
				$data['html'] = 'Tu información fue enviada al equipo, recibirás promociones próximamente!';
			}else{
				$data['html'] ='gracias en breve nos pondremos en contacto contigo.';
			}
		} else {
			$data['estatus'] = 'fail';
			$data['html'] = '¡ah ocurrido un error! No se recibio el correo.';
		}
		echo json_encode($data);
	}
	function participa() {
		// $this -> load -> library('email');
		// $config = array(
		// 'language'  => 'english',
		// 'protocol'  => 'smtp',
		// 'smtp_host' => '127.0.0.1',
		// 'smtp_port' => 25,
		 // 'smtp_user' => 'postmaster@127.0.0.1',
		 // 'smtp_pass' => '123456',
		// 'mailtype'  => 'text',
		// 'crlf'      => "\r\n",
		 // 'newline'   => "\r\n"
		 // );
		// $this -> email -> initialize($config);
		$this -> load -> library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$this -> email -> initialize($config);
		//info@enfant.com.mx
		$mensaje = '<p>Mensaje de: ' . $this -> input -> post('nombre') . '<br/>Correo: ' . $this -> input -> post('email') . '<br/>Residencia: <br/>' . $this -> input -> post('estado') . '<br/>Num de hijos: <br/>' . $this -> input -> post('hijos') . '</p><br/><br/><p>Deseo Participar en Promociones Exclusivas de Enfant </p>';
		$this -> email -> from($this -> input -> post('email'), $this -> input -> post('nombre'));
		$this -> email -> to('info@enfant.com.mx');
		$this -> email -> subject('Deseo participar en las promociones de Enfant');
		$this -> email -> message($mensaje);
		if ($this -> email -> send()) {
			$data['estatus'] = 'ok';
			$data['html'] = 'Tu información fue enviada al equipo, recibirás promociones próximamente!';
		} else {
			$data['estatus'] = 'fail';
			$data['html'] = '¡ah ocurrido un error! No se recibio el correo.';
		}
		//die(var_dump($data));
		echo json_encode($data);
	}
	function revistaParticipa() {
		// $this -> load -> library('email');
		// $config = array(
		// 'language'  => 'english',
		// 'protocol'  => 'smtp',
		// 'smtp_host' => '127.0.0.1',
		// 'smtp_port' => 25,
		 // 'smtp_user' => 'postmaster@127.0.0.1',
		 // 'smtp_pass' => '123456',
		// 'mailtype'  => 'text',
		// 'crlf'      => "\r\n",
		 // 'newline'   => "\r\n"
		 // );
		// $this -> email -> initialize($config);
		$this -> load -> library('email');
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$this -> email -> initialize($config);
		//info@enfant.com.mx
		$mensaje = '<p>Mensaje de: ' . $this -> input -> post('nombre') . '<br/>Correo:' . $this -> input -> post('correo') . '<br/>Dirección: <br/>' . $this -> input -> post('direccion') . '<br/>Mensaje: <br/>' . $this -> input -> post('mensaje') . '</p><br/><br/><p>Deseo Recibir la revista Enfant</p>';
		$this -> email -> from($this -> input -> post('correo'));
		$this -> email -> to('info@enfant.com.mx');//info@enfant.com.mx
		$this -> email -> subject('Deseo recibir la revista Enfant');
		$this -> email -> message($mensaje);
		$data['html'] = 'Su mensaje ha sido enviado, contestaremos a la brevedad <b>GRACIAS</b>';
		if ($this -> email -> send()) {
			$data['estatus'] = 'ok';
		} else {
			$data['estatus'] = 'fail';
		}
		echo json_encode($data);

	}
   function goEnfant(){
   	$this->load->view('publico/first_view');
   }
   function contactoRevista() {
   	$this->load->view('publico/revista_contacto_view');
   }

}
