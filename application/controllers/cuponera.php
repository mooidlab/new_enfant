<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Cuponera extends CI_Controller {

    public function __construct(){
            parent::__construct();
            $this->load->helper(array('form', 'url'));
            $this->load->model('cliente_model');
            $this->load->model('cuponera_model');
            $this->load->library('pagination');
    }



    function index() {

            $data['SYS_metaTitle']                  = 'Enfant';

            $data['SYS_metaKeyWords']               = 'Directorio Infantil, Infantes';

            $data['SYS_metaDescription']    = 'Directorio Infantil';
            $cupones = $this->cuponera_model->getCuponesFront(); 
            
            if (!is_null($cupones)) {
                $data['pages'] = round($cupones->num_rows() / 12);  
                $data['cupones'] = $cupones->result();
            }  else {
                $data['pages'] = 1;
                $data['cupones'] = null;
            }
            
            $data['module'] ='publico/cuponera_view.php';
            $data['banners'] =  $this->cliente_model->getBanners();   
            $data['color'] = '#5EC2A5';
            $data['categoria'] = 'Cuponera';
            $this->load->view('publico/main_2_view',$data);

    }
    function bridge(){
       
        $ipVisitante=$_SERVER['REMOTE_ADDR'];
        $email = $this->input->post('email');
        $ihavecuponera=$this->input->cookie('ihavecupones');
        $uid = substr(md5(uniqid(rand())), 0, 20);
        $Hash=md5($_SERVER['REMOTE_ADDR'].$uid).':'.md5($uid);
        $usuario = $this->cuponera_model->checkMail($email);
        $fecha_actual = date("Y-m-d H:i:s");
        if($ihavecuponera != false){
            
            $data['response'] = 'false';

        }else{
             if($usuario != null){
                $fechaFinal = strtotime($fecha_actual)- strtotime($usuario->fecha) ;
                $fechaFinal = $fechaFinal / 60 / 60 / 24 ;
                
                if($fechaFinal < 30){
                    $data['response'] = 'false';
                }else{
                    $this->cuponera_model->updateMail($usuario->idCuponeraCliente,$ipVisitante,$Hash,$usuario->vecesDescargado);
                    $data['response'] = 'true';
                }
             }else{
                $this->cuponera_model->registrarMail($email,$ipVisitante,$Hash);
                $data['response'] = 'true';
             }

             
         }
       echo json_encode($data);
    }

    function imprimir(){
        $data['SYS_metaTitle']                  = 'Enfant';
        $data['SYS_metaKeyWords']               = 'Directorio Infantil, Infantes';
        $data['SYS_metaDescription']    = 'Directorio Infantil';
        $cookie=$this->input->cookie('ihavecupones');
        $data['usuario']= $this->cuponera_model->checkMailByCookie($cookie);
        $cupones = $this->cuponera_model->getCuponesFront();   
        $data['cupones'] = $cupones->result();
        $this->load->view('publico/imprimir/cuponera_print_view',$data);
    }



}
