<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Principal extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('blog_model');
		$this->load->model('cliente_model');
		$this->load->helper('cookie');
    }
    

	function index() {
		if(get_cookie('enfant') == (int)1){
			$data['popUp'] = 0;
		} else {
			$cookie = array(
			    'name'   => 'enfant',
			    'value'  => 1,
			    'expire' => '1086500',
			    'domain' => 'enfant.com.mx',
			    'path'   => '/',
			    'prefix' => ''
			);
			$this->input->set_cookie($cookie);
			$data['popUp'] = 1;
		}
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['posts'] = $this->blog_model->get_coments();
		$data['clientes'] = $this->cliente_model->getFeaturedClientes(); 
		$data['banners'] =  $this->cliente_model->getBanners();  
		$data['bottom'] = $this->cliente_model->getBannersBottom();
		$count = array('contador' => 1);
		$this->cliente_model->insrtContador($count);
		
		$data['module'] = 'publico/principal_view.php';	
		$data['calendar'] = $this->blog_model->getCalendar();
		$data['mini'] = $this->cliente_model->getBannersMini();
		$this->load->view('publico/main_view',$data);

	}
}