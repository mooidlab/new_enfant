<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Privacidad extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('cliente_model');
    }

	function index() {
		$data = array();
		$this->load->view('publico/privacidad_view',$data);
	}

}

/* End of file privacidad.php */
/* Location: ./application/controllers/privacidad.php */