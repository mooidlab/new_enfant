<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Revista extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('cliente_model');
		$this->load->model('revista_model');
    }
	
	function index() {
		$data['SYS_metaTitle'] 			= 'Enfant | Revista';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] ='publico/revista_view.php';
		$data['banners'] =  $this->cliente_model->getBanners();  
		$data['revista'] = $this->revista_model->getRevista(); 
		$this->load->view('publico/main_2_view',$data);
		
		
	}
	function grande(){
		$data['revista'] = $this->revista_model->getRevista(); 
		$this->load->view('publico/revista_big_view',$data);
	}
	
	

}