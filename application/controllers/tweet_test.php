<?php

	class Tweet_test extends CI_Controller {
		
		function __construct()
		{
			parent::__construct();
			
			// It really is best to auto-load this library!
			$this->load->library('tweet');
			$this->load->model('twitter_model');
			
			// Enabling debug will show you any errors in the calls you're making, e.g:
			$this->tweet->enable_debug(TRUE);
			
			// If you already have a token saved for your user
			// (In a db for example) - See line #37
			// 
			// You can set these tokens before calling logged_in to try using the existing tokens.
			$token = $this->config->item('tweet_acces_token');
			$token_secret = $this->config->item('tweet_acces_token_secret');
			$tokens = array('oauth_token' => $token, 'oauth_token_secret' => $token_secret);
			$this->tweet->set_tokens($tokens);
			
			
			if ( !$this->tweet->logged_in() )
			{
				// This is where the url will go to after auth.
				// ( Callback url )
				
				$this->tweet->set_callback(site_url('tweet_test/auth'));
				
				// Send the user off for login!
				$this->tweet->login();
			}
			else
			{
				// You can get the tokens for the active logged in user:
				// $tokens = $this->tweet->get_tokens();
				
				// 
				// These can be saved in a db alongside a user record
				// if you already have your own auth system.
			}
		}
		
		function index($cadena)
		{
			if ($cadena === 'c03798a7e138f08037e6fd65c5d880ea'){
				$data = $this->tweet->call('GET','statuses/user_timeline',array('screen_name' => 'EnfantMx','count' => 4));
			$tweets = $this->twitter_model->getTweets();
			foreach ($tweets as $row) {
				$this->twitter_model->deleteTweets($row->idTwitter);
			}
			


				foreach ($data as $row) {

					$id =  $row->id;
					$text = $row->text;
					$fecha = $row->created_at;
					$name = $row->user->name;
					$screen_name = $row->user->screen_name;
					$img =  $row->user->profile_image_url;
					
					if(isset($row->entities->media)){
						$link = $row->entities->media[0]->url;
						
					} else {
						$link= null;
					}
					if(isset($row->entities->user_mentions)&&$row->entities->user_mentions != null){
						$link_screen_name =  $row->entities->user_mentions[0]->screen_name;
						
					} else {
						$link_screen_name= null;
					}
					if(isset($row->entities->urls)&&$row->entities->urls != null){
						$link2 = $row->entities->urls[0]->url;
						
					} else {
						$link2= null;
					}
					$arrInsert = array(
						'id' => $id,
						'name' => $name,
						'text' => $text,
						'screen_name' => $screen_name,
						'img' => $img,
						'link1' => $link,
						'link2' => $link2,
						'follower' => $link_screen_name,
						'fecha' => $fecha 
						);

				$this->twitter_model->insertTweets($arrInsert);
				}
			}

			
		}
		
	}