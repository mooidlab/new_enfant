<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LectorRSS {
	var $url;
	var $data;
	function LectorRSS ($url){
		$this->url;
		$this->data = implode ("", file ($url));
	}
	function obtener_items (){
		preg_match_all ("/<item .*>.*<\/item>/xsmUi", $this->data, $matches);
		$items = array();
		foreach ($matches[0] as $match){
			$items[] = new RssItem ($match);
		}
		return $items;
	}
}

/* End of file LectorRSS.php */