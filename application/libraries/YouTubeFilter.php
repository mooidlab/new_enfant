<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class YouTubeFilter{
    protected function preFilter($html) {
        $pre_regex = '#<object[^>]+>.+?'.
            'http://www.youtube.com/v/([A-Za-z0-9\-_]+).+?</object>#s';
        $pre_replace = '\1';
        return preg_replace($pre_regex, $pre_replace, $html);
    }	
    protected function armorUrl($url) {
        return str_replace('--', '-&#45;', $url);
    }	
    public function filtrarYouTube($html, $w, $h) {
        $url = $this->armorUrl($this->preFilter($this->preFilter($html)));
        return '<object width="'.$w.'" height="'.$h.'" type="application/x-shockwave-flash" '.
            'data="http://www.youtube.com/v/'.$url.'">'.
            '<param name="movie" value="http://www.youtube.com/v/'.$url.'">'.
            '<!--[if IE]>'.
            '<embed src="http://www.youtube.com/v/'.$url.'"'.
            'type="application/x-shockwave-flash"'.
            'wmode="transparent" width="'.$w.'" height="'.$h.'" />'.
            '<![endif]-->'.
            '</object>';
    }
}

/* End of file YouTubeFilter.php */