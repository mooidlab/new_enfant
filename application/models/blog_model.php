<?php
   if(!defined('BASEPATH'))
	die('No');

class Blog_model extends CI_Model{
	
	
	function __construct(){
		parent::__construct();
		$this -> tablas = $this -> config -> item('tablas', 'tables');
		
	}
	
	function get_coments (){
		
		$this -> db -> Select('wp_posts.post_date, wp_posts.post_title, wp_posts.post_name');
		$this -> db -> where('post_status','publish');
		$this -> db -> not_like('post_title','BLOG');
		$this -> db -> order_by('wp_posts.ID', 'desc');	
		$this -> db -> limit(1);
		$query = $this -> db -> get('wp_posts');
		
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	function getCalendar(){
		$a = 'calendario';
		$query = $this->db->get($a);
		return ($query -> num_rows() >= 1)? $query -> result() : NULL;
	}

	function updateCalendar($idCalendario, $arrUpdate){
		$a = 'calendario';
		$this->db->where('idCalendario', $idCalendario);
		if($this->db->update($a, $arrUpdate)){
			return true;
		} else {
			return false;
		}
	}
	
}