<?php
   if(!defined('BASEPATH'))
	die('No');

class Revista_model  extends CI_Model{
	
	
	function __construct(){
		parent::__construct();
		$this -> load -> config('tables', TRUE);
		$this -> tablas = $this -> config -> item('tablas', 'tables');
		
	}
	
	function insertRevista ($data) {
		$this -> db -> insert('revista', $data);
		return true;
	}
	
	function updateRevista($idRevista,$array){
		$this->db->where('idRevista',$idRevista);
		$this->db->update('revista',$array);
		return true;
	}
	function deleteResvista($idRevista) {
		$this -> db -> where('idRevista', $idRevista);
		$this -> db -> delete('revista');
	}
	function getRevista (){
		$this -> db -> order_by('idSort','ASC');		
		$query = $this -> db -> get('revista');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	function updateIdSort($idRevista,$array) {
     	$this->db->where('idRevista',$idRevista);
		$this->db->update('revista',$array);
		return true;
				
     }
}