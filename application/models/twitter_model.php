<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Twitter_model extends CI_Model {

	function insertTweets($arrayInsert){
		$this->db->insert('twitter', $arrayInsert);
	}
	function deleteTweets($idTwitter){
		$this->db->where('idTwitter',$idTwitter);
		$this->db->delete('twitter');
	}
	function getTweets(){
		$query = $this->db->get('twitter');
		if ($query->num_rows()>=1)
			return $query->result();
		return null; 
	}

}

/* End of file twitter_model.php */
/* Location: ./application/models/twitter_model.php */