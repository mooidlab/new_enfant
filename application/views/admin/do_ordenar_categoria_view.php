<script type="text/javascript">
    <!--
    jQuery(document).ready(function($) {
<?php if (isset($error) || $this->session->flashdata('error')): ?>
            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);
<?php endif ?>

        /*
                Validacion de campos con JS.
                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
         */
        aformPro     = new Array();
        aformPro[0] = new Array('formPro', 'categoria'      , true, val_INPUT   , 'Categoría'       , -1, -1, 0, 0, null, true);
        aformPro[1] = new Array('formPro', 'titulo'     , true, val_INPUT   , 'Título'      , -1, -1, 0, 0, null, true);
        aformPro[2] = new Array('formPro', 'img'        , true, val_INPUT   , 'Imagen principal'        , -1, -1, 0, 0, null, true);
    
    });
    //-->
    
    jQuery(document).ready(function() {
        $("#sortableTable tbody").sortable({
            opacity: 0.6, cursor: 'move', update: function() {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings';
                $.post("<?=base_url()?>admin/directorio/do_ordenar", order, function(theResponse){
                	
					for (i =0; i<= theResponse.lenght ; i++){
                    $("#recordsArray_"+theResponse[i]).html(theResponse[i]);           
                    
                    }        
                });
            }
        });
        
        $("#categoria").change(
            function(){
                if($(this).val() != 'X'){                    
                    $(this).closest("form").submit();
                }                
            }
        );
    });
</script>

<br/>

<div class="box1">    
    <h1>Seleccione una Categor&iacute;a para Ordenar el directorio</h1>
    <form name="getCategory" id="getCategory" method="get" action="<?=base_url()?>admin/directorio/ordenar_do">
        <table class="formTable centerheadings">
            <tr>
                <td>
                    <label for="categoria">
                        Categor&iacute;a:
                    </label>
                </td>
                <td>
                    <select name="categoria" id="categoria">
                        <?php if($categorias != null):
                            ?><option value="X">Seleccione</option><?php                                                
                            foreach($categorias as $row): ?>                                
                                <option value="<?=$row->idCategoria?>" <?php if($row->idCategoria==$this->input->get('categoria')): echo ' selected="selected" '; endif;?>><?=$row->categoria?></option>
                            <?php endforeach;
                        endif; ?>
                    </select>
                </td>                
            </tr>        
        </table>
    </form>
    
    <h1>Arrastre la fila del cliente hasta el lugar deseado</h1>
    <table id="sortableTable" class="formTable centerheadings">
        <thead>
            <tr>
                <td> <strong> Logo </strong> </td>
                <td> <strong> Nombre </strong> </td>
                <td> <strong> Correo electrónico </strong> </td>
            </tr>            
        </thead>
        
        <tbody>
        <?php        
        if ($clientes != null) {
            foreach ($clientes as $row) {
                echo'
                <tr id="recordsArray_' . $row->idCliente . '">
                <td> <img src="' . base_url() . 'docs/cliente/' . $row->logo . '" alt="So close..." width="60" height="35" /></td>
                <td>' . $row->nombre . '</td>
                <td>' . $row->correo . '</td>                
                </tr>
                ';
            }
        }
        ?>
        </tbody>
    </table>
</div>