<script type="text/javascript">

    <!--

    jQuery(document).ready(function($) {

<?php if (isset($error) || $this->session->flashdata('error')): ?>

            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);

<?php endif ?>



        /*

                Validacion de campos con JS.

                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE

         */

        

    

    });

    //-->

    

    jQuery(document).ready(function() {

        var ocultar = false;
         $("#idSubcategoria").change(function(){
            var sub_cat = $(this).val();
            $("#btn_delete").attr('data-id',sub_cat);
            $.ajax({
              url: 'editSub',
              type: 'POST',
              dataType: 'json',
              data: {subCat: sub_cat},
              success: function(data) {
                //called when successful
                if (data.response == 'true') {
                    console.log(data);
                    var htmls ='<tr><td><label for="subCategoria">Sub Categoria</label></td><td><input type="text" name="subCategoria" id="subCategoria" value="'+data.obj.subCategoria+'"/></td></tr>';
                    if (data.obj.poner_guia == '1') {
                       htmls +='<tr><td><label for="option">Mostrar en:</label></td><td><select name="option" id="option"><option value="0">De todo un poco</option><option selected="selected" value="1">Guia zona poniente</option></select></td></tr>';
                    } else {
                       htmls +='<tr><td><label for="option">Mostrar en:</label></td><td><select name="option" id="option"><option selected="selected" value="0">De todo un poco</option><option value="1">Guia zona poniente</option></select></td></tr>';
                    }
                    if (data.obj.poner_guia == '1') {
                       htmls +='<tr><td><label for="color">Color de fondo:</label></td><td><select style="width:70px; background:'+data.obj.color+';" name="color" id="color">';
                        for (var i=0; i < data.colores.length ;i++) {
                            if (data.colores[i].color == data.obj.color) {
                                var select = 'selected="selected"';
                            } else {
                                var select = '';
                            }
                            htmls +='<option '+select+' value="'+data.colores[i].color+'" style="background:'+data.colores[i].color+';"></option>'
                        }
                        htmls +='</select></td></tr>';
                        htmls +='<tr><td><label for="option_two">Agregar Sub Sub Categoria?</label></td><td>';
                    if (data.obj.sub_sub_categoria != null) {
                        htmls+='Sí <input type="radio" checked="checked" name="option_sub_sub" value="1" class="option_sub_sub"> No <input type="radio" name="option_sub_sub" value="0" class="option_sub_sub">';
                    } else {
                        htmls+='Sí <input type="radio" name="option_sub_sub" value="1" class="option_sub_sub"> No <input type="radio" checked="checked" name="option_sub_sub" value="0" class="option_sub_sub">';
                    }
                        htmls +='</td></tr>';
                        if (data.sub_sub_categoria != null) {
                            
                            htmls+='<tr><td><label for="sub_sub_categoria">Sub Sub Categoria</label></td><td><input type="text" name="sub_sub_categoria" id="sub_sub_categoria"></td></tr>';
                           for (var a=0; a < data.sub_sub_categoria.length ;a++) {
                                htmls+='<tr id="sub_sub_'+data.sub_sub_categoria[a]+'"><td><label>'+data.sub_sub_categoria[a]+'</label></td><td><a class="borrar_tag" href="'+data.sub_sub_categoria[a]+'">borrar</a> <input type="hidden" value="'+data.sub_sub_categoria[a]+'" name="sub_sub_cat[]"/></td></tr>';
                           } 
                           
                        }
                    }
                    htmls+='<input name="idSubCategoria" type="hidden" value="'+data.obj.idSubCategoria+'" />'
                   $("#btn_delete").attr('data-id',data.obj.idSubCategoria);
                   $("#infoSub").html(htmls);
                   $('#color').val(data.obj.color);
                  
                } else {
                  alert('Error 404 ! consulte a su Webmaster'); 
                }
              }
            });
         });
       

                

        $("a.AddItem").click(function(){

            var box = $(this).attr("href");

            if(ocultar == false){

                $(box).fadeIn();

                ocultar = true;

            }

            else{

                $(box).fadeOut();

                ocultar = false;

            }

            return false;

        });

        

        $("a.borrar").click(function(){

            var id = $(this).attr("href");

            var len = id.lenght;

            id = id.substring(1,len);

            if(confirm("¿Está seguro de querer eliminar esta galería?")){

                window.location = "<?php echo base_url(); ?>admin/proyectos/borrarproyecto/"+id;

            }

            else{

                return false

            }

            return false;

        });

    });

</script>





<br/>

<div class="box1" id="addCatX">

    <h1>Editar SubCategoria</h1>

    <form name="formPro" id="formPro" method="post" action="<?php echo base_url() ?>admin/directorio/edit_do_subCategoria" enctype="multipart/form-data">
         <table class="formTable">
            <tr>
                <td><label for="idSubcategoria">Sub Categoría</label></td>
                <td><select name="idSubcategoria" id="idSubcategoria">
                        <?php
                        if ($categorias != null) {
                            echo'<option value="">--Seleccionar Sub categoría--</option>';
                            foreach ($categorias as $row) {
                                echo'
                                  <option value="' . $row->idSubCategoria . '">' . $row->subCategoria . '</option>
                                ';
                            }
                        } else {
                            echo '<option value="">--No se encontraron sub categorías--</option>';
                        }
                        ?>
                    </select></td>
            </tr>
           
        </table>
        <table class="formTable" id="infoSub">
            
             
        </table>
       
        
        <table class="formTable">
            <tr>
                <td><button type="submit">Editar</button></td>
                <td><a id="btn_delete" data-id="" href="deleteSubCat"><button>Eliminar</button></a></td>
            </tr>
        </table>

    </form>

</div>
<script type="text/javascript">
            function add_tag(){
            
            var tag = jQuery.trim(jQuery('#sub_sub_categoria').val());
            
            if ( tag != '' ) {
            
               jQuery('#infoSub').append('<tr id="sub_sub_'+tag+'"><td><label>'+tag+'</label></td><td><a class="borrar_tag" href="'+tag+'">borrar</a> <input type="hidden" value="'+tag+'" name="sub_sub_cat[]"/></td></tr>');
             } else{
                alert('no puedes agregar una subcategoria vacia');
             }
            
            

            jQuery('#sub_sub_categoria').val('');
        }
    jQuery(document).ready(function($) {
        $("#option").change(function(){
            var option = $(this).val();
            if (option == '1') {
                $("#option_guia_one").slideDown();
            }
        });
        $(".option_sub_sub").on( "click",function(){
            var value_radio = $(".option_sub_sub:checked").val();
            if (value_radio == '1') {
                $("#sub_sub_catego").slideDown();
            }
         });
        $(document).on("change","#color",function(){
           var color = $(this).val(); 
           $("#color").css('background',color);
        });
        $(document).on("keypress","#sub_sub_categoria",function (event){
            var key_code = (event.which === 0) ? event.keyCode : event.which;
            if (key_code === 13) {
                event.preventDefault();
                add_tag();
            }
            
        });
        $(".borrar_tag").live('click',function (e){
            e.preventDefault();
            var id_tag = $(this).attr('href');
            $("#sub_sub_"+id_tag).remove();
        });
        $("#btn_delete").live('click',function (e){
            e.preventDefault();
            var id_sub = $(this).attr('data-id');
            var url = $(this).attr('href');
            if (id_sub != '') {
                $.ajax({
                        url: url,
                        type: 'post',
                        data: {idSub:id_sub},
                        success: function (data) {
                           if (data.response === 'true') {
                            alert('Sub Categoria Eliminada');
                            window.location.href="<?base_url()?>admin/directorio/index_guia";
                           }else {
                             window.location.href="<?base_url()?>admin/directorio/index_guia";
                           }
                        }
                    });
            } else{
                alert('Elige una Sub Categoria');
            }
        });

        

    });
</script>