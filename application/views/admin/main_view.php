﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

    <head>

        <title><?= $SYS_metaTitle; ?></title>



        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 

        <meta name="author" 		content="Mooid.mx">

        <meta name="keywords" 		content="<?= $SYS_metaTitle; ?>">

        <meta name="description" 	content="<?= $SYS_metaDescription; ?>">	



        <link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/layout.css" type="text/css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jqueryUi.css" type="text/css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/facebox.css" type="text/css">

        <link href="<? echo base_url();?>static/css/admin/jquery.booklet.1.2.0.css" 	rel="stylesheet" type="text/css">

        <link rel="shortcut icon" href="<?php echo base_url(); ?>static/img/favicon.ico" type="image/x-icon">

        <script type="text/javascript" src="<? echo base_url();?>static/js/jquery-1.7.2.min.js"></script>

        <script type="text/javascript" src="<? echo base_url();?>static/js/jquery.booklet.1.2.0.min.js"></script>

        

        <script type="text/javascript" src="<?php echo base_url(); ?>js/jqueryUi.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/facebox.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/library.js"></script>

       <script type="text/javascript">

		jQuery(document).ready(function() {

        jQuery(function() {

					$("#mybook").booklet({

						width : 400,

						height : 300,

						speed : 1500,

						manual : false,

						keyboard : true,

						closed : true,

						autoCenter : true,

						pageNumbers : true,

						pagePadding : 0,

						arrows : false,

						tabs : false,

						tabWidth : 0,

						tabHeight : 0

					});		

		});

		});

		</script>		

        <?php

        //Caramos los arreglos para librerías/css dinámicos (propuesta)

        if (isset($css)) {

            for ($i = 0; $i < count($css); $i++) {

                echo '<link rel="stylesheet" href="' . base_url() . 'css/' . $css[$i] . '" type="text/css">';

            }

        }



        if (isset($librerias)) {

            for ($i = 0; $i < count($librerias); $i++) {

                echo '<script type="text/javascript" src="' . base_url() . 'js/' . $librerias[$i] . '"></script>';

            }

        }

        ?>

        <!-- Load Dynamic Libraries -->

        <?php if (isset($dinamicLibrary['autoComplete'])): ?>

            <link rel="stylesheet" href="<?php echo base_url(); ?>css/autocomplete.css" type="text/css">

            <script type="text/javascript" src="<?php echo base_url(); ?>js/autocomplete.js"></script>	

        <?php endif ?>



        <script type="text/javascript">

            //Arreglo que contiene el idioma para los errores

            arrLanError	 = new Array ();

<?php

for ($i = 0; $i <= 9; $i++):

    echo "arrLanError[" . $i . "] = \"" . ($this->lang->line('error_js_' . $i)) . "\";\n\t\t\t\t";

endfor

?>

        </script>

    </head>

    <body>

        <!--

        

        Designed by Mooid Lab SC

        http://www.mooid.mx/

        

        Titulo     : Enfant Directorio Infantil

        Version    : 1.10

        Fecha      : Septiembre/2012

        

        -->

    <body>

        <div id="bodyWrap">

            <div id="header">

                <div class="direccion">

                    <strong>Hola <?php echo $this->session->userdata('emailUsuario'); ?></strong>

                    <br>

                    <small>Estas en el Panel Administrativo.</small>

                    <br>

                    <a href="<?php echo base_url(); ?>sesion/logout/admin">Logout</a>

                    <a href="<?php echo base_url(); ?>">Usuarios</a>

                </div>

                <div class="logo_principal2 sprite3">

                  <strong>Directorio Infantil   </strong>

                </div>			

            </div>

            <div id="content">

                <div class="box">

                    <div class="boxMenu">

                        <h1>Dashboard</h1>

                        <ul>

                            <li><a href="<?php echo base_url(); ?>admin/" class="<?php if ($pestana == 1)

    echo "active"; ?>">Mis datos</a></li>    

                        </ul>

                        <h1>Directorio</h1>

                        <ul>

                            <li><a href="<?php echo base_url(); ?>admin/directorio/agregar" class="<?php if ($pestana == 2)

                                       echo "active"; ?>">Nuevo Cliente</a></li>                            

                            <li><a href="<?php echo base_url(); ?>admin/directorio/" class="<?php if ($pestana == 3)

                                       echo "active"; ?>">Ver Directorio</a></li>

                            <li><a href="<?php echo base_url(); ?>admin/directorio/ordenarcategoria" class="<?php if ($pestana == 4)

                                       echo "active"; ?>">Ordenar Directorio</a></li>

                        </ul>  

                        <h1>Banners</h1>

                        <ul>

                            <li><a href="<?php echo base_url(); ?>admin/banner/agregar" class="<?php if ($pestana == 5)

                                       echo "active"; ?>">Nuevo Banner</a></li>                            

                            <li><a href="<?php echo base_url(); ?>admin/banner/" class="<?php if ($pestana == 6)

                                       echo "active"; ?>">Ver Banners</a></li>

                            

                        </ul>

                        <h1>Guía Zona Poniente</h1> 

                          <ul>

                            <li><a href="<?php echo base_url(); ?>admin/directorio/agregarClienteSubCategoria" class="<?php if ($pestana == 7)

                                       echo "active"; ?>">Nuevo Cliente</a></li>   
                                                  

                           <li><a href="<?php echo base_url(); ?>admin/directorio/agregarSubcategoria" class="<?php if ($pestana == 7)

                                       echo "active"; ?>">Nueva Sub Categoria</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/directorio/EditarSubcategoria" class="<?php if ($pestana == 7)

                                       echo "active"; ?>">Editar Sub Categoria</a></li>
                                                       
                           <li><a href="<?php echo base_url(); ?>admin/directorio/index_guia" class="<?php if ($pestana == 8)

                                    echo "active"; ?>">Ver Guía zona poniente</a></li>

                           

                            <li><a href="<?php echo base_url(); ?>admin/directorio/ordenarSubcategoria" class="<?php if ($pestana == 9)

                                       echo "active"; ?>">Ordenar Guia Zona Poniente</a></li>

                        </ul> 

                        <h1>Revista On-Line</h1> 

                          <ul>

                            <li><a href="<?php echo base_url(); ?>admin/revista/agregar" class="<?php if ($pestana == 10)

                                       echo "active"; ?>">Nueva Pagina</a></li>                            

                            <li><a href="<?php echo base_url(); ?>admin/revista/" class="<?php if ($pestana == 11)

                                       echo "active"; ?>">Ver Revista</a></li>

                            <li><a href="<?php echo base_url(); ?>admin/revista/ordenar_do" class="<?php if ($pestana == 12)

                                       echo "active"; ?>">Ordenar Paginas</a></li>

                        </ul> 

                        <h1>Cuponera</h1> 

                          <ul>

                            <li><a href="<?php echo base_url(); ?>admin/cuponera/agregar" class="<?php if ($pestana == 10)

                                       echo "active"; ?>">Nuevo Cupon</a></li>                            

                            <li><a href="<?php echo base_url(); ?>admin/cuponera/ver" class="<?php if ($pestana == 11)

                                       echo "active"; ?>">Ver Cuponera</a></li>

                            

                        </ul> 
                        <h1>Calendario</h1>

                        <ul>

                            <li><a href="<?php echo base_url(); ?>admin/banner/calendario" class="<?php if ($pestana == 15)

                                       echo "active"; ?>">Modificar Imagen de calendario</a></li>   

                            

                        </ul>
                        <h1>Contador de Visitas</h1> 

                        <ul>   

                        <li>Visitas <?=$contador?></li>                		

                        </ul>

                    </div>

                    <div class="boxModul">

                        <?php $this->load->view($module); ?>

                    </div>

                </div>		

            </div>

        </div>

    </body>

</html>