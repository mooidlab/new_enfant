
<a href="#addCat" class="AddItem">Nuevo Cliente</a>

<br/>
<div class="box1" id="addCatX">
    <h1>Agregar Cupon(es)</h1>
    <form name="formPro" id="formPro" method="post" action="<?php echo base_url() ?>admin/cuponera/agregar_do" enctype="multipart/form-data">
       
        <table class="formTable">
            <tr>
                <td><label for="idCliente">Cliente</label></td>
                <td><select name="idCliente" id="idCliente">
                        <?php
                        if ($losClientes != null) {
                            echo'<option value="">--Seleccionar categoría--</option>';
                            foreach ($losClientes as $row) {
                                echo'
                                  <option value="' . $row->idCliente . '">' . $row->nombre . '</option>
                                ';
                            }
                        } else {
                            echo '<option value="">--No se encontraron categorías--</option>';
                        }
                        ?>
                    </select></td>
            </tr>
            <tr>
                <td><label for="noCupones">Cupones a Agregar</label></td>
                <td>
                    <select name="noCupones" id="noCupones">
                    <?php
                        for($i=1 ; $i <= 20 ; $i++){
                          echo'
                                  <option value="' . $i . '">' . $i. '</option>
                                ';   
                        }
                    ?>
                    </select>
                </td>
            </tr>
           
        </table>
        <table class="formTable" id="laCuponera">

        </table>
    </form>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {
    $('#noCupones').change(function(){
        var numero_cupones = $(this).val();
        $("#laCuponera").load('<?=base_url()?>admin/cuponera/generar_input/'+numero_cupones+'/');
    });
});
</script>