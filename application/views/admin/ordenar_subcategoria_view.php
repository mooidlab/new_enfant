<script type="text/javascript">
    <!--
    jQuery(document).ready(function($) {
<?php if (isset($error) || $this->session->flashdata('error')): ?>
            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);
<?php endif ?>

        /*
                Validacion de campos con JS.
                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
         */
        aformPro     = new Array();
        aformPro[0] = new Array('formPro', 'categoria'      , true, val_INPUT   , 'Categoría'       , -1, -1, 0, 0, null, true);
        aformPro[1] = new Array('formPro', 'titulo'     , true, val_INPUT   , 'Título'      , -1, -1, 0, 0, null, true);
        aformPro[2] = new Array('formPro', 'img'        , true, val_INPUT   , 'Imagen principal'        , -1, -1, 0, 0, null, true);
    
    });
    //-->
    
    jQuery(document).ready(function() {
        var ocultar = false;
        $("#addCat").hide();
                
        $("a.AddItem").click(function(){
            var box = $(this).attr("href");
            if(ocultar == false){
                $(box).fadeIn();
                ocultar = true;
            }
            else{
                $(box).fadeOut();
                ocultar = false;
            }
            return false;
        });
        
        $("a.borrar").click(function(){
            var id = $(this).attr("href");
            var len = id.lenght;
            id = id.substring(1,len);
            if(confirm("¿Está seguro de querer eliminar esta galería?")){
                window.location = "<?php echo base_url(); ?>admin/directorio/delete/"+id;
            }
            else{
                return false
            }
            return false;
        });
        
        $("#categoria").change(
            function(){
                if($(this).val() != 'X'){                    
                     if($(this).val() != '0'){                    
                    $(this).closest("form").submit();
                    }else{
                        $('#dirDeTodo').fadeIn();
                    }
                }
               
            }
        );
        $("#dirDeTodo").change(
            function(){
                $(this).closest("form").submit();
            }
        );
    });
</script>

<br/>

<div class="box1">    
    <h1>Seleccione una Categor&iacute;a para Ordenar el directorio</h1>
    <form name="getCategory" id="getCategory" method="get" action="<?=base_url()?>admin/directorio/ordenar_do_subcategoria">
        <table class="formTable centerheadings">
            <tr>
                <td>
                    <label for="categoria">
                       Categor&iacute;a:
                    </label>
                </td>
                <td>
                    <select name="categoria" id="categoria">
                        <?php if($categorias != null):
                            ?><option value="X">Seleccione</option>
                            <option value="0">De todo Un Poco</option><?php
                            foreach($categorias as $row): ?>
                                <option value="<?=$row->idSubCategoria?>"><?=$row->subCategoria?></option>
                            <?php endforeach;
                        endif; ?>
                    </select>
                </td>      
                <td>
                    <select name="dirDeTodo" id="dirDeTodo" style="display:none;">
                        <?php if($dirDetodo != null):
                            ?><?php
                            foreach($dirDetodo as $row): ?>
                                <option value="<?=$row->idSubCategoria?>"><?=$row->subCategoria?></option>
                            <?php endforeach;
                        endif; ?>
                    </select>
                </td>            
            </tr>        
        </table>
    </form>
</div>