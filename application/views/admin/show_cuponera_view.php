<div class="box1">
    <h1>Cuponera    (Para Ordenar solo arrastra el cupon a la pagina que gustes.)</h1>
	<div class="laCuponera">
		<?php
		$top= 1;
		$pagina=1;
		echo '<h1>Pagina '.$pagina.'</h1>';
		if (!is_null($cupones)) {
			foreach ($cupones as $row) {
				if($top >= 13){
					$pagina++;
					
					echo '<h1>Pagina '.$pagina.'</h1>';
					
					$top=1;
					
				}
				$texto = ($row->publish == 1)?'Habilitado':'Deshabilitado';

				echo '
					<div class="cupon" id="recordsArray_'.$row->idCuponera.'">

						<img src="'.base_url().'docs/cuponera/'.$row->cupon.'"/>
						<a href="'.base_url().'admin/cuponera/editar/'.$row->idCuponera.'">editar</a>
						<a href="'.$row->idCuponera.'" class="estado_cupon" id="">'.$texto.'</a>
					</div>
					';
				$top++;
			}
		} else {
			echo 'No se han agregado cupones.';
		}
		
		
		?>
	</div>
	<div class="controles">
		
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
	 $(".laCuponera").sortable({
            opacity: 0.6, cursor: 'move', update: function() {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings';
                $.post("<?=base_url()?>admin/cuponera/do_ordenar", order, function(theResponse){
                   // $(".boxModul").html(theResponse);                   
               
               
                });
            }
        });
	
	$(".estado_cupon").live('click', function (e) {
        e.preventDefault();
        var id_cupon = $(this).attr('href');
        var me = $(this);
        $.ajax({
            url      : 'change_status/',
            type     : 'POST',
            dataType : 'json',
            data     : 'idCuponera='+id_cupon,
            success  : function(data){
                if(data.response == 'true'){
                    me.html(data.string);
                }
            }
        });
    });
	
});
</script>
