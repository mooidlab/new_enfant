<div id="bg_beneficios">
	<h1 >Anunciate</h1>
	<div class="head">
		<h2>Elige uno de nuestros 3 paquetes o...</h2> <a href="<?=base_url()?>docs/docs/presentacion_enfant.pdf"></a>
	</div>
	<ul class="list">
		<li>4 tipos de Banners en nuestra página web www.enfant.com.mx.</li>
		<li style="color:#67B6F5;">Publicar artículos en nuestro Blog.</li>
		<li>Tener un Mini sitio con galeria de fotos, geo localización y redireccionamiento a la página del anunciante y sus redes sociales.</li>
		<li style="color:#67B6F5;">Promoción continua en: Facebook, Twitter, Instagram y Pinterest.</li>
		<li>Entrega de estadísticas de comportamiento de la página y reporte de clicks por cliente.</li>
		<li style="color:#67B6F5;">Newsletter unico o compartido, llegando directamente a los correos de nuestra base de datos.</li>
	</ul>

</div>