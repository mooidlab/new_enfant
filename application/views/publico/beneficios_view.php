<div id="bg_beneficios">

		

	<h1 >Beneficios</h1>

	<p class="justify">
		<span style="color:#666;">Magnífica oportunidad de negocio poniéndolo en contacto directo con un segmento de mercado exigente y con hábitos de consumo constantes.</span> </br>
		<span style="color:#5EC2A5;">Es la única guía cuyos anunciantes pasan por un filtro meticuloso de selección, dando como resultado una agrupación de prestadores de servicios de primer nivel.</br>
		Le permite al anunciante generar alguna promoción específica para su producto o servicio a través de redes sociales, banners, mailing y eventos mensuales.</span> </br>
		<span style="color:#666;">Es la única guía que cuenta con 4 redes sociales que impulsan el impacto de las promociones logrando llegar a un mercado mas diversificado.</span> </br>
		<span style="color:#5EC2A5;">Se invierte en buscadores y publicidad pagada en redes sociales para posicionar a nuestros clientes.</span> </br>
		<span style="color:#666;">Le permite al anunciante tener un mini sitio en nuestra página <a href="<?=base_url()?>">www.enfant.com.mx</a> con galeria de hasta 6 fotos y geo localización</span>
	  
	</p>

</div>