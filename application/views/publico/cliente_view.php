
<?php if($row != null): ?>							
<?foreach($row as $result):	
	if ($result->latitud != NULL  && $result->latitud != '0' ) {
	$latitud = $result->latitud;
	} else {
	$latitud = '20.5968';   
	}
	if ($result->longitud != NULL  && $result->longitud != '0' ) {
	$longitud = $result->longitud;
	} else {
	$longitud = '-100.3921';   
	}

						?>	
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi?key=AIzaSyCa3tY3h1ORqUjz0_VFxsLgQSjBdiWkazY"></script>
        <script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/googleearth/src/googleearth-compiled.js"></script>
        <script type="text/javascript">
          function initialize() {
            var mapOptions = {
              zoom: 9,
              minZoom: 9,
              maxZoom: 18,
              center: new google.maps.LatLng(<?=$latitud?>, <?=$longitud?>),//y=>Latitude, x=>Longitud
              mapTypeControl: false,
              mapTypeControlOptions: {
                 style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                 position: google.maps.ControlPosition.TOP_RIGHT
              },
              rotateControl: false,
              zoomControl: true,
              zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.TOP_RIGHT
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var styles = [{
                    featureType: "all",
                    elementType: "all",
                    stylers: [
              { saturation: 0 }
            ]
                }];
            var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
            var mapType = new google.maps.StyledMapType(styles);
                map.mapTypes.set("map_canvas", mapType);
                map.setMapTypeId("map_canvas");

            var image = '<?=base_url()?>static/img/marcadore.png';

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(<?=$latitud?>, <?=$longitud?>),
                icon: image,
                title: "<?=$result->nombre?>"
            });
      
            // To add the marker to the map, call setMap();
            marker.setMap(map); 

            google.maps.event.addListener(marker, 'position_changed', function() {
                document.getElementById('latitud').value = marker.getPosition().lat();
                document.getElementById('longitud').value = marker.getPosition().lng();
              });
          }
          window.onload = initialize
</script>	
<div id="clientes" style="height:auto !important;">			
	<div class="contenido1">				
		<div class="texto">					
			<div class="titulo">						
				<h1 style="color:<?=$color?> !important;" ><?=$result->nombre?></h1>					
			</div>					
			<div class="descripcion">						
				<p>	
				<? 
					if ($result->p != NULL && $result->p != '0') {
						echo $result->p ;
					}
				?>							
				</p>
			</div>
		</div>				
		<div class="imagen">					
			<img src="<? echo base_url();?>images/thumbnailer/200/120/cliente/<?=$result->logo?>" alt="<?=$result->nombre?>">				
		</div>			
	</div>			
	<div class="contenido2">				
		<div class="espacio"></div>				
		<div class="contenedor_gal">					
			<div id="MCarousel">						   
			<?php if($rowimg != null): 						    							    	
			foreach($rowimg as $row1):									
			if($result->idCliente == $row1->idCliente)									
			{					
			?>												
				<div class="box"> 						
					<div class="galeria">													
					</div>						
					<div class="foto" style="background: url(<? echo base_url();?>images/thumbnailer/350/260/cliente/<?= $row1->imagen?>)no-repeat;">  
					</div>						
				</div>						
			<?									
			}						
			endforeach;?>						
			<?php endif; ?>						
			</div>				
		</div>				
		<div class="btn_gal">					
			<!-- id="next"  -->					<!-- id="previous"  -->					
			<input name="next" id="next"  type="button" value="" class="btn_gal_next"/>					
			<input name="previous" id="previous"  type="button" value="" class="btn_gal_back" />				
		</div>			
	</div>
	<div class="contenido3">				
		<div class="leinfo1">					   
			<table>	
			<?php
			echo '<tr>'; 
			if ($result->direccion != NULL && $result->direccion != '0'){
			 echo '<td class="titulo" style="color:'.$color.' !important;">';
			 echo '<b>Dirección:</b>'; 	
			 echo '</td>';	
			 echo '<td class="descr">';
			 echo $result->direccion;	
			 echo '</td>';				                       
			}
			echo '</tr>';					                       						
			echo '<tr>'; 
			if ($result->telefono != NULL && $result->telefono != '0'){
			 echo '<td class="titulo" style="color:'.$color.' !important;">';
			 echo '<b>Teléfono:</b>'; 	
			 echo '</td>';	
			 echo '<td class="descr">';
			 echo $result->telefono;	
			 echo '</td>';                     
			}
			echo '</tr>';					                       						
			echo '<tr>'; 
			if ($result->correo != NULL && $result->correo != '0' ){
   			 echo '<td class="titulo" style="color:'.$color.' !important;">';
			 echo '<b>Correo:</b>'; 	
			 echo '</td>';	
			 echo '<td class="descr">';
			 echo $result->correo;	
			 echo '</td>';
			}
			echo '</tr>';	
			echo '<tr>';
			if ($result->web != NULL && $result->web != '0' ){
			echo '
			<td class="titulo" style="color:'.$color.' !important;"><b>Web:</b></td>
			<td class="descr"><a target="_BLANK" href="http://'.$result->web.'" style="color:<?=$color?> !important;">'.$result->web.'</a></td>';
			}
			echo '</tr>';
			?>					

			</table>				
		</div>		
		<div class="leinfo2">
			<div id="map_canvas" style="width: 370px !important; height: 200px !important;"></div>
		</div>		
		<div class="leinfo3">					
			<style>						
			.facebook:hover{color:<?=$color?> !important;}						
			.twitter:hover{color:<?=$color?> !important;}					
			</style>										
			<?php if (($result->facebook != null) && ($result->facebook != 'http://www.facebook.com/')) {?>					
			<a target="_blank"  href="<?= $result->facebook?>" class="facebook">Facebook</a> 
			&nbsp;&nbsp;					
			<?php }					      
			if  (($result->twitter != null) && ($result->twitter != 'http://www.twitter.com/')){					
			?>					
			<a target="_BLANK"  href="<?= $result->twitter?>" class="twitter">Twitter</a>					
			<?php } ?>									
		</div>				
	</div>	
</div>
<div class="paginado">					
<?php echo $this -> pagination -> create_links();?>				
</div>		
<?endforeach?>						
<?php endif; ?>		