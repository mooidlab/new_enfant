<script  type="text/javascript">
<? $totalpaginas=1;?>
jQuery(document).ready(function($) {
		var total_child ;
		var per_page;
		var page;
		var anime;
		total_child = $('#masonry-container > .masonry-grid').size();
		per_page= Math.ceil(total_child/3);
		for (i=1 ; i <= per_page ; i++){
			$('.paginado').append('<div class="bullet" id="'+i+'"></div>');
		}
		$('.bullet').click(function(){
				page = $(this).attr('id');
				anime = (page-1)*720;
				$('#masonry-container').stop(true,true).animate({
					left: -anime+'px'
				});
			});
		
		$('.todo').click(function(){
			$('.relleno').stop(true,true).fadeOut(150);
				id = $(this).attr('id');
				$('#todo'+id+' > h4').css('color','#fff');
				$('#todo'+id).css('background','#87296F');
				
				$('#relleno'+id).stop(true,true).fadeIn(150);
			});
		$('.relleno').mouseleave(function(){
				id = $(this).attr('data-id');
				$('#todo'+id+' > h4').css('color','');
				$('#todo'+id).css('background','');
				$('#relleno'+id).stop(true,true).fadeOut(150);
			});

	});	
</script>
<div class="wrappmaqueta2">

		<div class="primer_con_2">
				<div  class="logotipo2">
					<a href="#" class="logo_principal2 sprite3"></a>
				</div>
				<div id="search2" >
					<form  method="post" action="<?=base_url();?>buscar/prebuscar" >
						<fieldset class="search2_bg sprite3" style="border:0; margin: 2px 0 0 -4px;">
							<legend></legend>
							<input class="search2_input sprite3" type="text" name="palabraClave">
				<input class="search2_send_btn sprite3" type="submit" value=" ">
				</fieldset>
					</form>
				</div>
				<div id="leftmenu2">
					<ul>
						<li>
							<a  class="clases2_btn sprite3" href="<?=base_url();?>clientes/clases/1"></a>
						</li>
						<li>
							<a href="<?=base_url();?>clientes/clases/2" class="salud2_btn sprite3"></a>
						</li>
						<li>
							<a href="<?=base_url();?>clientes/clases/3" class="mama2_btn sprite3"></a>
						</li>
						<li>
							<a href="<?=base_url();?>clientes/clases/4" class="diversion2_btn sprite3"></a>
						</li>
						<li>
							<a href="<?=base_url();?>clientes/clases/5" class="fiesta2_btn sprite3"></a>
						</li>
						<li>
							<a href="<?=base_url();?>clientes/clases/6" class="ropa2_btn sprite3"></a>
						</li>
						<li>
							<a href="<?=base_url();?>deTodounPoco/clases/7" class="todo2_btn sprite3"></a>
						</li>
					</ul>
				</div>
				<div id="formulario"  >
					<h1 class="letra1m2"> Participa</h1>
				
					<form method="post" class="" id="participa" action="">
						<fieldset style=" border:0; margin-top:-29px;"class="contact_bg sprite3" >
							<legend></legend>
						<p class="letra3">
							Sí quieres participar en promociones
							exclusívas de Enfant, deja tus datos:
						</p>
					
						<input type="text" id="form_mail" name="email" value="Mail" class="contact_input sprite3" style="border:0;"/>
						<br>
						<input type="text" id="form_name" name="nombre" value="Nombre" class="contact_input sprite3" style="border:0;"/>
						<br>
						<select name="estado"  class="contact_input contact_dropdown_btn sprite3" style="border:0;">
							<option value="" selected>Lugar de Residencia</option>
							<option value="Polanco">Polanco</option>
							<option value="Interlomas">Interlomas</option>
							<option value="Bosques de Las Lomas">Bosques de Las Lomas</option>
							<option value="Herradura">Herradura</option>
							<option value="Vista Hermosa">Vista Hermosa</option>
							<option value="Santa Fe">Santa Fe</option>
							<option value="Contadero">Contadero</option>
							<option value="Cuajimalpa">Cuajimalpa</option>
							<option value="Tecamachalco">Tecamachalco</option>
						</select>
						<br>
							<select name="hijos"  class="contact_input contact_dropdown_btn sprite3"style="border:0;">
								<option value="" selected>Hijos</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
						</select>
						<input class="contact_btn sprite3" type="submit" style="border: 0pt; z-index: 1; margin-top: 16px; float: left; margin-left: 136px;" value=" ">
					
				</fieldset>
				</form>
			</div>
			</div>
			<div class="segundo_con_2">
				<div id="menu_con2">
					<ul style="">
						<li>
						<a href="<? echo base_url();?>" style="float: right;">Inicio</a>
					</li>
					<li>
						<a href="<? echo base_url();?>revista" style="float: right;">Revista On-Line</a>
					</li>
					<li>
						<a href="<? echo base_url();?>nosotros" style="float:right;">¿Quiénes Somos?</a>
					</li>
					<li>
						<a href="<? echo base_url();?>beneficios" style="float: right;">Beneficios</a>
					</li>
					<li>
						<a href="<? echo base_url();?>socialresponsable" style="float: right;">Socialmente Responsable</a>
					</li>
					<li>
						<a href="<? echo base_url();?>contacto" style="float: right;">Contacto</a>
					</li>
					</ul>
				</div>
					
				<div id="clientes">
					<div class="encabezado"> <span style="color:#666;">Directorio Infantil > </span><span style="color:<?=$color?>;"><?=$categoria.' '?></span><span style="color:#666;"> > </span><span style="color:<?=$color?>;"><?=' '.$sub_categoria?></span></div>
				    <?php 
				   if ($cliente!=null){
				   	$for_limit = 10;
				   	$bg_color = 'a'; 
				   	$c = 0;
				   ?>
				    <div class="detodo" id="detodo" style="overflow: hidden; !important;"> 
				    	<div id="masonry-container" style="width: 2000% !important; height: 610px !important; position:relative;">
				    		<div class="masonry-grid" style="position: relative; width: 220px;  height: 610px; margin: 0 10px 0 10px; float: left;">

<? 
				     				foreach($cliente as $row) : 

				     					
				     						if($c > $for_limit){
				    					echo '
				    					</div>
				    					<div class="masonry-grid" style="position: relative; width: 220px;  height: 610px; margin: 0 10px 0 10px; float: left;">
				    					';
				    					$c=0;
				    				      }
			     						$c++;
				     				?>
				     					<div class="todo-content-n row-<?=$bg_color?>" id="todo<?=$row->idClienteSubCatego?>">
					     		          	<h4 class="todo" id="<?=$row->idClienteSubCatego?>" >
					     		          		<?=substr($row->nombre, 0, 26) .'..'?>
					     		          	</h4>

					     		          	
			     		          		</div>
			     		          		<div class="relleno" id="relleno<?=$row->idClienteSubCatego?>" data-id="<?=$row->idClienteSubCatego?>" style="display: none;">
						     		          	<div class="informacion">
						     		          		<p>				     		          		
							     		          		<?=($row->nombre!=NULL)      ? '<b>Nombre: </b> ' . $row->nombre .'<br>': ''?>
							     		          		<?=($row->dir1!=NULL)      ? '<b>Direccion: </b> ' . $row->dir1 .'<br>': ''?>
							     		          		<?=($row->telefono!=NULL)  ? '<b>Telefono: </b> ' . $row->telefono .'<br>': ''?>
							     		          		<?=($row->telefono2!=NULL) ? '<b>Telefono: </b> ' . $row->telefono2 .'<br>': ''?>
							     		          		<?=($row->email!=NULL)     ? '<a target="_blank" href="http://'. $row->email.'">'.$row->email.'</a><br>': ''?>
						     		          		</p>
				     		          			</div>
			     		          			</div>
				     				<? 
				     				$bg_color = ($bg_color == 'a') ? 'b' : 'a'; 
				     				
				     				endforeach; 
?>

									</div>
					     	
						</div>				  
				    </div>
				    <div class="paginado">
						<a href="<?=base_url();?>deTodounPoco/clases/7" class="regresar">Regresar</a>
						</div>
						<?php }else{?>
						<div class="encabezado" ><span style="color:<?=$color?>;">Proximamente..</span></div>
						<?php }?>
			    </div>
				
				<div class="kid sprite3" style="margin-left:715px; margin-top: -5px;"> 
				<!-- <div class="shadowr" style="height:583px !important; margin-left: 75px;"></div>-->
				<a href="http://www.facebook.com/pages/Enfant-Directorio-Infantil/370827672944989" class="socialFB_btn1 sprite" style="margin-left: 70px; margin-top: 68px;   position: absolute; z-index:100;"> </a>
				<a href="http://www.tumblr.com/"  class="socialTh_btn1" style="margin-left:70px; margin-top: 125px; position: absolute; z-index:100;"> </a>
				<a target="a_blank"  href="http://pinterest.com/enfantmexico/"  class="socialPint_btn1" style="margin-left:52px; margin-top: 140px; position: absolute;"> </a>
				<a href="http://twitter.com/#!/EnfantMx"  class="socialT_btn1 sprite" style="margin-left:70px; margin-top: 190px; position: absolute; z-index:100;"> </a>
				<!-- <div class="shadowb" style="margin-top:646px !important;"></div>-->
					</div>
			</div>
			
	
	<div class="clearer"></div>
</div>