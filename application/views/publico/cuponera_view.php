
<div id="clientes">
<div class="encabezado"> <span style="color:#666;">Directorio Infantil > </span><span style="color:<?=$color?>;"><?=$categoria?></span></div>
<?php 
if ($cupones!=null){
$per_page = 1;
?>
    <div class="detodoCupon" id="detodo" style="overflow: hidden; !important;">
        <div class="laCuponera">
            <?php
                echo '<div class="contenedor">';
                foreach ($cupones as $row):
                    if($per_page == 13){
                       echo '</div>'; 
                       echo '<div class="contenedor">';
                       $per_page = 1;
                    }
                    echo '
                        <div class="cupon">
                            <img src="'.base_url().'docs/cuponera/'.$row->cupon.'"/>
                        </div>
                        ';
                    $per_page++;
                endforeach;
                echo '</div>';
            ?>
        </div>
        <div class="controles">

        </div> 
        <div class="imprimir">
            <a id="btn_imprimir" href="#">Imprimir Cuponera</a>
            <form target="_blank" action="<?=base_url()?>cuponera/imprimir" method="post" id="form_print">
                <table>
                        <tr>
                            <td> <label for="email">Correo Eléctronico:</label> </td>
                            <td> <input type="text" name="email" id="email"/> </td>
                            <td> <input type="submit" value="Imprimir"> </td>
                        </tr>
                </table>    
                
                

            </form>
        </div>
    </div>
<?php } else {?>
<div class="encabezado" ><span style="color:<?=$color?>;">Proximamente..</span></div>
<?php }?>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('#btn_imprimir').click(function (e){
        e.preventDefault();
        $('#btn_imprimir').stop(true,false).slideUp();
        $('#form_print').stop(true,false).slideDown();
    });

    $('#form_print').submit(function (e){
        e.preventDefault();
        var email= $.trim($("#email").val()); 
        if(email == ''){
            alert('Debes ingresar un correo valido.');
        }else{
            
          $.ajax({
            url      : 'bridge/',
            type     : 'POST',
            dataType : 'json',
            data     : 'email='+email,
            success  : function(data){
                if(data.response == 'true'){
                   window.open('<?=base_url()?>cuponera/imprimir/');
                }else{
                    alert('ya imprimiste la cuponera este mes!');
                }
            }
         });
        }
    });


    var total_child ;
        var per_page;
        var page;
        var anime;
         total_child = $('.laCuponera > .contenedor').size();
            
            for (i=1 ; i <= total_child ; i++){
                    $('.controles').append('<a href="" class="button_controles" id="'+i+'">'+i+'</a>');
            }
            $('.button_controles').click(function(e){
                e.preventDefault();
                            page = $(this).attr('id');
                            anime = (page-1)*785;
                            $('.laCuponera').stop(true,true).animate({
                                    left: -anime+'px'
                            });
            });
});
</script>
       
