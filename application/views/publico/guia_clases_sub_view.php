 <div id="clientes" style="background:#DDF0EA;">
 	<?php if ($clientes != null):?>
<div class="encabezado"> <span style="color:#666;">Directorio Infantil > <a href="<?=base_url()?>guia_zona_poniente/" class="guia_zona_poniente_a" style="color:<?=$color?>;">Guía Zona Poniente</a> > </span><span style="color:<?=$color?>;"><?=$categoria?></span></div>

<div class="detodoCupon" id="detodo" style="overflow: hidden; !important;"> 
	
	<div class="laCuponera">
	    <?php
	    if ($llego == '1') {
	    	$class = 'a';
	   		$per_page = 1;
	        echo '<div class="contenedor">';
	        	echo '<div class="guia" style="width:270px;">';
	        foreach ($sub_sub_cat as $row):
	            if($per_page == 13){
	               echo '</div>'; 
	               echo '<div class="guia" style="width:270px;">';
	               $per_page = 1;
	            }

	            echo '
	                <div class="clase_guia " style="width:270px;">
	                   <h3 style="color: '.$color.'; ">'.$row->subCategoria.'</h3>
	                   <a class="btn_vermas" style="background-color:'.$color.';" href="'.base_url().'guia_zona_poniente/show_sub_cat/'.$row->idSubCategoria.'?a=0"></a>
	                </div>
	                <span class="line_dotted" style="width:270px;"></span>
	                ';
	            $per_page++;
	            $class = ($class == 'a') ? 'b' : 'a';
	        endforeach;
	        echo '</div>';
	        echo '</div>';
	    } else {
	    	$class = 'a';
	    $per_page = 1;
	        echo '<div class="contenedor">';
	        	echo '<div class="guia">';
	        for ($i=0 ; $i < count($sub_sub_cat) ; $i ++):
	            if($per_page == 13){
	               echo '</div>'; 
	               echo '<div class="guia">';
	               $per_page = 1;
	            }

	            echo '
	                <div class="clase_guia " >
	                   <h3 style="color: '.$color.'; ">'.$sub_sub_cat[$i].'</h3>
	                   <a class="btn_vermas" style="background-color:'.$color.';" href="'.base_url().'guia_zona_poniente/show_sub_cat/'.$idSubCategoria.'?a='.htmlentities(json_encode($sub_sub_cat[$i])).'"></a>
	                </div>
	                <span class="line_dotted"></span>
	                ';
	            $per_page++;
	            $class = ($class == 'a') ? 'b' : 'a';
	        endfor;
	        echo '</div>';
	        echo '</div>';
	    }
	    
	    ?>
	</div>
	
	
</div>

<?php else:?>
<div class="encabezado"> <span style="color:#666;">Directorio Infantil > <a href="http://enfant.com.mx/guia_zona_poniente/" class="guia_zona_poniente_a" style="color:<?=$color?>;">Guía Zona Poniente</a> > </span><span style="color:<?=$color?>;">No se encontraron Resultados</span></div>

	<?php endif;?>

</div>
<div class="controles_guia"></div> 
<script type="text/javascript">
jQuery(document).ready(function($) {
	 var total_child ;
        var per_page;
        var page;
        var anime;
         total_child = $('.laCuponera > .contenedor').size();
            
            for (i=1 ; i <= total_child ; i++){
                    $('.controles_guia').append('<a href="" class="bullet" id="'+i+'"></a>');
            }
            $('.bullet').click(function(e){
                e.preventDefault();
                            page = $(this).attr('id');
                            anime = (page-1)*785;
                            $('.laCuponera').stop(true,true).animate({
                                    left: -anime+'px'
                            });
            });
            var total_bullet = $(".controles_guia > .bullet").size();
            if (total_bullet < 2) {
            	$(".controles_guia").css('display','none');
            };
});
</script>