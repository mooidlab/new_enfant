<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<!--

		Designed by Mooid Lab
		http://www.mooid.mx/

		Titulo     : Proyecto
		Version    : 0.0
		Fecha      : Jan/2012

		-->	
<meta http-equiv="Content-Type" content="text/html; charset= UTF-8">		
<meta name="author" content="Mooid Lab S.C.">		
<meta name="keywords" content="<?=$SYS_metaKeyWords?>">		
<meta name="description" content="><?=$SYS_metaDescription?>">		
<script src="<? echo base_url();?>static/js/jquery-1.7.2.min.js"></script>			
 	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.jcarousel.js"></script>	
	<script type="text/javascript" src="<? echo base_url();?>static/js/coin-slider.min.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.msCarousel.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.msCarousel-min.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.easing.1.3.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/button_selects.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.booklet.1.2.0.min.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.noty.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/center.js"></script> 		
	<script type="text/javascript" src="<? echo base_url();?>static/js/default.js"></script> 		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/layoutEnfant.css">		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/fonts.css">		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/skin.css">		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/jquery.noty.css">		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/noty_theme_default.css"/>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.fancybox.pack.js"></script>          
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/jquery.fancybox.css" />				
	<link href="<? echo base_url();?>static/css/jquery.minitwitter.css" rel="stylesheet" type="text/css"/>   	
	<link href="<? echo base_url();?>static/css/jquery.booklet.1.2.0.css" 	rel="stylesheet" type="text/css">		
	<link href="<? echo base_url();?>static/css/mscarousel.css" 	rel="stylesheet" type="text/css">		
	<link href="<? echo base_url();?>static/css/buttons_selects.css" rel="stylesheet" type="text/css">		
	<link  href="<? echo base_url();?>static/css/coin-slider-styles.css" rel="stylesheet"type="text/css">				
	<link rel="shortcut icon" href="<? echo base_url();?>static/img/favicon.ico" type="image/x-icon">		
	<title><?=$SYS_metaTitle?></title>		<!-- Date: 2012-04-16 -->		
	<script type="text/javascript">		
		jQuery(document).ready(function() {				
			$(".fancybox").fancybox({'autoScale': true,'transitionIn': 'none','transitionOut': 'none','width': 400,'height': 200,'z-inex':9999,'type': 'iframe'});								
			$(".fancybox-privacity").fancybox({'autoScale': true,'transitionIn': 'none','transitionOut': 'none','width': 800,'height': 400,'z-inex':9999,'type': 'iframe'});	
			$('#contactoRevista').click(function() {$('#slider').css('display','none');});
				jQuery('#mycarousel').jcarousel({auto : 6,wrap : 'last',scroll : 1});				
				jQuery('#mycarousel2').jcarousel({auto : 6,wrap : 'last',scroll : 1});	
						
				mcarousel2 = $("#MCarousel").msCarousel({boxClass : 'div.box',height : 340,	width : 760,loop : true	}).data("msCarousel");				
				//add click event				
				$("#next").click(function() {					
					//calling next method					
					mcarousel2.next();				});				
				$("#previous").click(function() {					
					//calling previous method					
					mcarousel2.previous();				
				});				
				mcarousel = $("#miCarrusel").msCarousel({boxClass : 'div.box',height : 390,	width : 710}).data("msCarousel");				
				//add click event				
				$("#siguiente").click(function() {					
					//calling next method					
					mcarousel.next();				
				});				
				$("#atras").click(function() {					
					//calling previous method					
					mcarousel.previous();				
				});				
				$(function() {					
					$("#mybook").booklet({width : 545,height : 386,	speed : 1500,manual : true,	keyboard : true,closed : true,autoCenter : false,pageNumbers : false,pagePadding : 0,arrows : false,tabs : false,tabWidth : 180,tabHeight : 20});					
					$('#coin-slider').coinslider();});
			            
					$("#participa").submit(function(e) {
						e.preventDefault();
						var listo = 1;
						var nombre = $('#form_name').val();
						var mail = $('#form_mail').val(); 
						if ((nombre == 'Nombre')||(mail=='Mail') ) {        
							MensajeError();        			
							listo=0;		
						}
						if(listo == 1){
							$.ajax({    
								url:"<?=base_url()?>contacto/participa",	
								type: 'POST',	
								dataType: 'json',	
								data: $("#participa").serialize(),	
								success: function(data){
								if(data.estatus=='ok'){	
										jQuery.fancybox(data.html);	
									}else{
										jQuery.fancybox(data.html);
									}		
									
								}	
							});
						}    
					});  
				});		
	</script>		
 </head>	
<body>		
	<div id="wrapp">			
		<div id="header">				
			<a href="<?=base_url()?>" class="logo-index"></a>
			<div class=" jcarousel-skin-tango banner">								
				<div class="jcarousel-container jcarousel-container-horizontal" >									
					<div class="jcarousel-clip jcarousel-clip-horizontal" >										
						<ul id="mycarousel" class="jcarousel-list">											 
						<?php                                            
						 if ($banners != null):                                             
						 	foreach ($banners as $row):                                            	
						 		if ($row->posicion == 1):                                             	
						 ?>											
							 <li class="jcarousel-item jcarousel-item-horizontal  " style="float: left; list-style: none outside none;" >			
								<a href="<?=base_url();?>clientes/ver/<?=$row->idCategoria?>/<?=$row->idCliente?>/0"><img width="770" height="140" alt="" src="<? echo base_url();?>docs/anuncios/<?=$row->imagen?>"></a>
							 </li>
						 <?php
								endif;											 
							endforeach;											  
						endif;                                             
						?>																					
						</ul>									
					</div>								
				</div>							
			</div>				
		</div>					
				
		<div id="content">				
		<div class="wrappmaqueta2">
			<div class="primer_con_2">
				<div id="search2" >
				<form  method="post" action="<?=base_url();?>buscar/prebuscar" >
					<input class="search2_input " type="text" name="palabraClave" placeholder="BUSCAR">
				</form>
				</div>
				<div id="leftmenu2">
				<ul>
				<li id="inicio">						
					<a  class="inicio_btn" href="<?=base_url();?>" ></a>					
				</li>						
				<li id="clases">						
					<a  class="clases_btn" href="<?=base_url();?>clientes/clases/1/mis-clases" ></a>					
				</li>					
				<li id="salud">						
					<a href="<?=base_url();?>clientes/clases/2/mi-salud" class="salud_btn" ></a>						
				</li >					
				<li id="mama">						
					<a href="<?=base_url();?>clientes/clases/3/mi-mama" class="mama_btn" ></a>						
				</li >					
				<li id="diversion">						
					<a href="<?=base_url();?>clientes/clases/4/mi-diversion" class="diversion_btn" ></a>					
				</li>					
				<li id="fiesta">						
					<a href="<?=base_url();?>clientes/clases/5/mi-fiesta" class="fiesta_btn" ></a>					
				</li>					
				<li id="ropa">						
					<a href="<?=base_url();?>clientes/clases/6/mi-ropa-y-accesorios" class="ropa_btn"></a>	
				</li>					
				<!--				
				<li id="detodo">						
					<a href="<?=base_url();?>guia_zona_poniente/" class="detodo_btn" ></a>					
				</li>	
				-->		
				</ul>
				</div>
				<div id="formulario">
					<h2>¡Participa!</h2>
					<form method="post" class="" id="participa" action=""> 
						<p class="letra3">
							Si quieres recibir promociones exculsivas de Enfant, déjanos 
						</p>
						<input type="text" id="form_mail" name="email"  class="contact_input " style="border:0;" placeholder="MAIL"/>
						<input type="text" id="form_name" name="nombre"  class="contact_input " style="border:0;" placeholder="NOMBRE"/>
						
						<select name="estado"  class="contact_input_select" style="border:0;" placeholder="Lugar de Residencia">
							<option value="" style="color:#666;">Lugar de Residencia</option>
							<option value="Aguascalientes">Aguascalientes</option>
							<option value="Baja California">Baja California</option>
							<option value="Baja California Sur">Baja California Sur</option>
							<option value="Campeche">Campeche</option>
							<option value="Chiapas">Chiapas</option>
							<option value="Chihuahua">Chihuahua</option>
							<option value="Coahuila">Coahuila</option>
							<option value="Colima">Colima</option>
							<option value="Distrito Federal">Distrito Federal</option>
							<option value="Durango">Durango</option>
							<option value="Estado de México">Estado de México</option>
							<option value="Guanajuato">Guanajuato</option>
							<option value="Guerrero">Guerrero</option>
							<option value="Hidalgo">Hidalgo</option>
							<option value="Jalisco">Jalisco</option>
							<option value="Michoacán">Michoacán</option>
							<option value="Morelos">Morelos</option>
							<option value="Nayarit">Nayarit</option>
							<option value="Nuevo León">Nuevo León</option>
							<option value="Oaxaca">Oaxaca</option>
							<option value="Puebla">Puebla</option>
							<option value="Querétaro">Querétaro</option>
							<option value="Quintana Roo">Quintana Roo</option>
							<option value="San Luis Potosí">San Luis Potosí</option>
							<option value="Sinaloa">Sinaloa</option>
							<option value="Sonora">Sonora</option>
							<option value="Tabasco">Tabasco</option>
							<option value="Tamaulipas">Tamaulipas</option>
							<option value="Tlaxcala">Tlaxcala</option>
							<option value="Veracruz">Veracruz</option>
							<option value="Yucatán">Yucatán</option>
							<option value="Zacatecas">Zacatecas</option>
						</select>
						<select name="hijos"  class="contact_input_select contact_dropdown_btn sprite3"style="border:0;">
							<option value="" selected>Hijos</option>
							<?for($i=0;$i<=8;$i++):?>
							<option value="<?=$i?>"><?=$i?></option>
							<?endfor;?>
						</select>
						<button class="contact_btn"></button>
					
					</form>
				</div>
			</div>
			<div class="segundo_con_2">
				<div id="menu_con2">
				<ul style="">
				<li>						
					<a href="<? echo base_url();?>cuponera" style="float: right;">Cuponera</a>					
				</li>					
				<li>						
					<a href="<? echo base_url();?>nosotros" style="float: right;">Nosotros</a>					
				</li>					
				<li>						
					<a href="<? echo base_url();?>beneficios" style="float: right;">Beneficios</a>					
				</li>					
				<li>						
					<a href="<? echo base_url();?>anunciate" style="float: right;">Anúnciate</a>					
				</li>	
				<li>								
	 				<a href="<? echo base_url();?>blog/">Blog</a>							
	 			</li>					
				<li>						
					<a href="<? echo base_url();?>contacto" style="float: right;">Contacto</a>					
				</li>
				</ul>
				</div>
				<?php $this -> load -> view($module);?>	
			</div>
			</div>
			
	
			<div class="clearer"></div>		
		</div>

		<div id="footer">				
			<div class="wrapp-footer">
				<a href="http://www.facebook.com/pages/Enfant-Directorio-Infantil/370827672944989" class="btn-footer" id="fb" target="_blank"></a>
				<a href="http://twitter.com/#!/EnfantMx" class="btn-footer" id="tw" target="_blank"></a>
				<a href="http://pinterest.com/enfantmexico/" class="btn-footer" id="pnt" target="_blank"></a>
				<a href="" class="btn-footer" id="tb" target="_blank"></a>
				<a class="fancybox-privacity" href="<?=base_url()?>privacidad" id="privacidad">Aviso de Privacidad</a>
			</div>		
		</div>		
	</div>		
	<script type="text/javascript">
/*
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-38458823-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
*/
	</script>	
</body>
</html>