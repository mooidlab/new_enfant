<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html>	
<head>		
<meta http-equiv="Content-Type" content="text/html; charset= UTF-8">		
<meta name="author" content="Mooid Lab S.C.">		
<meta name="keywords" content="<?=$SYS_metaKeyWords?>">		
<meta name="description" content="><?=$SYS_metaDescription?>">		
<script src="<? echo base_url();?>static/js/jquery-1.7.2.min.js"></script>		
 <!-- <script type="text/javascript" src="<? echo base_url();?>static/js/jquery.js"></script> --> 	
 	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.jcarousel.js"></script>	
	<script type="text/javascript" src="<? echo base_url();?>static/js/coin-slider.min.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.msCarousel.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.msCarousel-min.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.easing.1.3.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/button_selects.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jvalidate.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.booklet.1.2.0.min.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.noty.js"></script>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/center.js"></script> 		
	<script type="text/javascript" src="<? echo base_url();?>static/js/default.js"></script> 		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/layoutEnfant.css">		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/fonts.css">		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/skin.css">		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/jquery.noty.css">		
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/noty_theme_default.css"/>		
	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.fancybox.pack.js"></script>          
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/jquery.fancybox.css" />		
	<script language="javascript" src="<? echo base_url();?>static/js/jquery.minitwitter.js" type="text/javascript"></script>		
	<link href="<? echo base_url();?>static/css/jquery.booklet.1.2.0.css" 	rel="stylesheet" type="text/css">		
	<link href="<? echo base_url();?>static/css/mscarousel.css" 	rel="stylesheet" type="text/css">		
	<link href="<? echo base_url();?>static/css/buttons_selects.css" rel="stylesheet" type="text/css">	
	<link rel="stylesheet" href="<?echo base_url(); ?>static/css/responsiveslider/responsiveslider.css" type="text/css" media="screen" />
	<script type="text/javascript" src="<? echo base_url();?>static/js/responsiveslider/responsiveslider.min.js"></script>	
	<link  href="<? echo base_url();?>static/css/coin-slider-styles.css" rel="stylesheet"type="text/css">				
	<link rel="shortcut icon" href="<? echo base_url();?>static/img/favicon.ico" type="image/x-icon">		
	<title><?=$SYS_metaTitle?></title>		<!-- Date: 2012-04-16 -->		
		
 </head>	
<body>		
	<div id="wrapp">			
		<div id="header">				
			<a href="<?=base_url()?>" class="logo-index"></a>
			<div class=" jcarousel-skin-tango banner">								
				<div class="jcarousel-container jcarousel-container-horizontal" >									
					<div class="jcarousel-clip jcarousel-clip-horizontal" >										
						<ul id="mycarousel" class="jcarousel-list">											 
						<?php                                            
						 if ($banners != null):                                             
						 	foreach ($banners as $row):                                            	
						 		if ($row->posicion == 1):                                             	
						 ?>											
							 <li class="jcarousel-item jcarousel-item-horizontal  " style="float: left; list-style: none outside none;" >			
								<a href="<?=base_url();?>clientes/ver/<?=$row->idCategoria?>/<?=$row->idCliente?>/0"><img width="770" height="140" alt="" src="<? echo base_url();?>docs/anuncios/<?=$row->imagen?>"></a>
							 </li>
						 <?php
								endif;											 
							endforeach;											  
						endif;                                             
						?>																					
						</ul>									
					</div>								
				</div>							
			</div>				
		</div>					
				
		<div id="content">				
			<?php $this -> load -> view($module);?>			
		</div>

		<div id="footer">				
			<div class="wrapp-footer">
				<a href="http://www.facebook.com/pages/Enfant-Directorio-Infantil/370827672944989" class="btn-footer" id="fb" target="_blank"></a>
				<a href="http://twitter.com/#!/EnfantMx" class="btn-footer" id="tw" target="_blank"></a>
				<a href="http://pinterest.com/enfantmexico/" class="btn-footer" id="pnt" target="_blank"></a>
				<a href="" class="btn-footer" id="tb" target="_blank"></a>
				<a class="fancybox-privacity" href="<?=base_url()?>privacidad" id="privacidad">Aviso de Privacidad</a>
			</div>		
		</div>		
	</div>		
	<script type="text/javascript">
		jQuery(document).ready(function() {		
			<? if ($popUp == 1): ?>
			jQuery.fancybox('d')	
			$(".fancybox-inner").load('<?=base_url()?>contacto/goEnfant')
			<? endif;?>
			$(".fancybox").fancybox({'autoScale': true,'transitionIn': 'none','transitionOut': 'none','width': 400,'height': 200,'z-inex':9999,'type': 'iframe'});								
			$(".fancybox-privacity").fancybox({'autoScale': true,'transitionIn': 'none','transitionOut': 'none','width': 800,'height': 400,'z-inex':9999,'type': 'iframe'});	
			$('#contactoRevista').click(function() {$('#slider').css('display','none');});

				$(".rslides").responsiveSlides({
                    pager: true,
                    nav: true
                });
                var tabs_w = 0-($(".rslides_tabs").width()/2);
                $(".rslides_tabs").css('margin-left', tabs_w+'px');
				jQuery('#mycarousel').jcarousel({auto : 6,wrap : 'last',scroll : 1});				
				jQuery('#mycarousel2').jcarousel({auto : 6,wrap : 'last',scroll : 1});	
				
				jQuery("#form_mail").blur(function() {					
					var valor_actual = jQuery(this).val();					
					if(valor_actual == '') {						
						jQuery(this).val('Mail');}
					});				
				jQuery("#form_name").focus(function() {					
					var valor_actual = jQuery(this).val();					
					if(valor_actual == 'Nombre') {						
						jQuery(this).val('');					
					}				
				});				
				jQuery("#form_name").blur(function() {					
					var valor_actual = jQuery(this).val();					
					if(valor_actual == '') {						
						jQuery(this).val('Nombre');					
					}				
				});				
				mcarousel2 = $("#MCarousel").msCarousel({boxClass : 'div.box',height : 340,	width : 760,loop : true	}).data("msCarousel");				
				//add click event				
				$("#next").click(function() {					
					//calling next method					
					mcarousel2.next();				});				
				$("#previous").click(function() {					
					//calling previous method					
					mcarousel2.previous();				
				});				
				mcarousel = $("#miCarrusel").msCarousel({boxClass : 'a.box',height : 390,	width : 710, autoSlide: 4000, loop: true }).data("msCarousel");				
				//add click event				
				$("#siguiente").click(function() {					
					//calling next method					
					mcarousel.next();				
				});				
				$("#atras").click(function() {					
					//calling previous method					
					mcarousel.previous();				
				});				
				$(function() {					
					$("#mybook").booklet({width : 545,height : 386,	speed : 1500,manual : true,	keyboard : true,closed : true,autoCenter : false,pageNumbers : false,pagePadding : 0,arrows : false,tabs : false,tabWidth : 180,tabHeight : 20});					
					$('#coin-slider').coinslider();});
			            
					$("#participa").submit(function(e) {
						e.preventDefault();
						var listo = 1;
						var nombre = $('#form_name').val();
						var mail = $('#form_mail').val(); 
						if ((nombre == 'Nombre')||(mail=='Mail') ) {        
							MensajeError();        			
							listo=0;		
						}
						if(listo == 1){
							$.ajax({    
								url:"<?=base_url()?>contacto/participa",	
								type: 'POST',	
								dataType: 'json',	
								data: $("#participa").serialize(),	
								success: function(data){
								if(data.estatus=='ok'){	
										jQuery.fancybox(data.html);	
									}else{
										jQuery.fancybox(data.html);
									}		
									
								}	
							});
						}    
					});  
				});
	</script>
	<script type="text/javascript">
/*
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-38458823-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
*/
	</script>	
</body>
</html>