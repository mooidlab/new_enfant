
<div class="con1">		
	<div class="primer_con">			
		<div id="search" >				
			<form id="buscador" method="post" action="<?=base_url();?>buscar/prebuscar">					
				<input type="text" class="search_input" name="palabraClave" placeholder="BUSCAR"/> 						
			</form>			
		</div>			
		<div id="leftmenu">				
			<ul>
				<li id="inicio">						
					<a  class="inicio_btn" href="<?=base_url();?>" ></a>					
				</li>						
				<li id="clases">						
					<a  class="clases_btn" href="<?=base_url();?>clientes/clases/1/mis-clases" ></a>					
				</li>					
				<li id="salud">						
					<a href="<?=base_url();?>clientes/clases/2/mi-salud" class="salud_btn" ></a>						
				</li >					
				<li id="mama">						
					<a href="<?=base_url();?>clientes/clases/3/mi-mama" class="mama_btn" ></a>						
				</li >					
				<li id="diversion">						
					<a href="<?=base_url();?>clientes/clases/4/mi-diversion" class="diversion_btn" ></a>					
				</li>					
				<li id="fiesta">						
					<a href="<?=base_url();?>clientes/clases/5/mi-fiesta" class="fiesta_btn" ></a>					
				</li>					
				<li id="ropa">						
					<a href="<?=base_url();?>clientes/clases/6/mi-ropa-y-accesorios" class="ropa_btn"></a>	
				</li>	
				<!--				
				<li id="detodo">						
					<a href="<?=base_url();?>guia_zona_poniente/" class="detodo_btn" ></a>					
				</li>	
				-->		
			</ul>			
		</div>		
	</div>		
	<div class="segundo_con">			
		<div id="menu_con">				
			<ul>											
				<li>						
					<a href="<? echo base_url();?>cuponera" style="float: right;">Cuponera</a>					
				</li>					
				<li>						
					<a href="<? echo base_url();?>nosotros" style="float: right;">Nosotros</a>					
				</li>					
				<li>						
					<a href="<? echo base_url();?>beneficios" style="float: right;">Beneficios</a>					
				</li>					
				<li>						
					<a href="<? echo base_url();?>anunciate" style="float: right;">Anúnciate</a>					
				</li>	
				<li>								
	 				<a href="<? echo base_url();?>blog/">Blog</a>							
	 			</li>					
				<li>						
					<a href="<? echo base_url();?>contacto" style="float: right;">Contacto</a>					
				</li>				
			</ul>			
		</div>						
		<div id="slider">	
			<ul id="leslider" class="rslides">												
				<?php 
				if($clientes != null):					               					
					 foreach($clientes as $row):					
				?>							
				<li>
					<a  class="box" href="<?=base_url();?>clientes/ver/<?=$row->idCategoria?>/<?=$row->idCliente?>/0" style="width:535px;  " >											
						<img src="<?=base_url();?>docs/cliente/<?=$row->imagen?>" style="height:386px; width:535px;  ">		
					</a>
				</li>				
				<?php 	
					endforeach; 
				endif; 
				?>									
			</ul>				
			<!--<input name="siguiente" id="siguiente" type="button" value="" class="btn_back" style="position:relative; float:right;  top:1px; border:0; cursor:pointer; z-index: 5;"/>				
			<input name="atras"  id="atras"  type="button" value="" class=" btn_next" style="position:relative;  float:right; top:1px;border:0; cursor:pointer; z-index: 5;"/>							
			-->
		</div>	
		<ul id="banners">
			<?php                                             
			if ($mini != null) {                                             
				foreach ($mini as $row) {                                             	
					if ($row->posicion == 3) {                                             	
			?>											
			<li>												
				<a href="<?=base_url();?>clientes/ver/<?=$row->idCategoria?>/<?=$row->idCliente?>/0"><img width="225" height="70" alt="" src="<? echo base_url();?>docs/anuncios/<?=$row->imagen?>"></a>											
			</li>											 
			<?php                                            
				}											 
			}											  
			}                                             
			?>	
		</ul>			
	</div>	
</div>	
<div class="con2">		
	<div class="social">
		<div id="blog">
			<h3>¡Blog!</h3>
			<table >					
				<?foreach($posts as $row):					?>					
				<tr>						
					<td class="tabla" >
						<a class="tabla2" href="<? echo base_url();?>blog/<?=$row->post_name?>"><?=$row->post_title?></a>						
					</td>					
				</tr>					
				<tr>						
					<td class="tabla2"><?=fancy_date(date("Y-m-d", strtotime($row->post_date)) )?></td>				
				</tr>					
				<?endforeach?>
			</table>
			<a href="<?=base_url()?>blog/" class="btn-ver-mas"></a>
		</div>
		<h3 id="h3">¡Síguenos!</h3>
		<div class="box">
			<a href="http://www.facebook.com/pages/Enfant-Directorio-Infantil/370827672944989"id="facebook" class="btn-social"></a>
		</div>
		<div class="box">
			<a href="http://twitter.com/#!/EnfantMx" id="twitter" class="btn-social"></a>
		</div>
		<div class="box">
			<a href="" id="instagram" class="btn-social"></a>
		</div>
		<div class="box">
			<a href="" id="pinterest" class="btn-social"></a>
		</div>
			
	</div>		
	<?
		foreach ($calendar as $row) {
	?>
	<img id="calendar" src="<?=base_url();?>docs/calendar/<?=$row->img?>">
	<?
		}
	?> 	
	
	<div class="banner-bottom">
		
		<div class=" jcarousel-skin-tango" style="width:1000px !important;">								
			<div class="jcarousel-container jcarousel-container-horizontal" style="position: relative; display: block; width:920px !important;">									
			<div class="jcarousel-clip jcarousel-clip-horizontal" style="position: relative; width:1000px !important;">										
			<ul id="mycarousel2" class="jcarousel-list" style="width:1000px !important;">											 
			<?php                                             
			if ($bottom != null) {                                             
			foreach ($bottom as $row) {                                             	
			if ($row->posicion == 2) {                                             	
			?>											
			<li class="jcarousel-item jcarousel-item-horizontal  " style="float: left; list-style: none outside none; width:1000px !important;" >												
			<a href="<?=base_url();?>clientes/ver/<?=$row->idCategoria?>/<?=$row->idCliente?>/0"><img width="1000" height="130" alt="" src="<? echo base_url();?>docs/anuncios/<?=$row->imagen?>"></a>											
			</li>											 
			<?php                                            
			}											 
			}											  
			}                                             
			?>										
			</ul>									
			</div>								
			</div>							
		</div>
	
	</div>
</div>	
<div class="birds">
<img src="<?=base_url()?>docs/imgs/footer.png">
</div>
<div class="clearer"></div>

