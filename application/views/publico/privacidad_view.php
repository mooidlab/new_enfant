<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html>	
<head>		
<meta http-equiv="Content-Type" content="text/html; charset= UTF-8">
<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/layoutEnfant.css">
</head>
<style type="text/css">
.fancybox-wrap{
	width: 800px !important;
}
</style>
<body style="background: none !important;">
	<div id="enfant-privacidad">
		<h2>Aviso de privacidad</h2>
		<p>
			ENFANT DIRECTORIO INFANTIL. (en lo sucesivo “ENFANT”) con domicilio ubicado en XXXXXXXXXXX suscribe el presente aviso de privacidad en cumplimiento con la Ley Federal de Protección de Datos Personales en Posesión de los Particulares (en lo sucesivo la “Ley”). 
			Los datos personales que Usted (en lo sucesivo “el Titular”) ha proporcionado directamente o a través de medios electrónicos a ENFANT (en lo sucesivo “el Responsable”), han sido recabados y serán tratados por ENFANT bajo los principios de licitud, consentimiento, información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad, de conformidad con lo dispuesto por la Ley. 
			Los datos personales proporcionados a ENFANT, se encontrarán debidamente protegidos a través de medidas de seguridad tecnológicas, físicas y administrativas, previniendo el uso o divulgación indebida de los mismos.
			Su información personal será utilizada para una de las siguientes opciones: fines operativos, estadísticos, comerciales y de promoción, de contratación de personal, de evaluación y desarrollo de personal, de registro de clientes, deudores, proveedores y acreedores, así como sus pagos, encuestas de servicio, o por disposición de alguna autoridad gubernamental. Para las finalidades antes mencionadas, requerimos obtener de forma enunciativa mas no limitativa los siguientes datos personales  y datos personales sensibles: Nombre completo, Domicilio completo, Dirección de lugar de trabajo, Teléfono de casa, Otro Teléfono de contacto, Teléfono de oficina, Celular de trabajo, E-mail personal, E-mail de trabajo, Puesto actual, Fecha de nacimiento, Lugar de Nacimiento, Nacionalidad, Género, Estado civil, No. de Cartilla, No. de Credencial de Elector, Nivel de Escolaridad, Información del centro de estudios, Nombre completo y apellidos del padre, Nombre completo y apellidos de la madre, No. IMSS (11 dígitos), Clave de RFC con homoclave, Clave de CURP, Contacto de emergencia, Información de empleos anteriores, Sueldos de empleos anteriores, Referencias personales, Número de cuenta bancaria.
			ENFANT podrá recabar datos personales sensibles en función del servicio o producto contratado por el Titular. Asimismo, le informamos que sus datos personales pueden ser transferidos y tratados dentro y fuera del país, por personas distintas a esta empresa. En ese sentido, su información puede ser compartida con a terceros que actúen en nombre de ENFANT o cualquiera de sus filiales nacionales o extranjeras, para procesarlos de acuerdo con el/los objetivo(s) para los cuales fueron originariamente recopilados o procesarse para cualquier otro objetivo autorizado por la Ley, en el entendido de que estos terceros deberán de obligarse con ENFANT para que dichos datos personales y datos personales sensibles sean utilizados solamente para el cumplimiento de los objetivos acordados así como a no vender o revelar su información personal y/o personal sensible a terceros, excepto en aquellos casos autorizados por la Ley, con previa autorización de ENFANT. Si usted no manifiesta su oposición, por medio de un correo electrónico o comunicación a las direcciones antes mencionadas, para que sus datos personales sean transferidos, se entenderá que ha otorgado su consentimiento para ello.
			Usted tiene derecho de acceder, rectificar y cancelar sus datos personales, así como de oponerse al tratamiento de los mismos o revocar el consentimiento que para tal fin nos haya otorgado, a través de los procedimientos que hemos implementado. Para ello usted se puede poner en contacto con nosotros en las direcciones y/o correos electrónicos que mencionamos a continuación:
			Nombre:  XXXXXXX
			Domicilio: XXXXX
			Correo electrónico: XXXX
			Teléfono: XXXX

			ENFANT entenderá que Usted como Titular, da su consentimiento tácitamente al tratamiento de sus datos personales, en caso de no manifestar su oposición al presente aviso de privacidad.
			El presente aviso de privacidad podrá ser modificado o actualizado unilateralmente por ENFANT, informándole al Titular de dichos cambios a través de su página web.

		</p>
	</div>

</body>

</html>

