 <div id="clientes" style="background:#DDF0EA;">
 	<?php if ($clientes != null):?>
<div class="encabezado"> <span style="color:#666;">Directorio Infantil > <a href="<?=base_url()?>guia_zona_poniente/" class="guia_zona_poniente_a" style="color:<?=$color?>;">Guía Zona Poniente</a> > </span><span style="color:<?=$color?>;"><?=$categoria?></span></div>

<div class="detodoCupon" id="detodo" style="overflow: hidden; !important;"> 
	
	<div class="laCuponera">
	    <?php

	    $class = 'a';
	    $per_page = 1;
	        echo '<div class="contenedor">';
	        foreach ($clientes as $row):
	            if($per_page == 7){
	               echo '</div>'; 
	               echo '<div class="contenedor">';
	               $per_page = 1;
	            }

	            echo '
	                <div class="clase_item_sub row-'.$class.'" style="border: 1px solid '.$color.'; ">
	                    <table>
	                    <thead>
		                    <tr> 
		                    <td colspan="2" class="titulo" style="color:'.$color.' !important;">'.$row->nombre.'</td>
		                    </tr>
		                </thead>
		                <tbody>
		                	<tr>
		                		<td colspan="2" style="color:'.$color.' !important;">'.$sub_sub_categoria.'</td>
		                	</tr>
		                ';
		                if ($row->dir2 != null) {
		                
		               echo '<tr> 
		                    <td class="titulo" style="color:'.$color.' !important;">Dir:</td><td>'.$row->dir2.'</td>
		                    </tr>
		                    <tr> ';
		                 }
		                 if ($row->telefono != null) {
		                 echo'<td class="titulo" style="color:'.$color.' !important;">Tel:</td><td>'.$row->telefono.'</td>
		                    </tr>';
		                 }   
		                  if ($row->email != null) {
		                  echo'<tr> 
		                    <td colspan="2" class="titulo" ><a href="http://'.$row->email.'" target="_blank" style="color:'.$color.' !important;">'.$row->email.'</a></td>
		                    </tr>
		                  ';
		              }

		                echo'</tbody>   
	                    </table>
	                </div>
	                ';
	            $per_page++;
	            $class = ($class == 'a') ? 'b' : 'a';
	        endforeach;
	        echo '</div>';
	    ?>
	</div>
	
	<div class="controles_guia">

	</div> 
</div>
<?php else:?>
<div class="encabezado"> <span style="color:#666;">Directorio Infantil > <a href="http://enfant.com.mx/guia_zona_poniente/" class="guia_zona_poniente_a" style="color:<?=$color?>;">Guía Zona Poniente</a> > </span><span style="color:<?=$color?>;">No se encontraron Resultados</span></div>

	<?php endif;?>

</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
	 var total_child ;
        var per_page;
        var page;
        var anime;
         total_child = $('.laCuponera > .contenedor').size();
            
            for (i=1 ; i <= total_child ; i++){
                    $('.controles_guia').append('<a href="" class="bullet" id="'+i+'"></a>');
            }
            $('.bullet').click(function(e){
                e.preventDefault();
                            page = $(this).attr('id');
                            anime = (page-1)*785;
                            $('.laCuponera').stop(true,true).animate({
                                    left: -anime+'px'
                            });
            });
            var total_bullet = $(".controles_guia > .bullet").size();
            if (total_bullet < 2) {
            	$(".controles_guia").css('display','none');
            };
});
</script>