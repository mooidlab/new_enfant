<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cuponera extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
        if(!is_logged()){
            $query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
            $redir = str_replace('/', '-', uri_string().$query);            
            // die(var_dump(is_logged()));
            redirect('admin/login/index/' . $redir);
        } // checamos si existe una sesión activa
		$this->load->helper(array('form', 'url'));
		$this->load->model('cliente_model');
		$this->load->model('cuponera_model');
		$this->load->model('file_model');
        $this->load->library('pagination');
    }
	
	function ver() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['module'] = 'admin/show_cuponera_view';
		$data['pestana'] = 6;
		$data['cupones'] = $this->cuponera_model->getAllCupones();
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['clientes'] = $this->cliente_model->getBannersForAdmin();
		$this->load->view('admin/main_view',$data);
	}
	function agregar() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Agreger Cupon';
		$data['SYS_metaDescription'] 	= 'Agreger Cupon';
		$data['module'] = 'admin/new_cupon_view';
		$data['pestana'] = 6;
		$data['losClientes'] = $this->cuponera_model->getClientes();
		$data['contador'] =  $this->cliente_model->getContador(); 
		$data['clientes'] = $this->cliente_model->getBannersForAdmin();
		$this->load->view('admin/main_view',$data);
	}
	function generar_input($numeroInputs){
		$data['misNumeros'] = $numeroInputs;
		$this->load->view('admin/ajax/numero_cupones_view',$data);
	}

	function agregar_do(){
	$idCliente      = $this->input->post('idCliente');
	$numero_cupones = $this->input->post('noCupones');
	$imagen = array();
	for($i=1; $i <= $numero_cupones; $i++){
		$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
		$imagen[$i] = $this -> file_model -> uploadItem('cuponera', $file_data, 'cupon_'.$i, false);
	}
	
	for($i=1; $i <= $numero_cupones;$i++){
			$arrInsert = array(
			'idCliente' => $idCliente, 
			'cupon'      => $imagen[$i],
			'publish' => 1			
			);

		$this->cuponera_model->insertCupones($arrInsert);
	}	
		redirect('admin/cuponera/ver');
	}
	function change_status()
	{
		$data['response'] = 'false';
		$idCuponera = $this->input->post('idCuponera');
		$this_cuponera = $this->cuponera_model->get_single_cupon($idCuponera);
		if($this->cuponera_model->update_status($idCuponera, $this_cuponera->publish)){
			$data['response'] = 'true';
			$this_cuponera = $this->cuponera_model->get_single_cupon($idCuponera);
			$data['string'] = ($this_cuponera->publish == 1) ? 'Habilitado' : 'Deshabilitado';
		}
		echo json_encode($data);
	}
	function editar($idCuponera) {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Editar Cupon';
		$data['SYS_metaDescription'] 	= 'Editar Cupon';
		$data['module'] = 'admin/edit_cupon_view';
		$data['pestana'] = 6;
		$data['losClientes'] = $this->cuponera_model->getClientes();
		$data['contador'] 	 =  $this->cliente_model->getContador(); 
		$data['cupon']   	 =  $this->cuponera_model->get_single_cupon($idCuponera);
		$data['clientes']	 =  $this->cliente_model->getBannersForAdmin();
		$this->load->view('admin/main_view',$data);
	}
	function editar_do(){
		$idCuponera      = $this->input->post('idCuponera');
		$idCliente      = $this->input->post('idCliente');
		$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
		$imagen= $this -> file_model -> uploadItem('cuponera', $file_data,'img_cupon', false);
		if (!is_array($imagen)) {
			$arrUpdate = array(
				'idCliente'  => $idCliente, 
				'cupon'      => $imagen,
				);
		}else{
			$arrUpdate = array(
				'idCliente' => $idCliente
				);
		}
				

			if($this->cuponera_model->updateCupones($idCuponera,$arrUpdate)){
				redirect('admin/cuponera/ver');
			}
	}
	 function do_ordenar(){
        $data = array(); 
		$ajax = array();
        $data['response'] = 'false';        
        if($this -> input -> post('action') == 'updateRecordsListings'):
            
            $recordsArray = array();
            $recordsArray = $this -> input -> post('recordsArray');
            
            $dafoc = '';
            
            for($idSort = 0; $idSort < count($recordsArray); $idSort++):
              $updateArr = array(                
                'id_sort'    => $idSort                
              );            
              $this -> cuponera_model -> updateIdSort($recordsArray[$idSort], $updateArr);
              
            //  print_r($recordsArray[$idSort] . ' - ' . $idSort . ' | ');
			  
            endfor;
        else:
            //echo ' ';            
        endif;
        //$data['paginas'] = $this -> revista_model -> getRevista();
       // $this->load->view('admin/do_ordenar_revista_view',$data);                          
    }
}
