<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class DeTodounPoco extends CI_Controller {

        

        public function __construct(){

                parent::__construct();

                $this->load->helper(array('form', 'url'));

                $this->load->model('cliente_model');

                $this->load->library('pagination');

    }

        

        function index() {

                $data['SYS_metaTitle']                  = 'Enfant';

                $data['SYS_metaKeyWords']               = 'Directorio Infantil, Infantes';

                $data['SYS_metaDescription']    = 'Directorio Infantil';

                $data['module'] ='publico/guia_zona_poniente_view';
                $data['banners'] =  $this->cliente_model->getBanners();   
                $data['color'] = '#891B63';
                $data['categoria'] = 'Guía Zona Poniente';
                $this->load->view('publico/main_2_view',$data);

        }
        function clases($idCategoria){

                $data['SYS_metaTitle']                  = 'Enfant';
                $data['SYS_metaKeyWords']               = 'Directorio Infantil, Infantes';
                $data['SYS_metaDescription']    = 'Directorio Infantil';
                $data['module'] ='publico/guia_clases_sub_view';
                $data['banners'] =  $this->cliente_model->getBanners();   
                $data['clientes']  = $this->cliente_model->getClientesdeTodounPoco();
                $data['sub_sub_cat'] = $this->cliente_model->getSubCategoriasDeTodoUnPoco();  
                 $data['llego'] = '1';
                $data['color'] = '#891B63';
                $data['categoria'] = 'De Todo un Poco';
        
                $this->load->view('publico/main_2_view',$data);

        }



}