<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Guia_zona_poniente extends CI_Controller {

        

        public function __construct(){

                parent::__construct();

                $this->load->helper(array('form', 'url'));

                $this->load->model('cliente_model');

                $this->load->library('pagination');

    }

        

        function index() {

                $data['SYS_metaTitle']                  = 'Enfant';

                $data['SYS_metaKeyWords']               = 'Directorio Infantil, Infantes';

                $data['SYS_metaDescription']    = 'Directorio Infantil';

                $data['module'] ='publico/guia_zona_poniente_view';
                $data['banners'] =  $this->cliente_model->getBanners();   
                $data['color'] = '#891B63';
                $data['categoria'] = 'Guía Zona Poniente';
                $data['items'] = $this->cliente_model->getSubCategoriasGuia();
                $this->load->view('publico/main_2_view',$data);

        }
        function show_item($idSubcategoria){
                $data['SYS_metaTitle']                  = 'Enfant | Guía Escolar';
                $data['SYS_metaKeyWords']               = 'Guía Escolar';
                $data['SYS_metaDescription']    = 'Directorio Infantil';
                $data['clientes'] = $this->cliente_model->getClientesGuia($idSubcategoria);
                $subCategoria = $this->cliente_model->getSubCategoria($idSubcategoria);
                
                $data['banners'] =  $this->cliente_model->getBanners();  
                $data['subCategoria'] = $subCategoria; 
                $data['color'] = $subCategoria->color;
                $data['categoria'] = $subCategoria->subCategoria;
                $data['idSubCategoria'] = $idSubcategoria;
              // die(var_dump(json_decode($subCategoria->sub_sub_categoria)));
                if ($subCategoria->sub_sub_categoria != null) {
                    $data['module'] ='publico/guia_clases_sub_view';
                    $data['sub_sub_cat'] = json_decode($subCategoria->sub_sub_categoria);
                    $data['llego'] = '0';
                } else {
                    $data['module'] ='publico/guia_clases_view';
                }
                $this->load->view('publico/main_2_view',$data);
        }
        function show_sub_cat($idSubcategoria){
                $sub_sub_categoria = trim($this->input->get('a'),'"') ;
                
                
                $data['SYS_metaKeyWords']               = $sub_sub_categoria;
                $data['SYS_metaDescription']    = 'Directorio Infantil';
                if ($sub_sub_categoria === '0') {
                    $subCategoria = $this->cliente_model->getSubCategoria($idSubcategoria);
                    $data['SYS_metaTitle']   = 'Enfant | '.$subCategoria->subCategoria;
                    $data['clientes'] = $this->cliente_model->getClientesGuia($idSubcategoria);
                    $data['module'] ='publico/guia_clases_view';
                     $data['color'] = '#891B63';
                     $data['categoria'] = 'De Todo un Poco';
                } else {
                 $data['SYS_metaTitle']                  = 'Enfant | '.$sub_sub_categoria;
                 $data['clientes'] = $this->cliente_model->getClientesGuiaSub($sub_sub_categoria); 
                 $data['module'] ='publico/show_item_sub_sub_view';  
                 $subCategoria = $this->cliente_model->getSubCategoria($idSubcategoria);
                $data['color'] = $subCategoria->color;
                $data['categoria'] = $subCategoria->subCategoria ;
                }
                
                
                $data['banners'] =  $this->cliente_model->getBanners();   
                
                
                $funciona = '"'.$sub_sub_categoria.'"';
               
                $data['sub_sub_categoria'] = json_decode($funciona);
                $this->load->view('publico/main_2_view',$data);
        }
        function guia_escolar() {
                $idSubcategoria = 18;
                $data['SYS_metaTitle']                  = 'Enfant | Guía Escolar';
                $data['SYS_metaKeyWords']               = 'Guía Escolar';
                $data['SYS_metaDescription']    = 'Directorio Infantil';
                $data['clientes'] = $this->cliente_model->getEscuelasBonitas($idSubcategoria);
                $data['module'] ='publico/guia_clases_view';
                $data['banners'] =  $this->cliente_model->getBanners();   
                $data['color'] = '#00B1BE';
                $data['categoria'] = 'Guía Escolar';
                $this->load->view('publico/main_2_view',$data);

        }
        function guia_restaurante() {
                $idSubcategoria = 19;
                $data['SYS_metaTitle']                  = 'Enfant | Guía Escolar';
                $data['SYS_metaKeyWords']               = 'Guía Escolar';
                $data['SYS_metaDescription']    = 'Directorio Infantil';
                $data['clientes'] = $this->cliente_model->getClientesGuia($idSubcategoria);
                $data['module'] ='publico/guia_clases_view';
                $data['banners'] =  $this->cliente_model->getBanners();   
                $data['color'] = '#ED8407';
                $data['categoria'] = 'Guía Restaurantes';
                $this->load->view('publico/main_2_view',$data);

        }
        function guia_emergencia() {
                $idSubcategoria = 17;
                $data['SYS_metaTitle']                  = 'Enfant | Guía Escolar';
                $data['SYS_metaKeyWords']               = 'Guía Escolar';
                $data['SYS_metaDescription']    = 'Directorio Infantil';
                $data['clientes'] = $this->cliente_model->getClientesGuia($idSubcategoria);
                $data['module'] ='publico/guia_clases_view';
                $data['banners'] =  $this->cliente_model->getBanners();   
                $data['color'] = '#E5443B';
                $data['categoria'] = 'Emergencias';
                $this->load->view('publico/main_2_view',$data);

        }
        function guia_medica() {
                $idSubcategoria = 20;
                $data['SYS_metaTitle']                  = 'Enfant | Guía Médica';
                $data['SYS_metaKeyWords']               = 'Guía Médica';
                $data['SYS_metaDescription']    = 'Directorio Infantil';
                $data['clientes'] = $this->cliente_model->getClientesGuia($idSubcategoria);
                $data['module'] ='publico/guia_clases_view';
                $data['banners'] =  $this->cliente_model->getBanners();   
                $data['color'] = '#FDBB30';
                $data['categoria'] = 'Guía Médica';
                $this->load->view('publico/main_2_view',$data);

        }
        
 }