<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nosotros extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('cliente_model');
    }
	
	function index() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['banners'] =  $this->cliente_model->getBanners();   
		$data['module'] ='publico/nosotros_view.php';
		$this->load->view('publico/main_2_view',$data);
		
		
	}
	
	

}