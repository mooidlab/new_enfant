<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Principal extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('blog_model');
		$this->load->model('cliente_model');
    }
	
	function index() {
		$data['SYS_metaTitle'] 			= 'Enfant';
		$data['SYS_metaKeyWords'] 		= 'Directorio Infantil, Infantes';
		$data['SYS_metaDescription'] 	= 'Directorio Infantil';
		$data['posts'] = $this->blog_model->get_coments();
		$data['clientes'] = $this->cliente_model->getFeaturedClientes(); 
		$data['banners'] =  $this->cliente_model->getBanners();  
		$count = array('contador' => 1);
		$this->cliente_model->insrtContador($count);
		// $data['clientes'] = $this->cliente_model->getClientes();
		    
		$data['module'] = 'publico/principal_view.php';	
		//$this->load->view('publico/main_view',$data);
		$this->load->view('publico/main_new_design_view',$data);
	}
	
	

}