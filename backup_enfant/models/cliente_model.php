<?php
   if(!defined('BASEPATH'))
	die('No');

class Cliente_model  extends CI_Model{
	
	
	function __construct(){
		parent::__construct();
		$this -> load -> config('tables', TRUE);
		$this -> tablas = $this -> config -> item('tablas', 'tables');
		
	}
	function update_todos($data,$idCliente){
		$this->db->where('idCliente',$idCliente);
		$this->db->update('cliente',$data);
		return true;

	}
	function te_llevo_todo(){
		$query = $this->db->get('cliente');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
	}
	function insrtContador($data){
		$this -> db -> insert('contador',$data);
		return true;
	}
	
	function getContador(){
		$query = $this -> db -> get('contador');
		if ($query -> num_rows() >= 1)
			return $query -> num_rows();
		return null;
	}
	function insertCliente ($data) {
		$this -> db -> insert('cliente', $data);
		return true;
	}
	function insertImagen ($data) {
		$this -> db -> insert('imgcliente', $data);
		return true;
	}
	function insertBanner ($data) {
		$this -> db -> insert('banner', $data);
		return true;
	}
	function insertSubCategoria ($data) {
		$this -> db -> insert('subcategoria', $data);
		return true;
	}
	function deleteSubCat($idSubCategoria){
		$this->db->where('idSubCategoria',$idSubCategoria);
		if ($this -> db -> delete('subcategoria')) {
			return true ; 
		} else {
			return false;
		}
	}
	function updateSubCategoria ($idSubCategoria,$data) {
		$this -> db -> where('idSubCategoria',$idSubCategoria);
		$this -> db -> update('subcategoria', $data);
		return true;
	}
	function getBanners (){
		$this -> db ->select('banner.*,cliente.idCategoria');
		$this -> db ->join('cliente','cliente.idCliente = banner.idCliente');			
		$query = $this -> db -> get('banner');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	function getClienteBanner(){
    	$this -> db -> select('cliente.idCliente,cliente.nombre');
		$this -> db -> order_by('cliente.nombre','ASC');
		$query = $this -> db -> get('cliente');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
    }
	
	function updateCliente($idCliente,$array){
		$this->db->where('idCliente',$idCliente);
		$this->db->update('cliente',$array);
		return true;
	}
	function updateBanner($idBanner,$array){
		$this->db->where('idBanner',$idBanner);
		$this->db->update('banner',$array);
		return true;
	}
	function deleteBanner($idBanner) {
		$this -> db -> where('idBanner', $idBanner);
		$this -> db -> delete('banner');
	}
	function deleteImgCliente($idImgCliente) {
		$this -> db -> where('idImgCliente', $idImgCliente);
		$this -> db -> delete('imgcliente');
	}
	function deleteCliente($idCliente) {
		$this -> db -> where('idCliente', $idCliente);
		$this -> db -> delete('cliente');
	}
	
	function getClientesForAdmin (){
		
		$this -> db -> select('cliente.idCliente,cliente.nombre,cliente.imagen, cliente.logo , cliente.correo, cliente.destacado');
		$query = $this -> db -> get('cliente');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	function getBannersForAdmin(){
		$this -> db -> select('banner.* ,cliente.idCliente,cliente.nombre, cliente.logo , cliente.correo, cliente.destacado');	
		$this -> db -> join('cliente','cliente.idCliente = banner.idCliente');
		$query = $this -> db -> get('banner');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
	}
	function getBanner ($idBanner){
			
		$this -> db -> where('banner.idBanner', $idBanner);
		$query = $this -> db -> get('banner');
		if ($query -> num_rows() == 1)
			return $query -> row();
		return null;
		}
	function getClientes (){		
		$query = $this -> db -> get('cliente');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
    function getFeaturedClientes(){
        $query =  $this -> db -> get_where('cliente', array('destacado' => 1));
        if($query -> num_rows() >= 1)
            return  $query -> result();
        return false;
    }
    

        function getClientesCategoriaPaginado ($idCategoria,$idCliente,$segment = 0,$perpage){
			
			if (!$segment){	$segment = 0;}
		$cmd="select *  from cliente where idCategoria = ".$idCategoria." 
              Order by (CASE
			  when
              idCliente = ".$idCliente." THEN -20 ELSE 0 END)+(idSort)  ASC
              
              LIMIT ".$segment.",".$perpage."
              ";
		$query = $this -> db -> query($cmd);	  
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	 function getClientesCategoria ($idCategoria, $perpage, $segment){
		$this -> db -> where('cliente.idCategoria',$idCategoria);
        $this -> db -> order_by('idSort', 'asc');
		$query = $this -> db -> get('cliente', $perpage, $segment);
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	  function countClientesCategoria ($idCategoria){
	  	$this -> db -> where('cliente.idCategoria',$idCategoria);
		$query = $this -> db -> get('cliente');
		if ($query -> num_rows() >= 1)
			return $query -> num_rows();
		return null;
		}
	  
	 
	   
	 function getImgClientesCategoria ($idCategoria){
	 	$this -> db -> select('imgcliente.*');
		$this -> db -> join('cliente', 'cliente.idCliente = imgcliente.idCliente');
		$this -> db -> where('cliente.idCategoria',$idCategoria);
		$query = $this -> db -> get('imgcliente');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	
	
	function getCliente ($idCliente){
				
		$this -> db -> where('idCliente', $idCliente);
		$query = $this -> db -> get('cliente');
		if ($query -> num_rows() == 1)
			return $query -> row();
		return null;
		}
	
	function getImagenes ($idCliente){
		
		$this -> db -> select('imgcliente.idImgCliente,imgcliente.idCliente, imgcliente.imagen');
		$this -> db -> join('cliente', 'cliente.idCliente = imgcliente.idCliente');
		$this -> db -> where('cliente.idCliente', $idCliente);
		$query = $this -> db -> get('imgcliente');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	
	function getCategorias(){
	    $query = $this -> db -> get('categoria');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
     function updateIdSort($idCliente,$array) {
     	$this->db->where('idCliente',$idCliente);
		$this->db->update('cliente',$array);
		return true;
				
     }
	 function countBuscar ($palabraClave) {
	 	$resuladofinale = str_replace ('%20',' ',$palabraClave);
		 $elements =  str_replace ('%C3%B1','ñ',$resuladofinale);
		//var_dump($elements);
	 	$cmd= "select * from cliente 
               where cliente.`correo` regexp '".$elements."'
			   or  ( `cliente`.`descripcion` regexp '".$elements."')
			   or  ( cliente.`tags` regexp '".$elements."')
			   or  ( cliente.`direccion` regexp '".$elements."')
			   or  ( cliente.`facebook` regexp '".$elements."')
			   or  ( cliente.`nombre` regexp '".$elements."')
			   or  ( cliente.`p` regexp '".$elements."')
			   or  ( cliente.`twitter` regexp '".$elements."')
			   or  ( cliente.`web` regexp '".$elements."%')
			   or  ( (select categoria from categoria where categoria.idCategoria = cliente.idCategoria ) regexp '".$elements."')";
			   
			   
			   
			   
			   
	$query = $this -> db -> query($cmd);	  
		if ($query -> num_rows() >= 1)
			return $query -> num_rows();
		return null;
		}
	 function resultBuscar ($palabraClave,$segment,$perpage) {
	 	$resuladofinale = str_replace ('%20',' ',$palabraClave);
		 $elements =  str_replace ('%C3%B1','ñ',$resuladofinale);
	 	if (!$segment){	$segment = 0;}
	 	$cmd= "select * from cliente 
               where cliente.`correo` regexp '".$elements."'
			   or  ( `cliente`.`descripcion` regexp '".$elements."')
			   or  ( cliente.`tags` regexp '".$elements."')
			   or  ( cliente.`direccion` regexp '".$elements."')
			   or  ( cliente.`facebook` regexp '".$elements."')
			   or  ( cliente.`nombre` regexp '".$elements."')
			   or  ( cliente.`p` regexp '".$elements."')
			   or  ( cliente.`twitter` regexp '".$elements."')
			   or  ( cliente.`web` regexp '".$elements."%')
			   or  ( (select categoria from categoria where categoria.idCategoria = cliente.idCategoria ) regexp '".$elements."')
			   order by cliente.nombre
			   Limit ".$segment.",".$perpage."
			   ";
	$query = $this -> db -> query($cmd);	  
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	
	function getClientesSubcatego(){
		$this -> db -> select('clientesubcatego.* ,subcategoria.subCategoria');	
		$this -> db -> join('subcategoria','subcategoria.idSubCategoria = clientesubcatego.idSubCategoria');
		$this->db->where('subcategoria.poner_guia', 0);
		$this -> db -> order_by('subcategoria.subCategoria','ASC');
		$query = $this -> db -> get('clientesubcatego');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
	}

	function getEscuelasBonitas($idSubcategoria){
		$this -> db -> select('clientesubcatego.* ,subcategoria.subCategoria');	
		$this -> db -> join('subcategoria','subcategoria.idSubCategoria = clientesubcatego.idSubCategoria');
		$this->db->where('clientesubcatego.idSubcategoria', $idSubcategoria);
		$this -> db -> order_by('clientesubcatego.nombre','ASC');
		$query = $this -> db -> get('clientesubcatego');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
	}
	function getClientesGuiaForAdmin($idSubcategoria){
		$this -> db -> select('clientesubcatego.* ,subcategoria.subCategoria');	
		$this -> db -> join('subcategoria','subcategoria.idSubCategoria = clientesubcatego.idSubCategoria');
		$this->db->where('clientesubcatego.idSubcategoria', $idSubcategoria);
		$this -> db -> order_by('subcategoria.subCategoria','ASC');
		$query = $this -> db -> get('clientesubcatego');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
	}
	function getClientesdeTodounPoco(){
		$this->db->join('subcategoria','subcategoria.idSubCategoria = clientesubcatego.idSubcategoria');
 		$this->db->where('subcategoria.poner_guia', 0);
 		 $this -> db -> order_by('clientesubcatego.idSort', 'asc');
		$query = $this -> db -> get('clientesubcatego');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
	}
	function getClientesGuia($idSubcategoria){

		$this->db->where('idSubcategoria',$idSubcategoria);
		 $this -> db -> order_by('clientesubcatego.nombre', 'asc');
		$query = $this -> db -> get('clientesubcatego');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
	}
	function getClientesGuiaSub($sub_sub_categoria){

		$this->db->where('sub_sub_categoria',$sub_sub_categoria);
		 $this -> db -> order_by('clientesubcatego.nombre', 'asc');
		$query = $this -> db -> get('clientesubcatego');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
	}
	
	function getSubCategoriasDeTodoUnPoco(){
		$this->db->where('poner_guia', 0);
		$this -> db -> order_by('subcategoria.subCategoria','asc');

	    $query = $this -> db -> get('subcategoria');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	function CountClientesSubcatego(){

		$this -> db -> select('clientesubcatego.* ,subcategoria.subCategoria');	
		$this -> db -> join('subcategoria','subcategoria.idSubCategoria = clientesubcatego.idSubCategoria');
		$idSubcategoria = array('17', '18', '19');
 		$this->db->where_not_in('clientesubcatego.idSubcategoria', $idSubcategoria);
		$this -> db -> order_by('subcategoria.subCategoria','ASC');
		$query = $this -> db -> get('clientesubcatego');
		if ($query -> num_rows() >= 1)
			return $query -> num_rows();
		return null;
	}
	function CountClientesGuiaForAdmin($idSubcategoria){

		$this -> db -> select('clientesubcatego.* ,subcategoria.subCategoria');	
		$this -> db -> join('subcategoria','subcategoria.idSubCategoria = clientesubcatego.idSubCategoria');
		$this->db->where('clientesubcatego.idSubcategoria', $idSubcategoria);
		$this -> db -> order_by('subcategoria.subCategoria','ASC');
		$query = $this -> db -> get('clientesubcatego');
		if ($query -> num_rows() >= 1)
			return $query -> num_rows();
		return null;
	}
	function insertClientesSubcatego ($data) {
		$this -> db -> insert('clientesubcatego', $data);
		return true;
	}
	function updateClientesSubcatego($idClienteSubCatego,$array){
		$this->db->where('idClienteSubCatego',$idClienteSubCatego);
		$this->db->update('clientesubcatego',$array);
		return true;
	}
		
	function deleteClienteSubcatego($idClienteSubCatego) {
		$this -> db -> where('idClienteSubCatego', $idClienteSubCatego);
		$this -> db -> delete('clientesubcatego');
	}
	function getSubCategorias(){
		$this -> db -> order_by('subcategoria.subCategoria','ASC');
	    $query = $this -> db -> get('subcategoria');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
		function getSubCategoriasDeTodo(){
 		$this->db->where('subcategoria.poner_guia',0);	
		$this -> db -> order_by('subcategoria.subCategoria','ASC');
	    $query = $this -> db -> get('subcategoria');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
	function getSubCategoriasGuia(){
		$this->db->where('subcategoria.poner_guia',1);
		$this -> db -> order_by('subcategoria.idSubCategoria','asc');
	    $query = $this -> db -> get('subcategoria');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
		}
    function countSubCategorias(){
	    $query = $this -> db -> get('subcategoria');
		if ($query -> num_rows() >= 1)
			return $query -> num_rows();
		return null;
		}
    function getClienteSubCatego ($idClienteSubCatego){
		
		$this -> db -> where('idClienteSubCatego', $idClienteSubCatego);
		 $this -> db -> order_by('clientesubcatego.idSort', 'asc');
		$query = $this -> db -> get('clientesubcatego');
		if ($query -> num_rows() == 1)
			return $query -> row();
		return null;
		}
	
	 function getClienteSubCategoria ($idSubcategoria){
		$this -> db -> where('clientesubcatego.idSubcategoria',$idSubcategoria);
        $this -> db -> order_by('clientesubcatego.idSort', 'asc');
		$query = $this -> db -> get('clientesubcatego');
		$data=array();
		$data['result'] = null;
		$this ->db ->where('idSubcategoria',$idSubcategoria);
		$query2 = $this ->db ->get('subcategoria');
		$nombre = $query2-> row();

		if ($query -> num_rows() >= 1){
			$data['result'] =  $query -> result();
			$data['nombre'] = $nombre->subCategoria;
			return $data;
		}
			else{
			return null;
			}
		}
	function updateIdSortSubCategoria($idClienteSubCatego,$array) {
     	$this->db->where('idClienteSubCatego',$idClienteSubCatego);
		$this->db->update('clientesubcatego',$array);
		return true;
				
     }
     function getColores(){
     	return $this->db->get('color')->result();
     }

     function getSubCategoria($idSubCategoria){
     	$this->db->where('idSubCategoria',$idSubCategoria);
     	return $this->db->get('subcategoria')->row();
     }
     
}
?>
