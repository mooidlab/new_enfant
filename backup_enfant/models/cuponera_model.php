<?php
   if(!defined('BASEPATH'))
	die('No');

class Cuponera_model  extends CI_Model{
	
	
	function __construct(){
		parent::__construct();
		$this -> load -> config('tables', TRUE);
		$this -> tablas = $this -> config -> item('tablas', 'tables');
		
	}
	/* public*/
	function getCuponesFront(){
		$this->db->where('publish',1);
		$this->db->order_by('id_sort','asc');
		$query = $this->db->get('cuponera');
		if ($query -> num_rows() >= 1)
			return $query;
		return null;
	}
	function checkMail($email){
		$this->db->where('email',$email);
		$query = $this->db->get('cuponera_cliente');
		if ($query -> num_rows() == 1)
			return $query->row();
		return null;
	}

	function checkMailByCookie($hashCookie){
		$this->db->where('hashCookie',$hashCookie);
		$query = $this->db->get('cuponera_cliente');
		if ($query -> num_rows() == 1)
			return $query->row();
		return null;
	}

	function registrarMail($email,$ipVisitante,$hashCookie){
		$cookie = array(
			'name' => 'ihavecupones', 
			'value' => $hashCookie, 
			'expire' => mktime(24, 0, 0)
			);
		$this -> input -> set_cookie($cookie);

		$data = array(
			'email' => $email,
			'fecha' => date("Y-m-d H:i:s"), 
			'ipVisitante' => $ipVisitante, 
			'hashCookie' => $hashCookie,
			'vecesDescargado' => 1);
		$this -> db -> insert('cuponera_cliente', $data);
		return true;
	}
	function updateMail($idCuponeraCliente,$ipVisitante,$hashCookie,$contador){
		$contador++;
		$cookie = array(
			'name' => 'ihavecupones', 
			'value' => $hashCookie, 
			'expire' => mktime(24, 0, 0)
			);
		$this -> input -> set_cookie($cookie);

		$data = array(
			'fecha' => date("Y-m-d H:i:s"), 
			'ipVisitante' => $ipVisitante, 
			'hashCookie' => $hashCookie,
			'vecesDescargado' => $contador);
		$this-> db ->where('idCuponeraCliente',$idCuponeraCliente);
		$this -> db -> update('cuponera_cliente', $data);
		return true;
	}
	function checkIpVisitante($ipVisitante){
		$this->db->where('ipVisitante',$ipVisitante);
		$query = $this->db->get('cuponera_cliente');
		if ($query -> num_rows() > 0)
			return false;
		return true;
	}

	/* admin */
	function getClientes(){
		$query = $this->db->get('cliente');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;

	}
	function insertCupones($array){
		$this->db->insert('cuponera',$array);
		return true;

	}
	function getAllCupones(){
		$this->db->order_by('id_sort','asc');
		$query = $this->db->get('cuponera');
		if ($query -> num_rows() >= 1)
			return $query -> result();
		return null;
	}

	function get_single_cupon($idCuponera){
		$this->db->where('idCuponera',$idCuponera);
		$query = $this->db->get('cuponera');
		if ($query -> num_rows() == 1)
			return $query -> row();
		return null;
	}
	 function update_status($idCuponera, $actual_status)
    {
        $to_update = ($actual_status == 1) ? 0 : 1;
        $this->db->where('idCuponera', $idCuponera);
        if($this->db->update('cuponera', array('publish' => $to_update)))
            return true;
        return false;

    }

  	function updateCupones($idCuponera,$array){
  		$this->db->where('idCuponera',$idCuponera);
  		if($this->db->update('cuponera',$array))
  			return true;
  		return false;

  	}
  	function updateIdSort($idCuponera,$array) {
     	$this->db->where('idCuponera',$idCuponera);
		$this->db->update('cuponera',$array);
		return true;
				
     }

}