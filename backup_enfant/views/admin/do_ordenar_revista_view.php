<script type="text/javascript">
    <!--
    jQuery(document).ready(function($) {
<?php if (isset($error) || $this->session->flashdata('error')): ?>
            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);
<?php endif ?>

        /*
                Validacion de campos con JS.
                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
         */
        aformPro     = new Array();
        aformPro[0] = new Array('formPro', 'categoria'      , true, val_INPUT   , 'Categoría'       , -1, -1, 0, 0, null, true);
        aformPro[1] = new Array('formPro', 'titulo'     , true, val_INPUT   , 'Título'      , -1, -1, 0, 0, null, true);
        aformPro[2] = new Array('formPro', 'img'        , true, val_INPUT   , 'Imagen principal'        , -1, -1, 0, 0, null, true);
    
    });
    //-->
    
    jQuery(document).ready(function() {
        $("#sortableTable tbody").sortable({
            opacity: 0.6, cursor: 'move', update: function() {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings';
                $.post("<?=base_url()?>admin/revista/do_ordenar", order, function(theResponse){
                   // $(".boxModul").html(theResponse);                   
               
               
                });
            }
        });
        
        $("#categoria").change(
            function(){
                if($(this).val() != 'X'){                    
                    $(this).closest("form").submit();
                    
                }                
            }
        );
         $("a.borrar").click(function(){
            var id = $(this).attr("href");
            var len = id.lenght;
            id = id.substring(1,len);
            if(confirm("¿Está seguro de querer eliminar esta Pagína de la revista?")){
                window.location = "<?php echo base_url(); ?>admin/revista/delete/"+id;
            }
            else{
                return false
            }
            return false;
        });
    });
   
</script>

<br/>

<div class="box1">    
   <h1>Arrastre la fila de la Pagina  hasta el lugar deseado</h1>
    <table id="sortableTable" class="formTable centerheadings">
        <thead>
            <tr>
                <td> <strong> Orden </strong> </td>
                <td> <strong> Imagen </strong> </td>
                <td> <strong> Accion </strong> </td>
                
            </tr>            
        </thead>
        
        <tbody>
        <?php        
        if ($paginas != null) {
            foreach ($paginas as $row) {
                echo'
                <tr id="recordsArray_' . $row->idRevista . '">
                <td>Pagina ' . $row->idSort . '</td>
                <td> <img src="' . base_url() . 'docs/revista/' . $row->imagen . '" alt="So close..." width="60" height="105" /></td>
                 <td> <a href="#' . $row->idRevista. '" class="borrar">Eliminar</a></td>            
                </tr>
                ';
            }
        }
        ?>
        </tbody>
    </table>
</div>