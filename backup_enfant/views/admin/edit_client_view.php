<?php $row = $cliente;  ?>
<script type="text/javascript">
    <!--
    jQuery(document).ready(function($) {
<?php if (isset($error) || $this->session->flashdata('error')): ?>
            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);
<?php endif ?>

        /*
                Validacion de campos con JS.
                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
         */
    });
    //-->
    
    jQuery(document).ready(function() {
        var ocultar = false;
        
                
        $("a.AddItem").click(function(){
            var box = $(this).attr("href");
            if(ocultar == false){
                $(box).fadeIn();
                ocultar = true;
            }
            else{
                $(box).fadeOut();
                ocultar = false;
            }
            return false;
        });
        
        $("a.borrar").click(function(){
            var id = $(this).attr("href");
            var len = id.lenght;
            id = id.substring(1,len);
            if(confirm("¿Está seguro de querer eliminar esta galería?")){
                window.location = "<?php echo base_url(); ?>admin/proyectos/borrarproyecto/"+id;
            }
            else{
                return false
            }
            return false;
        });
    });
</script>
<a href="#addCat" class="AddItem"></a>

<br/>
<div class="box1" id="addCatXX">
    <h1>Agregar proyecto</h1>
    <form name="formPro" id="formPro" method="post" action="<?php echo base_url() ?>admin/directorio/editar_do/<?=$idCliente?>" enctype="multipart/form-data">
        <table class="formTable">
            <tr>
                <td><label for="categoria">Categoría</label></td>
                <td><select name="categoria" id="categoria">
                        <?php
                        if ($categorias != null) {
                            echo'<option value="">--Seleccionar categoría--</option>';
                            foreach ($categorias as $rowX) { ?>
                                <option value="<?=$rowX->idCategoria?>" <?php if($rowX->idCategoria == $row->idCategoria){echo 'selected="selected"';} ?>><?=$rowX->categoria?></option>
                                
                            <?php }
                        } else {
                            echo '<option value="">--No se encontraron categorías--</option>';
                        }
                        $facebook = '';
                        $twitter = '';
                        if($row->facebook !=''){
                            $exp_facebook = explode('/', $row->facebook);
                            $facebook = end($exp_facebook);
                        }
                        
                        if($row->twitter != ''){
                            $exp_twitter = explode('/', $row->twitter);
                            $twitter = end($exp_twitter);
                        }                        
                        ?>
                    </select></td>
            </tr>
            <tr>
                <td><label for="nombre">Nombre</label></td>
                <td><input type="text" name="nombre" id="nombre" value="<?=$row->nombre?>"/></td>
            </tr>
            <tr>
                <td><label for="descripcion">Descripción</label></td>
                <td><textarea name="descripcion" id="descripcion"><?=$row->descripcion?></textarea></td>
            <br/>
            </tr>
            <tr>
                <td><label for="direccion">Dirección</label></td>
                <td><input type="text" name="direccion" id="direccion" value="<?=$row->direccion?>"/></td>
            </tr>
            <tr>
                <td><label for="colonia">Colonia</label></td>
                <td><input type="text" name="colonia" id="colonia" value="<?=$row->colonia?>"/></td>
            </tr>
            <tr>
                <td><label for="telefono">Teléfono</label></td>
                <td><input type="text" name="telefono" id="telefono" value="<?=$row->telefono?>"/></td>
            </tr>
            <tr>
                <td><label for="correo">Correo Electrónico</label></td>
                <td><input type="text" name="correo" id="correo" value="<?=$row->correo?>"/></td>
            </tr>
            <tr>
                <td><label for="web">Sitio Web</label></td>
                <td><input type="text" name="web" id="web" value="<?=$row->web?>"/></td>
            </tr>
            <tr>
                <td><label for="p">Descripción Larga</label></td>
                <td><input type="text" name="p" id="p" value="<?=$row->p?>"/></td>
            </tr>
              <tr>
                <td><label for="tags">Tags de busqueda</label></td>
                <td><input type="text" name="tags" id="tags" value="<?=$row->tags?>"/></td>
            </tr>
            <tr>
                <td>
                    <label for="imagen">Imagen Principal</label>
                </td>
                <td>
                    <img src="<?=base_url()?>docs/cliente/<?=$row->imagen?>" alt="So Close.." width="100" height="70" />
                    <br />
                    <input type="file" name="imagen" id="imagen"/>
                </td>
            </tr>
            
            <tr>
                <td>
                    <label for="logo">Logo</label>
                </td>
                <td>
                    <img src="<?=base_url()?>docs/cliente/<?=$row->logo?>" alt="So Close.." width="100" height="70" />
                    <br />
                    <input type="file" name="logo" id="logo"/>
                </td>
            </tr>
            
            <tr>
                <td><label for="facebook">Perfil de Facebook</label></td>
                <td>www.facebook.com/<input type="text" name="facebook" id="facebook" value="<?=$facebook?>"/></td>
            </tr>
            
            <tr>
                <td><label for="twitter">Perfil de Twitter</label></td>
                <td>www.twitter.com/#!/<input type="text" name="twitter" id="twitter" value="<?=$twitter?>"/></td>
            </tr>
            
            <tr>
                <td><label for="destacado">Cliente Destacado</label></td>
                <td><input type="checkbox" value="1" name="destacado" id="destacado" <?php if($row->destacado == 1){echo 'checked="checked"';} ?>/></td>                
            </tr>
            
            <tr>
                <td> <button type="submit">Edit Cliente</button> </td>
                <td></td>
            </tr>
        </table>
    </form>
</div>