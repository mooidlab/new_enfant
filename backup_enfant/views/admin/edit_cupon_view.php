<br/>
<div class="box1" id="addCatX">
    <h1>Editar Cupon</h1>
    <form name="formPro" id="formPro" method="post" action="<?php echo base_url() ?>admin/cuponera/editar_do" enctype="multipart/form-data">
       <input type="hidden" name="idCuponera" value="<?=$cupon->idCuponera?>"/>
        <table class="formTable">
            <tr>
                <td><label for="idCliente">Cliente</label></td>
                <td><select name="idCliente" id="idCliente">
                        <?php
                        if ($losClientes != null) {
                            echo'<option value="">--Seleccionar cliente--</option>';
                            foreach ($losClientes as $row) {
                                if($cupon->idCliente == $row->idCliente){
                                     echo'
                                      <option value="' . $row->idCliente . '" selected="selected">' . $row->nombre . '</option>
                                    ';
                                }else{
                                    echo'
                                      <option value="' . $row->idCliente . '">' . $row->nombre . '</option>
                                    ';
                                }
                            }
                        } else {
                            echo '<option value="">--No se encontraron categorías--</option>';
                        }
                        ?>
                    </select></td>
            </tr>
            <tr>
                <td><label for="img_cupon">Editar Cupon</label></td>
                <td>
                   <input type="file" name="img_cupon" id="img_cupon">
                </td>
            </tr>
             <tr>
                <td><label for="noCupones">Cupon Actual</label></td>
                <td>
                   <img src="<?=base_url()?>docs/cuponera/<?=$cupon->cupon?>"/>
                </td>
            </tr>
            <tr>
                <td><button type="submit">Editar Cupon</button></td>
            </tr>       
           
        </table>
        <table class="formTable" id="laCuponera">

        </table>
    </form>
</div>