<script type="text/javascript">
    <!--
    jQuery(document).ready(function($) {
<?php if (isset($error) || $this->session->flashdata('error')): ?>
            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);
<?php endif ?>

        /*
                Validacion de campos con JS.
                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
         */
        aformPro     = new Array();
        aformPro[0] = new Array('formPro', 'categoria'      , true, val_INPUT   , 'Categoría'       , -1, -1, 0, 0, null, true);
        aformPro[1] = new Array('formPro', 'titulo'     , true, val_INPUT   , 'Título'      , -1, -1, 0, 0, null, true);
        aformPro[2] = new Array('formPro', 'img'        , true, val_INPUT   , 'Imagen principal'        , -1, -1, 0, 0, null, true);
    
    });
    //-->
    
    jQuery(document).ready(function() {
        var ocultar = false;
        $("#addCat").hide();
                
        $("a.AddItem").click(function(){
            var box = $(this).attr("href");
            if(ocultar == false){
                $(box).fadeIn();
                ocultar = true;
            }
            else{
                $(box).fadeOut();
                ocultar = false;
            }
            return false;
        });
        
        $("a.borrar").click(function(){
            var id = $(this).attr("href");
            var len = id.lenght;
            id = id.substring(1,len);
            if(confirm("¿Está seguro de querer eliminar esta galería?")){
                window.location = "<?php echo base_url(); ?>admin/directorio/deleteImage/"+id;
            }
            else{
                return false
            }
            return false;
        });
    });
</script>
<a href="#addCat" class="AddItem">Agregar Imagen</a>

<br/>

<div class="box1" id="addCat">
    <h1>Agregar proyecto</h1>
    <form name="formPro" id="formPro" method="post" action="<?php echo base_url() ?>admin/directorio/addImagen/<?=$cliente->idCliente?>" enctype="multipart/form-data">
        <table class="formTable">            
            <tr>
                <td><label for="imagen">Imagen</label></td>
                <td><input type="file" name="imagen" id="imagen"/></td>
            </tr>       
            <tr>
                <td> <button type="submit">Agregar Imagen</button> </td>
                <td></td>
            </tr>
        </table>
    </form>
</div>

<div class="box1">
    <h1>Imagenes</h1>
    <table class="formTable centerheadings">
        <tr>
            <td> <strong> Imagenes Actuales </strong> </td>
            <td> Acciones </td>            
        </tr>
        <?php
        if ($imagenes != null) {
            foreach ($imagenes as $row) {
                echo'
                <tr>
                <td> <img src="' . base_url() . 'docs/cliente/' . $row->imagen . '" alt="So close..." width="150" /> </td>
                <td> <a href="#' . $row->idCliente . '/'.$row->idImgCliente.'" class="borrar">Eliminar</a> </td>                                                
                </tr>
                ';
            }
        } else{
            echo'
            <tr>
            <td> <strong> NO SE ENCONTRARON RESULTADOS </stron> </td>                                                
            </tr>
            ';
        }
        ?>
    </table>
</div>