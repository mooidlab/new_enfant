<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title><?= $SYS_metaTitle; ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" 		content="Mooid.mx">
	<meta name="description" 	content="<?= $SYS_metaDescription; ?>">	
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/adminLogin.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/jqueryUi.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/facebox.css" type="text/css">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>imgs/favicon.ico" type="image/x-icon">
	
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jqueryUi.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/facebox.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/library.js"></script>
	
	<!-- Load Dynamic Libraries -->
	<?php if(isset($dinamicLibrary['autoComplete'])):?>
		 <link rel="stylesheet" href="<?php echo base_url(); ?>css/autocomplete.css" type="text/css">
		 <script type="text/javascript" src="<?php echo base_url(); ?>js/autocomplete.js"></script>	
	<?php endif ?>
		<script type="text/javascript">
	<!--
	jQuery(document).ready(function($) {
	<?php if(isset($error) || $this->session->flashdata('error')): ?>
		notificationManager("<?=$this->lang->line(((isset($error))?($error):($this->session->flashdata('error'))))?>" ,null, true);
	<?php endif ?>
	
		/*
			Validacion de campos con JS.
				Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
		*/
		aFormLogin 	 = new Array();
		aFormLogin[0] = new Array('formLogin', 'emailUsuario'		, true, val_INPUT	, 'Usuario'		, -1, -1, 0, 0, null, true);
		aFormLogin[1] = new Array('formLogin', 'contrasenaUsuario'	, true, val_INPUT	, 'Contraseña'	, -1, -1, 0, 0, null, true);
		
	});
	//-->
	</script>
	<script type="text/javascript">
		//Arreglo que contiene el idioma para los errores
		arrLanError	 = new Array ();
		<?php
		for($i=0; $i <= 9; $i++):
			echo "arrLanError[".$i."] = \"".($this->lang->line('error_js_'.$i))."\";\n\t\t\t\t";
		endfor
		?>
	</script>
</head>
<body>
<!--

Designed by EnnovaTi
http://www.ennovati.mx/

Titulo     : 
Version    : 0.0
Fecha      : Mnt/000

-->
<body>
    
<div id="bodyWrap">
    <div id="content">
		<div id="cmid">
			<div class="left textAlignCenter">
				<br><br>
				<img src="<?php echo base_url(); ?>static/img/nube-v02.png" alt="Enfant - Directorio Infantil">
			</div>
			<div class="right textAlignRight">	
				<h1>Administracion - Inicio de sesi&oacute;n</h1>
	<form name="formLogin" method="post" action="<?php echo base_url()?>sesion/login/<?=$redir?>/admin/login">
		<?=form_fieldset('Autentificaci&oacute;n'); ?>
			<label for="emailUsuario">Usuario</label>
			<input type="text" name="emailUsuario" id="emailUsuario" value="<?php echo set_value('emailUsuario'); ?>">
			<?=form_error('emailUsuario'); ?>
			<br>
			<label for="contrasenaUsuario">Contrase&ntilde;a</label>
			<input type="password" name="contrasenaUsuario" id="contrasenaUsuario" value="<?php echo set_value('emailUsuario'); ?>">
			<?=form_error('contrasenaUsuario'); ?>
			<br>
			<label for="recordarme">Recordarme</label>
			<?php //Excepcion: unicamente en checkbox puedo ocupar directamente $_POST ?>
			<input type="checkbox" name="recordarme" id="recordarme" value="true" <?php echo ((!empty($_POST['recordarme']))?('checked'):(''));?>>
			<br>
			<input type="submit" value="Entrar" class="btn"/>
		<?=form_fieldset_close(); ?>
	</form>
			</div>
			<div style="clear:both;"></div>			
		</div>
	</div>
</div>
</body>
</html>