<script type="text/javascript">
    <!--
    jQuery(document).ready(function($) {
<?php if (isset($error) || $this->session->flashdata('error')): ?>
            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);
<?php endif ?>

        /*
                Validacion de campos con JS.
                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
         */
        
    
    });
    //-->
    
    jQuery(document).ready(function($) {
        var ocultar = false;
        $("#idSubcategoria").change(function(){
            var sub_cat = $(this).val();
            $.ajax({
              url: 'traemeSubSub',
              type: 'POST',
              dataType: 'json',
              data: {subCat: sub_cat},
              success: function(data) {
                //called when successful
                if (data.response == 'true') {
                   $("#idSubSubcategoria").html(data.html);
                   $("#show_sub_sub").slideDown(); 
                } else {
                   $("#show_sub_sub").slideUp();  
                }
              }
            });
         });
                
        $("a.AddItem").click(function(){
            var box = $(this).attr("href");
            if(ocultar == false){
                $(box).fadeIn();
                ocultar = true;
            }
            else{
                $(box).fadeOut();
                ocultar = false;
            }
            return false;
        });
        
        $("a.borrar").click(function(){
            var id = $(this).attr("href");
            var len = id.lenght;
            id = id.substring(1,len);
            if(confirm("¿Está seguro de querer eliminar esta galería?")){
                window.location = "<?php echo base_url(); ?>admin/proyectos/borrarproyecto/"+id;
            }
            else{
                return false
            }
            return false;
        });
    });
</script>
<a href="#addCat" class="AddItem">Nuevo Cliente</a>

<br/>
<div class="box1" id="addCatX">
    <h1>Agregar Cliente a Sub Categoria</h1>
    <form name="formPro" id="formPro" method="post" action="<?php echo base_url() ?>admin/directorio/agregar_do_ClientSubCatego" enctype="multipart/form-data">
        <table class="formTable">
            <tr>
                <td><label for="idSubcategoria">Sub Categoría</label></td>
                <td><select name="idSubcategoria" id="idSubcategoria">
                        <?php
                        if ($categorias != null) {
                            echo'<option value="">--Seleccionar Sub categoría--</option>';
                            foreach ($categorias as $row) {
                                echo'
                                  <option value="' . $row->idSubCategoria . '">' . $row->subCategoria . '</option>
                                ';
                            }
                        } else {
                            echo '<option value="">--No se encontraron sub categorías--</option>';
                        }
                        ?>
                    </select></td>
            </tr>
            <tr id="show_sub_sub" style="display:none;">
                <td>
                    <label for="idSubSubcategoria">Sub Sub Categoría</label>
                </td>
                <td>
                    <select name="idSubSubcategoria" id="idSubSubcategoria">
                       
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="nombre">Nombre</label></td>
                <td><input type="text" name="nombre" id="nombre"/></td>
            </tr>
           
            <tr>
                <td><label for="dir1">Dirección 1</label></td>
                <td><input type="text" name="dir1" id="dir1"/></td>
            </tr>
            <tr>
                <td><label for="dir2">Dirección 2</label></td>
                <td><input type="text" name="dir2" id="dir2"/></td>
            </tr>
            <tr>
                <td><label for="telefono">Teléfono 1</label></td>
                <td><input type="text" name="telefono" id="telefono"/></td>
            </tr>
            <tr>
                <td><label for="telefono2">Celular</label></td>
                <td><input type="text" name="telefono2" id="telefono2"/></td>
            </tr>
            <tr>
                <td><label for="email">Correo Electrónico</label></td>
                <td><input type="text" name="email" id="email"/></td>
            </tr>
           <tr>
                <td><button type="submit">Agregar</button></td>
                <td></td>
            </tr>
        </table>
    </form>
</div>
