<script type="text/javascript">
    <!--
    jQuery(document).ready(function($) {
<?php if (isset($error) || $this->session->flashdata('error')): ?>
            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);
<?php endif ?>

        /*
                Validacion de campos con JS.
                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
         */
        
    
    });
    //-->
    
    jQuery(document).ready(function() {
        var ocultar = false;
       
                
        $("a.AddItem").click(function(){
            var box = $(this).attr("href");
            if(ocultar == false){
                $(box).fadeIn();
                ocultar = true;
            }
            else{
                $(box).fadeOut();
                ocultar = false;
            }
            return false;
        });
        
        $("a.borrar").click(function(){
            var id = $(this).attr("href");
            var len = id.lenght;
            id = id.substring(1,len);
            if(confirm("¿Está seguro de querer eliminar esta galería?")){
                window.location = "<?php echo base_url(); ?>admin/proyectos/borrarproyecto/"+id;
            }
            else{
                return false
            }
            return false;
        });
    });
</script>
<a href="#addCat" class="AddItem">Nuevo Cliente</a>

<br/>
<div class="box1" id="addCatX">
    <h1>Agregar proyecto</h1>
    <form name="formPro" id="formPro" method="post" action="<?php echo base_url() ?>admin/directorio/agregar_do" enctype="multipart/form-data">
        <table class="formTable">
            <tr>
                <td><label for="categoria">Categoría</label></td>
                <td><select name="categoria" id="categoria">
                        <?php
                        if ($categorias != null) {
                            echo'<option value="">--Seleccionar categoría--</option>';
                            foreach ($categorias as $row) {
                                echo'
                                  <option value="' . $row->idCategoria . '">' . $row->categoria . '</option>
                                ';
                            }
                        } else {
                            echo '<option value="">--No se encontraron categorías--</option>';
                        }
                        ?>
                    </select></td>
            </tr>
            <tr>
                <td><label for="nombre">Nombre</label></td>
                <td><input type="text" name="nombre" id="nombre"/></td>
            </tr>
            <tr>
                <td><label for="descripcion">Descripción</label></td>
                <td><textarea name="descripcion" id="descripcion"></textarea></td>
            <br/>
            </tr>
            <tr>
                <td><label for="direccion">Dirección</label></td>
                <td><input type="text" name="direccion" id="direccion"/></td>
            </tr>
             <tr>
                <td><label for="colonia">Colonia</label></td>
                <td><input type="text" name="colonia" id="colonia"/></td>
            </tr>
            <tr>
                <td><label for="telefono">Teléfono</label></td>
                <td><input type="text" name="telefono" id="telefono"/></td>
            </tr>
            <tr>
                <td><label for="correo">Correo Electrónico</label></td>
                <td><input type="text" name="correo" id="correo"/></td>
            </tr>
            <tr>
                <td><label for="web">Sitio Web</label></td>
                <td><input type="text" name="web" id="web"/></td>
            </tr>
            <tr>
                <td><label for="p">Descripción Larga</label></td>
                <td><input type="text" name="p" id="p"/></td>
            </tr>
            <tr>
                <td><label for="tags">Tags de busqueda</label></td>
                <td><input type="text" name="tags" id="tags"/></td>
            </tr>
            <tr>
                <td><label for="imagen">Imagen Principal</label></td>
                <td><input type="file" name="imagen" id="imagen"/></td>
            </tr>
            
            <tr>
                <td><label for="logo">Logo</label></td>
                <td><input type="file" name="logo" id="logo"/> medida recomendada 200 x 120 px</td>
            </tr>
            
            <tr>
                <td><label for="facebook">Perfil de Facebook</label></td>
                <td>www.facebook.com/<input type="text" name="facebook" id="facebook"/></td>
            </tr>
            
            <tr>
                <td><label for="twitter">Perfil de Twitter</label></td>
                <td>www.twitter.com/<input type="text" name="twitter" id="twitter"/></td>
            </tr>
            
            <tr>
                <td><label for="destacado">Cliente Destacado</label></td>
                <td><input type="checkbox" value="1" name="destacado" id="destacado"/></td>                
            </tr>
            
            <tr>
                <td><button type="submit">Agregar Cliente</button></td>
                <td></td>
            </tr>
        </table>
    </form>
</div>