<script type="text/javascript">

    <!--

    jQuery(document).ready(function($) {

<?php if (isset($error) || $this->session->flashdata('error')): ?>

            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);

<?php endif ?>



        /*

                Validacion de campos con JS.

                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE

         */

        

    

    });

    //-->

    

    jQuery(document).ready(function() {

        var ocultar = false;

       

                

        $("a.AddItem").click(function(){

            var box = $(this).attr("href");

            if(ocultar == false){

                $(box).fadeIn();

                ocultar = true;

            }

            else{

                $(box).fadeOut();

                ocultar = false;

            }

            return false;

        });

        

        $("a.borrar").click(function(){

            var id = $(this).attr("href");

            var len = id.lenght;

            id = id.substring(1,len);

            if(confirm("¿Está seguro de querer eliminar esta galería?")){

                window.location = "<?php echo base_url(); ?>admin/proyectos/borrarproyecto/"+id;

            }

            else{

                return false

            }

            return false;

        });

    });

</script>

<a href="#addCat" class="AddItem">De Todo Un Poco</a>



<br/>

<div class="box1" id="addCatX">

    <h1>Agregar SubCategoria</h1>

    <form name="formPro" id="formPro" method="post" action="<?php echo base_url() ?>admin/directorio/agregar_do_subCategoria" enctype="multipart/form-data">

        <table class="formTable">
             <tr>
                <td><label for="subCategoria">Sub Categoria</label></td>
                <td><input type="text" name="subCategoria" id="subCategoria"/></td>
            </tr>
             <tr>
                <td><label for="option">Mostrar en:</label></td>
                <td>
                    <select name="option" id="option">
                        <option value="0">De todo un poco</option>
                        <option value="1">Guia zona poniente</option>
                    </select>
                </td>
            </tr>
        </table>
        <table class="formTable" id="option_guia_one" style="display:none;">
            <tr>
                <td><label for="color">Color de fondo:</label></td>
                <td>
                    <select name="color" id="color">
                        <option value="">------------</option>
                        <?
                            foreach ($colores as $row) {
                               echo '<option value="'.$row->color.'" style="background:'.$row->color.';"></option>';
                            }
                        ?>
                    </select>
                </td>
                
            </tr>
            <tr>
                <td><label for="option_two">Agregar Sub Sub Categoria?</label></td>
                <td>
                    Sí <input type="radio" name="option_sub_sub" value="1" class="option_sub_sub">
                    No <input type="radio" name="option_sub_sub" value="0" class="option_sub_sub">
                </td>
            </tr>
           
   
        </table>
        <table class="formTable" id="sub_sub_catego" style="display:none;">
             <tr>
                <td><label for="sub_sub_categoria">Sub Sub Categoria</label></td>
                <td>
                  <input type="text" name="sub_sub_categoria" id="sub_sub_categoria">  
                </td>
            </tr>
        </table>
        <table  class="formTable" id="show_sub_sub" >
            
        </table>
        <table class="formTable">
            <tr>
                <td><button type="submit">Agregar</button></td>
                <td></td>
            </tr>
        </table>

    </form>

</div>
<script type="text/javascript">
            function add_tag(){
            
            var tag = jQuery.trim(jQuery('#sub_sub_categoria').val());
            
            if ( tag != '' ) {
            
               jQuery('#show_sub_sub').append('<tr id="sub_sub_'+tag+'"><td><label>'+tag+'</label></td><td><a class="borrar_tag" href="'+tag+'">borrar</a> <input type="hidden" value="'+tag+'" name="sub_sub_cat[]"/></td></tr>');
             } else{
                alert('no puedes agregar una subcategoria vacia');
             }
            
            

            jQuery('#sub_sub_categoria').val('');
        }
    jQuery(document).ready(function($) {
        $("#option").change(function(){
            var option = $(this).val();
            if (option == '1') {
                $("#option_guia_one").slideDown();
            }
        });
        $(".option_sub_sub").on( "click",function(){
            var value_radio = $(".option_sub_sub:checked").val();
            if (value_radio == '1') {
                $("#sub_sub_catego").slideDown();
            }
         });
        $("#sub_sub_categoria").keypress(function (event){
            var key_code = (event.which === 0) ? event.keyCode : event.which;
            if (key_code === 13) {
                event.preventDefault();
                add_tag();
            }
            
        });
        $(".borrar_tag").live('click',function (e){
            e.preventDefault();
            var id_tag = $(this).attr('href');
            $("#sub_sub_"+id_tag).remove();
        });

    });
</script>