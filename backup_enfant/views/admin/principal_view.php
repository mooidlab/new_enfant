<script type="text/javascript">
<!--
jQuery(document).ready(function($) {
<?php if(isset($error) || $this->session->flashdata('error')): ?>
	notificationManager("<?=$this->lang->line(((isset($error))?($error):($this->session->flashdata('error'))))?>" ,null, true);
<?php endif ?>

	/*
		Validacion de campos con JS.
			Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
	*/
	aFormLogin 	 = new Array();
	aFormLogin[0] = new Array('formLogin', 'emailUsuario'		, true, val_MAIL	, 'Usuario'		, -1, -1, 0, 0, null, true);
	aFormLogin[1] = new Array('formLogin', 'contrasenaUsuario'	, true, val_INPUT	, 'Contrase�a'	, -1, -1, 0, 0, null, true);
	
});
//-->
</script>
	<h1>Administracion - Inicio de sesi&oacute;n</h1>
	<form name="formLogin" method="post" action="<?php echo base_url()?>sesion/login/admin/false">
		<?=form_fieldset('Autentificaci&oacute;n'); ?>
			<label for="emailUsuario">Usuario</label>
			<input type="text" name="emailUsuario" id="emailUsuario" value="<?php echo set_value('emailUsuario'); ?>" onchange="valida_info_individual(aFormLogin, 0, arrLanError)">
			<?=form_error('emailUsuario'); ?>
			<br>
			<label for="contrasenaUsuario">Contrase&ntilde;a</label>
			<input type="password" name="contrasenaUsuario" id="contrasenaUsuario" value="<?php echo set_value('emailUsuario'); ?>"  onchange="valida_info_individual(aFormLogin, 1, arrLanError)">
			<?=form_error('contrasenaUsuario'); ?>
			<br>
			<label for="recordarme">Recordarme</label>
			<?php //Excepcion: unicamente en checkbox puedo ocupar directamente $_POST ?>
			<input type="checkbox" name="recordarme" id="recordarme" value="true" <?php echo ((!empty($_POST['recordarme']))?('checked'):(''));?>>
			<br>
			<input type="button" value="Entrar" class="btn" onclick="valida_info_completa(aFormLogin, arrLanError)">
		<?=form_fieldset_close(); ?>
	</form>
	<br><br>
	<a href="<?php echo base_url(); ?>admin/principal/recuperarContrasena/" title="Olvidaste tu contrase�a">�Olvidaste tu contrase�a?</a>