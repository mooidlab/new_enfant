<script type="text/javascript">
    <!--
    jQuery(document).ready(function($) {
<?php if (isset($error) || $this->session->flashdata('error')): ?>
            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);
<?php endif ?>

        /*
                Validacion de campos con JS.
                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE
         */
        aformPro     = new Array();
        aformPro[0] = new Array('formPro', 'categoria'      , true, val_INPUT   , 'Categoría'       , -1, -1, 0, 0, null, true);
        aformPro[1] = new Array('formPro', 'titulo'     , true, val_INPUT   , 'Título'      , -1, -1, 0, 0, null, true);
        aformPro[2] = new Array('formPro', 'img'        , true, val_INPUT   , 'Imagen principal'        , -1, -1, 0, 0, null, true);
    
    });
    //-->
    
    jQuery(document).ready(function() {
        var ocultar = false;
        $("#addCat").hide();
                
        $("a.AddItem").click(function(){
            var box = $(this).attr("href");
            if(ocultar == false){
                $(box).fadeIn();
                ocultar = true;
            }
            else{
                $(box).fadeOut();
                ocultar = false;
            }
            return false;
        });
        
        $("a.borrar").click(function(){
            var id = $(this).attr("href");
            var len = id.lenght;
            id = id.substring(1,len);
            if(confirm("¿Está seguro de querer eliminar esta galería?")){
                window.location = "<?php echo base_url(); ?>admin/directorio/delete/"+id;
            }
            else{
                return false
            }
            return false;
        });
    });
</script>

<br/>

<div class="box1">
    <h1>Directorio</h1>
    <table id="sortableTable" class="formTable centerheadings">
        <thead>
            <tr>
                <td> <strong> Nombre </strong> </td>
                <td> <strong> Correo electrónico </strong> </td>
                <td> <strong> Imagen </strong> </td>
                <td> <strong> Logo </strong> </td>
                <td> <strong> Destacados </strong> </td>
                <td> <strong> Acciones </strong> </td>
            </tr>            
        </thead>
        
        <tbody>
        <?php
        if ($clientes != null) {
            foreach ($clientes as $row) {
                echo'
                <tr>
                <td>' . $row->nombre . '</td>
                <td>' . $row->correo . '</td>
                <td> <img src="' . base_url() . 'docs/cliente/' . $row->imagen . '" alt="So close..." width="60" height="35" /> </td>
                <td> <img src="' . base_url() . 'docs/cliente/' . $row->logo . '" alt="So close..." width="60" height="35" /></td>
                <td>' . $row->destacado . '</td>
                <td> 
                    <a href="' . base_url() . 'admin/directorio/editar/' . $row->idCliente . '">Ver</a>
                    &nbsp;&nbsp
                    <a href="#' . $row->idCliente . '" class="borrar">Eliminar</a>
                    &nbsp;&nbsp
                    <a href="' . base_url() . 'admin/directorio/imagenes/' . $row->idCliente . '">+ Imagenes</a>
                </td>                                
                </tr>
                ';
            }
        }
        ?>
        </tbody>
    </table>
</div>












