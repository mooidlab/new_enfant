<script type="text/javascript">

    <!--

    jQuery(document).ready(function($) {

<?php if (isset($error) || $this->session->flashdata('error')): ?>

            notificationManager("<?= $this->lang->line(((isset($error)) ? ($error) : ($this->session->flashdata('error')))) ?>" ,null, true);

<?php endif ?>



        /*

                Validacion de campos con JS.

                        Funciones permitidas: val_MAIL, val_INPUT, val_TEXTAREA, val_FILE

         */

        aformPro     = new Array();

        aformPro[0] = new Array('formPro', 'categoria'      , true, val_INPUT   , 'Categoría'       , -1, -1, 0, 0, null, true);

        aformPro[1] = new Array('formPro', 'titulo'     , true, val_INPUT   , 'Título'      , -1, -1, 0, 0, null, true);

        aformPro[2] = new Array('formPro', 'img'        , true, val_INPUT   , 'Imagen principal'        , -1, -1, 0, 0, null, true);

    

    });

    //-->

    

    jQuery(document).ready(function() {

          

         $("#categoria").change(

            function(){

                if($(this).val() != 'X'){                    

                     if($(this).val() != '0'){                    

                    $(this).closest("form").submit();

                    }else{

                        $('#dirDeTodo').fadeIn();

                    }

                }

               

            }

        );

        $("#dirDeTodo").change(

            function(){

                $(this).closest("form").submit();

            }

        );

    });

</script>



<br/>



<div class="box1">    

    <h1>Seleccione una Categor&iacute;a para Ordenar el directorio</h1>

    <form name="getCategory" id="getCategory" method="get" action="<?=base_url()?>admin/directorio/ver_guia_for_admin">

        <table class="formTable centerheadings">

            <tr>

                <td>

                    <label for="categoria">

                        Categor&iacute;a:

                    </label>

                </td>

                <td>

                    <select name="categoria" id="categoria">

                        <?php if($categorias != null):

                            ?><option value="X">Seleccione</option>

                            <option value="0">De Todo Un Poco</option><?php                                                

                            foreach($categorias as $row): ?>                                

                                <option value="<?=$row->idSubCategoria?>" <?php if($row->idSubCategoria==$this->input->get('categoria')): echo ' selected="selected" '; endif;?>><?=$row->subCategoria?></option>

                            <?php endforeach;

                        endif; ?>

                    </select>

                </td>  

                 <td>

                    <select name="dirDeTodo" id="dirDeTodo" style="display:none;">

                        <?php if($dirDetodo != null):

                            ?><?php

                            foreach($dirDetodo as $row): ?>

                                <option value="<?=$row->idSubCategoria?>"><?=$row->subCategoria?></option>

                            <?php endforeach;

                        endif; ?>

                    </select>

                </td>                

            </tr>        

        </table>

    </form>

    

    <h1>Editar guia</h1>

     <table id="sortableTable" class="formTable centerheadings" style="width:700px !important;">

        <thead>

            <tr>


                <td> <strong> Nombre </strong> </td>

                <td> <strong> Direccion 1 </strong> </td>

                

                <td> <strong> Direccion 2 </strong> </td>

                <td> <strong> Telefono 1 </strong> </td>

                <td> <strong> Telefono 2 </strong> </td>

                <td> <strong> Correo electrónico </strong> </td>

                 <td> <strong> Acciones </strong> </td>

            </tr>            

        </thead>

        

        <tbody>

        <?php

        if ($clientes != null) {

            foreach ($clientes['result'] as $row) {

                echo'

                <tr>


                <td>' . $row->nombre . '</td>

                <td>' . $row->dir1 . '</td>

                <td>' . $row->dir2 . ' </td>

                <td>' . $row->telefono . ' </td>

                <td>' . $row->telefono2 . '</td>

                <td>' . $row->email . '</td>

                <td> 

                    <a href="' . base_url() . 'admin/directorio/editarClientSubCatego/' . $row->idClienteSubCatego . '">Ver</a>

                    &nbsp;&nbsp

                    <a href="#' . $row->idClienteSubCatego . '" class="borrar">Eliminar</a>
                </td>                                
                </tr>

                ';

            }

        

        ?>

        </tbody>

    </table>

    <?php  }else{

			echo'No existen Clientes en esta sub Categoria';

		}

        ?>

</div>