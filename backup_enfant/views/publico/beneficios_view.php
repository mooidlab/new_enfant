<div id="bg_beneficios">
		
	<h1 >Beneficios</h1>
	<p class="justify">
	   
	   Representa una magnífica oportunidad de negocio poniéndolo en contacto directo con un segmento de mercado exigente y de alto poder adquisitivo.<br>
	  <span class="colorb">Es la única guía cuyos anunciantes pasan por un filtro meticuloso de selección, dando como resultado una agrupación de prestadores de servicios de primer nivel.</span><br>
	   Le permite al anunciante generar alguna promoción específica para su producto o servicio a través de redes sociales.<br>
	 <span class="colorb">  Es la única guía que se distribuye mano a mano entre el sector de la población a quien vamos dirigidos.</span><br>
	   Le permite al anunciante estar publicado también en internet y captar una mayor población por medio del cyber espacio.<br>
	 <span class="colorb">  Pueden tener un mini sitio en ENFANT.COM.MX y contar con publicidad permanente y de gran impacto.</span>
	</p>
</div>