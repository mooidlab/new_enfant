<script  type="text/javascript">
<? $totalpaginas=1;?>
jQuery(document).ready(function($) {
                var total_child ;
                var per_page;
                var page;
                var anime;
                total_child = $('#masonry-container > .masonry-grid').size();
                per_page= Math.ceil(total_child/3);
                for (i=1 ; i <= per_page ; i++){
                        $('.paginado').append('<div class="bullet" id="'+i+'"></div>');
                }
                $('.bullet').click(function(){
                                page = $(this).attr('id');
                                anime = (page-1)*720;
                                $('#masonry-container').stop(true,true).animate({
                                        left: -anime+'px'
                                });
                        });
                
                $('.todo').click(function(){
                        $('.relleno').stop(true,true).slideUp(150);
                        $('.todo').css('background','');
                                id = $(this).attr('id');
                                var nombre_completo = $('#nombre_completo_'+id).val();
                                $('#todo'+id+' > h4').css('color','#fff');

                                $('#todo'+id+' > h4').html(nombre_completo);
                                $('#todo'+id).css('background','#87296F');
                                
                                $('#relleno'+id).stop(true,true).slideDown(150);
                        });
                $('.relleno').mouseleave(function(){
                                id = $(this).attr('data-id');
                                var nombre_corto = $('#nombre_corto_'+id).val();
                                $('#todo'+id+' > h4').html(nombre_corto);
                                $('#todo'+id+' > h4').css('color','');
                                $('#todo'+id).css('background','');
                                $('#relleno'+id).stop(true,true).slideUp(150);
                        });

                var total_bullet = $(".paginado > .bullet").size();
            if (total_bullet < 2) {
                $(".paginado").css('display','none');
            };            
        });     
</script>
 
                                <div id="clientes">
                                        <div class="encabezado"> <span style="color:#666;">Directorio Infantil > <a href="http://enfant.com.mx/guia_zona_poniente/" class="guia_zona_poniente_a" style="color:<?=$color?>;">Guía Zona Poniente</a> > </span><span style="color:<?=$color?>;"><?=$categoria?></span></div>
                                    <?php 
                                   if ($subCategoria!=null){
                                        $for_limit = 14;
                                   ?>
                                    <div class="detodo" id="detodo" style="overflow: hidden; !important;"> 
                                        <div id="masonry-container" style="width: 2000% !important; height: auto !important; position:relative;">
                                                <div class="masonry-grid" style="position: relative; width: 220px;  height: auto; margin: 0 10px 0 10px; float: left;">
                                                        <? 
                                                                $c = 1;
                                                                foreach($subCategoria as $result) : 
                                                                        $bg_color = 'a'; 
                                                                if($c > $for_limit  ){
                                                                        echo '
                                                                        </div>
                                                                        <div class="masonry-grid" style="position: relative; width: 220px;  height: auto; margin: 0 10px 0 10px; float: left;">
                                                                        ';
                                                                        $c=1;
                                                                }
                                                        ?>
                                                        <div class="todo-title-bg" id="subcategoria<?=$result->idSubCategoria?>">
                                                                        <h3><?=$result->subCategoria ?></h3>
                                                                </div>
                                                                <? 
                                                                foreach($cliente as $row) : 
                                                                        if($result->idSubCategoria == $row->idSubcategoria):
                                                                                if($c > $for_limit){
                                                                        echo '
                                                                        </div>
                                                                        <div class="masonry-grid" style="position: relative; width: 220px;  height: 610px; margin: 0 10px 0 10px; float: left;">
                                                                        ';
                                                                        $c=1;
                                                                }
                                                                        $c++;
                                                                ?>
                                                                        <div class="todo-content-n row-<?=$bg_color?>" id="todo<?=$row->idClienteSubCatego?>">
                                                                        <h4 class="todo" id="<?=$row->idClienteSubCatego?>" >
                                                                                <?=substr($row->nombre, 0, 24) .'..'?>
                                                                        </h4>
                                                                        <input type="hidden" name="nombre_completo" id="nombre_completo_<?=$row->idClienteSubCatego?>" value="<?=$row->nombre?>"/>
                                                                        <input type="hidden" name="nombre_corto" id="nombre_corto_<?=$row->idClienteSubCatego?>" value="<?=substr($row->nombre, 0, 24) .'..'?>"/>

                                                                        
                                                                </div>
                                                                <div class="relleno" id="relleno<?=$row->idClienteSubCatego?>" data-id="<?=$row->idClienteSubCatego?>" style="display: none;">
                                                                                <div class="informacion">
                                                                                        <p>                                                                     
                                                                                                <?=($row->dir1!=NULL)      ? '<b>Direccion: </b> ' . $row->dir1 .'<br>': ''?>
                                                                                                <?=($row->dir2!=NULL)      ? '<b>Direccion: </b> ' . $row->dir2 .'<br>': ''?>
                                                                                                <?=($row->telefono!=NULL)  ? '<b>Telefono: </b> ' . $row->telefono .'<br>': ''?>
                                                                                                <?=($row->telefono2!=NULL) ? '<b>Celular: </b> ' . $row->telefono2 .'<br>': ''?>
                                                                                                <?=($row->email!=NULL)     ? '<b>E-mail: </b> ' . $row->email .'<br>': ''?>
                                                                                        </p>
                                                                                </div>
                                                                        </div>
                                                                <? 
                                                                $bg_color = ($bg_color == 'a') ? 'b' : 'a'; 
                                                                endif;
                                                                endforeach; ?>
                                                                
                                                    <?  
                                                        $c ++;
                                                        endforeach; 
                                                    ?>
                                                </div>
                                                
                                                </div>        

                                    </div>
                                    
                                                <?php }else{?>
                                                <div class="encabezado" ><span style="color:<?=$color?>;">Proximamente..</span></div>
                                                <?php }?>
                           <div class="paginado" style="float: left; margin-left: 30px; margin-top: 10px; position: relative;width: 170px;"></div> 
                            </div>
                               