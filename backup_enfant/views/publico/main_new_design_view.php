<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">

<html>	

<head>		

<meta http-equiv="Content-Type" content="text/html; charset= UTF-8">		

<meta name="author" content="Mooid Lab S.C.">		

<meta name="keywords" content="<?=$SYS_metaKeyWords?>">		

<meta name="description" content="><?=$SYS_metaDescription?>">		

<script src="<? echo base_url();?>static/js/jquery-1.7.2.min.js"></script>		

 <!-- <script type="text/javascript" src="<? echo base_url();?>static/js/jquery.js"></script> --> 	

 	<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.jcarousel.js"></script>	

 		<script type="text/javascript" src="<? echo base_url();?>static/js/coin-slider.min.js"></script>		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.msCarousel.js"></script>		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.msCarousel-min.js"></script>		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.easing.1.3.js"></script>		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/button_selects.js"></script>		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/jvalidate.js"></script>		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.booklet.1.2.0.min.js"></script>		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.noty.js"></script>		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/center.js"></script> 		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/default.js"></script> 
 		<script type="text/javascript" src="<? echo base_url();?>static/js/twitter.js"></script> 		

 		<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/new_layout.css">		

 		<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/fonts.css">		

 		<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/skin.css">		

 		<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/jquery.noty.css">		

 		<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/noty_theme_default.css"/>		

 		<script type="text/javascript" src="<? echo base_url();?>static/js/jquery.fancybox.pack.js"></script>          

 		<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/jquery.fancybox.css" />			

 		<link href="<? echo base_url();?>static/css/twitter.css" rel="stylesheet" type="text/css"/>        		

 		<link rel="stylesheet" href="<? echo base_url();?>static/css/Jvalidate.css" type="text/css">		

 		<link href="<? echo base_url();?>static/css/jquery.booklet.1.2.0.css" 	rel="stylesheet" type="text/css">		

 		<link href="<? echo base_url();?>static/css/mscarousel.css" 	rel="stylesheet" type="text/css">		

 		<link href="<? echo base_url();?>static/css/buttons_selects.css" rel="stylesheet" type="text/css">		

 		<link  href="<? echo base_url();?>static/css/coin-slider-styles.css" rel="stylesheet"type="text/css">				

 		<link rel="shortcut icon" href="<? echo base_url();?>static/img/favicon.ico" type="image/x-icon">		

 		<title><?=$SYS_metaTitle?></title>		<!-- Date: 2012-04-16 -->		

 		<script type="text/javascript">		

 		function generate(layout) {  	

 			var n = noty({text: 'Todos los campos son obligatorios', type: 'error',dismissQueue: true,layout: layout,theme: 'default'});  	
 			console.log('html: '+n.options.id);  }
 			function generate1(layout) {  var n = noty({ text: 'Gracias por participar con Enfant', type: 'success', dismissQueue: true, layout: layout, theme: 'default'  	});  	
			console.log('html: '+n.options.id);  }  
			function MensajeError() {    generate('center');      }  
 			function MensajeSucces() {    generate1('center');      }						
			jQuery(document).ready(function() {				
 				$(".fancybox").fancybox({'autoScale': false,'transitionIn': 'none','transitionOut': 'none','z-inex':9999,'type': 'iframe',beforeClose:function(){
 					$('#slider').css('display','block');
 				} 

 				});								

 					$('#contactoRevista').click(function() {$('#slider').css('display','none');});

 					jQuery('#mycarousel').jcarousel({auto : 6,wrap : 'last',scroll : 1});				

 					jQuery('#mycarousel2').jcarousel({auto : 6,wrap : 'last',scroll : 1});				
					
					jQuery("#form_mail").blur(function() {					

 						var valor_actual = jQuery(this).val();					

 						if(valor_actual == '') {						

 							jQuery(this).val('Mail');}

 						});				

 					jQuery("#form_name").focus(function() {					

 						var valor_actual = jQuery(this).val();					

 						if(valor_actual == 'Nombre') {						

 							jQuery(this).val('');					

 						}				

 					});				

 					jQuery("#form_name").blur(function() {					

 						var valor_actual = jQuery(this).val();					

 						if(valor_actual == '') {						

 							jQuery(this).val('Nombre');					

 						}				

 					});				

 					mcarousel2 = $("#MCarousel").msCarousel({boxClass : 'div.box',height : 340,	width : 760,loop : true	}).data("msCarousel");				

 					//add click event				

 					$("#next").click(function() {					

 						//calling next method					

 						mcarousel2.next();				});				

 					$("#previous").click(function() {					

 						//calling previous method					

 						mcarousel2.previous();				

 					});				

 					mcarousel = $("#miCarrusel").msCarousel({autoSlide:5000,boxClass : 'div.box',height : 390,	width : 710}).data("msCarousel");				

 					//add click event				

 					$("#siguiente").click(function() {					

 						//calling next method					

 						mcarousel.next();				

 					});				

 					$("#atras").click(function() {					

 						//calling previous method					

 						mcarousel.previous();				

 					});				

 					$(function() {					

 						$("#mybook").booklet({width : 750,height : 550,	speed : 1500,manual : true,	keyboard : true,closed : true,autoCenter : false,pageNumbers : false,pagePadding : 0,arrows : false,tabs : false,tabWidth : 180,tabHeight : 20});					

 						$('#coin-slider').coinslider();});

 				            

 						$("#participa").submit(function(e) {

 							e.preventDefault();

 							var listo = 1;

 							var nombre = $('#form_name').val();

 							var mail = $('#form_mail').val(); 

 							if ((nombre == 'Nombre')||(mail=='Mail') ) {        

 								MensajeError();        			

 								listo=0;		

 							}

 							if(listo == 1){

 								$.ajax({    

 									url:"<?=base_url()?>contacto/participa",	

 									type: 'POST',	

 									dataType: 'json',	

 									data: $("#participa").serialize(),	

 									success: function(data){

 									if(data.estatus=='ok'){	

 											jQuery.fancybox(data.html);	

 										}else{

 											jQuery.fancybox(data.html);

 										}		

 										

 									}	

 								});

 							}    

 						});  

 					});		

 				</script>		

 	</head>	

 		<body>		

 			<div id="wrapp">			

 			<div id="header">				

 					<div class="wrapp">

 						<div  class="logotipo">				

							<a href="<?=base_url();?>" class="logo_principal "></a>			

						</div>

						<div class="social">

							<a target="a_blank"  href="http://www.facebook.com/pages/Enfant-Directorio-Infantil/370827672944989" class="socialFB_btn "> </a>			

					 		<a target="a_blank"  href="http://twitter.com/#!/EnfantMx"  class="socialT_btn " > </a>

					 		<a target="a_blank"  href="http://pinterest.com/enfantmexico/"  class="socialPint_btn" > </a>		

					 			



						</div>	

 					</div>	

 			</div>			

 		<div id="content">				

 			<?php $this -> load -> view($module);?>			

 		</div>			

 		<div id="footer">				

 			<div class="wrapp">					

 				<div id="banner">						

 					<div class="img4">							

 						<div class=" jcarousel-skin-tango">								

 							<div class="jcarousel-container jcarousel-container-horizontal" style="position: relative; display: block;">									

 								<div class="jcarousel-clip jcarousel-clip-horizontal" style="position: relative;">										

 									<ul id="mycarousel2" class="jcarousel-list">											 

 										<?php                                             

 										if ($banners != null) {                                             

 											foreach ($banners as $row) {                                             	

 												if ($row->posicion == 2) {                                             	

 										?>											

 										<li class="jcarousel-item jcarousel-item-horizontal  " style="float: left; list-style: none outside none;" >												

 											<a href="<?=base_url();?>clientes/ver/<?=$row->idCategoria?>/<?=$row->idCliente?>/0"><img width="1000" height="130" alt="" src="<? echo base_url();?>docs/anuncios/<?=$row->imagen?>"></a>											

 										</li>											 

 										<?php                                            

 									}											 

 								}											  

 							}                                             

 							?>										

 						</ul>									

 					</div>								

 				</div>							

 			</div>						

 		</div>					

 	</div>										

 	<div class="separador"></div>					

 	<div class="social">						

 		<div class="wrappsocial">							

 			<div class="divtwitter">								

 				<div id="content-tweets">																	
					 <?= tweets()?>
 				</div>								

 			</div>							

 			<div class="divtwfb">								

 				<div class="divUnoFeliz">								

 					</div>								

 				<div class="divdosFeliz">								

 					

 			</div>								

 			</div>						

 		</div>					

 	</div>					

 	<div class="contacto" >						

 		<span style="float: left !important; margin-top:10px; margin-left: 5px;">

 			<b>Enfant © <?=date('Y')?></b>

 		</span>						

 		<span style="float: right !important; margin-top:10px; margin-right: 50px;">

 			<a class="footerfb" target="a_blank" href="http://www.facebook.com/pages/Enfant-Directorio-Infantil/370827672944989" ></a>								

 			<a class="footertw" target="a_blank" href="http://twitter.com/#!/EnfantMx"></a>	

 			<a class="footerpin" target="a_blank" href="http://pinterest.com/enfantmexico/" ></a>								

 											

 		</span>					

 	</div>					

 	<div class="menufooter">						

 		<ul>							

 										

 			<li>								

 				<a href="<?=base_url()?>guia_zona_poniente/">Directorio Infantil</a>							

 			</li>							

 			<li>								

 				<a href="<? echo base_url();?>revista">Revista On-Line</a>							

 			</li>							

 			<li>								

 				<a href="<?=base_url()?>nosotros">Quiénes Somos?</a>							

 			</li>							

 			<li>								

 				<a href="<?=base_url()?>beneficios">Beneficios</a>							

 			</li>							

 										

 			<li>								

 				<a href="<?=base_url()?>socialresponsable">Socialmente Responsable</a>							

 			</li>	

 			<li>								

 				<a href="<? echo base_url();?>blog/">Blog</a>							

 			</li>						

 			<li>								

 				<a href="<?=base_url()?>contacto">Contacto</a>							

 			</li>						

 		</ul>						

 							<!--<a href="http://www.mooid.mx" class="mooid"></a>-->

 						</div>					

 							<div class="clearer"></div>				

 				</div>			

 			</div>		

 		</div>		

 					<div class="bground">

 					</div>

 					<script type="text/javascript">



  var _gaq = _gaq || [];

  _gaq.push(['_setAccount', 'UA-38458823-1']);

  _gaq.push(['_trackPageview']);



  (function() {

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();



</script>	
<script type="text/javascript">
jQuery(document).ready(function($) {
	display_twitter('screen_name=twitterapi&count=2', '#content_tweets');
});
</script>
</body>

</html>