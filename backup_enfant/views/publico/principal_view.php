<div class="wrapp">	
	<div class="con1">		
		<div class="primer_con">			
					
			<div id="search" >				
				<form id="buscador" method="post" action="<?=base_url();?>buscar/prebuscar">					
					<fieldset class="search_bg">						
						<legend></legend>						
						<input type="text" class="search_input" name="palabraClave" /> 						
											
					</fieldset>				
				</form>			
			</div>			
			<div id="leftmenu">				
				<ul>					
					<li id="clases">						
						<a  class="clases_btn" href="<?=base_url();?>clientes/clases/1/mis-clases" ></a>					
					</li>					
					<li id="salud">						
						<a href="<?=base_url();?>clientes/clases/2/mi-salud" class="salud_btn" ></a>						
					</li >					
					<li id="mama">						
						<a href="<?=base_url();?>clientes/clases/3/mi-mama" class="mama_btn" ></a>						
					</li >					
					<li id="diversion">						
						<a href="<?=base_url();?>clientes/clases/4/mi-diversion" class="diversion_btn" ></a>					
					</li>					
					<li id="fiesta">						
						<a href="<?=base_url();?>clientes/clases/5/mi-fiesta" class="fiesta_btn" ></a>					
					</li>					
					<li id="ropa">						
						<a href="<?=base_url();?>clientes/clases/6/mi-ropa-y-accesorios" class="ropa_btn"></a>	
					</li>					
					<li id="detodo">						
						<a href="<?=base_url();?>guia_zona_poniente/" class="detodo_btn" ></a>					
					</li>	
					<li id="cuponera">						
						<a href="<?=base_url();?>cuponera/" class="cupon_btn" ></a>					
					</li>			
				</ul>			
			</div>		
		</div>		
		<div class="segundo_con">			
			<div id="menu_con">				
				<ul>											
										
					<li>						
						<a href="<? echo base_url();?>revista" style="float: right;">Revista On-Line</a>					
					</li>					
					<li>						
						<a href="<? echo base_url();?>nosotros" style="float: right;">Quiénes Somos?</a>					
					</li>					
					<li>						
						<a href="<? echo base_url();?>beneficios" style="float: right;">Beneficios</a>					
					</li>					
					<li>						
						<a href="<? echo base_url();?>socialresponsable" style="float: right;">Socialmente Responsable</a>					
					</li>	
					<li>								
		 				<a href="<? echo base_url();?>blog/">Blog</a>							
		 			</li>					
					<li>						
						<a href="<? echo base_url();?>contacto" style="float: right;">Contacto</a>					
					</li>				
			</ul>			
		</div>						
		<div id="slider">				
			<div id="miCarrusel">										
				<?php if($clientes != null):					               					
					 foreach($clientes as $row):					?>					
					 <div class="box" style="background:url(<?=base_url();?>docs/cliente/<?=$row->imagen?>); height:377px; width:696px; z-index:-1 !important; margin:5px 6px;">												
					 												
					 </div>					
					<?php 	
						endforeach; 
							endif; ?>									
						</div>				
						<input name="siguiente" id="siguiente" type="button" value="" class="btn_back" style="position:relative; float:right;  top:1px; border:0; cursor:pointer; z-index: 5;"/>				
						<input name="atras"  id="atras"  type="button" value="" class=" btn_next" style="position:relative;  float:right; top:1px;border:0; cursor:pointer; z-index: 5;"/>							
					</div>					
					 				</div>	
					 		</div>	
					 				<div class="con2">		
					 					<div id="div1" class="container_blog sprite">			
					 						<div id="divcontainar1">				
					 			<table >					
					 				<?foreach($posts as $row):					?>					
					 				<tr>						
					 					<td class="tabla" colspan="2">
					 						<a class="tabla2" href="<? echo base_url();?>blog/<?=$row->post_name?>"><?=$row->post_title?></a>						
					 					</td>					
					 				</tr>					
					 				<tr>						
					 					<td class="tabla2"><?=date("Y-m-d", strtotime($row->post_date))?></td>
					 					<td class="tabla2">
					 						<a class="tabla2" href="<? echo base_url();?>blog/<?=$row->post_name?>">LEER</a>
					 					</td>					
					 				</tr>
					 				<tr style="height:1px;">
					 					<td  colspan="2">
					 						<div style="border-top:dotted 2px #8ECCC2; height:1px; width:93%;"></div>
					 					</td>
					 				</tr>					
					 				<?endforeach?>
					 			</table>			
					 		</div>			
					 			<div id="divcontainar2">				
					 				<a id="anuncio1" class="blog_btn sprite" href="<? echo base_url();?>blog/"> </a>			
					 			</div>		
					 		</div>		
			 				<div id="div2" class="container_anunciate ">			
			 					<a id="anuncio2" class="anuncio_btn sprite" href="<? echo base_url();?>contacto"></a>		
			 				</div>		
			 				<div id="div3">			
					 			<div class="promos">				        
				 					<div class="part1"> 
				 						<h3></h3>				            
						 				<p class="letra4">							
						 					Sí quieres participar en promociones							
						 					exclusívas de Enfant, deja tus datos:						    
						 				</p>						
				 					</div>						
										<div class="part2"> 									
											<form method="post" class="" id="participa" action="">					
												<fieldset style=" border:0;">							
												<legend></legend>					
												<div class="campo">						
													<input type="text" id="form_mail" name="email" value="Mail" class="campo_participa" style="border:0;"/>					
												</div>						
												<div class="campo">						
													<input type="text" id="form_name" name="nombre" value="Nombre" class="campo_participa" style="border:0;"/>					
												</div>						
												<div class="campo">						
													<select name="estado" id="form_estado" class="styled">							
													<option value=" " selected>Residencia</option>							
													<option value="Polanco">Polanco</option>							
													<option value="Interlomas">Interlomas</option>							
													<option value="Bosques de Las Lomas">Bosques de Las Lomas</option>							
													<option value="Herradura">Herradura</option>							
													<option value="Vista Hermosa">Vista Hermosa</option>							
													<option value="Santa Fe">Santa Fe</option>							
													<option value="Contadero">Contadero</option>							
													<option value="Cuajimalpa">Cuajimalpa</option>							
													<option value="Tecamachalco">Tecamachalco</option>						
													</select>					
												</div>													
												<div class="campo">							
													<select name="hijos" id="form_hijos"  class="styled">								
													<option value=" " selected>Hijos</option>							
													<option value="1">1</option>							
													<option value="2">2</option>							
													<option value="3">3</option>							
													<option value="4">4</option>						
													</select>					
												</div>						
												<div class="campo1">												
													<input class="enviar" id="form_enviar" type="submit" style="border: 0pt;" value=" ">					
												</div>						
												</fieldset>					
											</form>								
										</div>			
									</div>	
					 			<a class="fancybox" id="contactoRevista" href="<? echo base_url();?>contacto/contactoRevista"> </a>				
					 	</div>		
					 	<a id="div4" href="<?=base_url()?>contacto/"class="contact_link "></a>	
					 	</div>	
					 	<div class="clearer"></div>
					 </div>
