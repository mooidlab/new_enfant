<html>
	<head>
	<link rel="stylesheet" type="text/css" href="<? echo base_url();?>static/css/new_layout.css">
	<script src="<? echo base_url();?>static/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
	           	jQuery("#nombre").focus(function() {
					var valor_actual = jQuery(this).val();
					if(valor_actual == 'Nombre') {
						jQuery(this).val('');
					}
				});

				jQuery("#nombre").blur(function() {
					var valor_actual = jQuery(this).val();
					if(valor_actual == '') {
						jQuery(this).val('Nombre');
					}
				});
				jQuery("#correo").focus(function() {
					var valor_actual = jQuery(this).val();
					if(valor_actual == 'Correo') {
						jQuery(this).val('');
					}
				});
				jQuery("#correo").blur(function() {
					var valor_actual = jQuery(this).val();
					if(valor_actual == '') {
						jQuery(this).val('Correo');
					}
				});
				jQuery("#direccion").focus(function() {
					var valor_actual = jQuery(this).val();
					if(valor_actual == 'Dirección') {
						jQuery(this).val('');
					}
				});
				jQuery("#direccion").blur(function() {
					var valor_actual = jQuery(this).val();
					if(valor_actual == '') {
						jQuery(this).val('Dirección');
					}
				});
		$("#revistaContacto").submit(function(e) {
		e.preventDefault();
		var listo = 1;
		var nombre = $('#nombre').val();
		var mail = $('#correo').val();
		var Direccion = $('#direccion').val();

		 if ((nombre == 'Nombre')||(mail=='Correo')||(nombre == 'Dirección')) {
		        MensajeError();
		        	
				listo=0;
				}
		if(listo == 1){
		$.ajax({
		    url:"<?php echo base_url() ;?>contacto/revistaParticipa",
			type: 'POST',
			dataType: 'json',
			data: $("#revistaContacto").serialize(),
			success: function(data){
			if(data.estatus=='ok'){
			alert('Sus datos han sido recibidos, nos pondremos en contacto lo antes posible.');
			}
			}
			});
		}
		    });
		  });

  
		</script>
		</head>
			<body class="revista_contacto">
				<div id="revistaContactobox" class="revistaContacto" >
					<form id="revistaContacto" method="POST" action="">
						<fieldset>
							<legend></legend>
							<div class="campo1">
							<label for="nombre">Nombre:</label>
							<input type="text" id="nombre" name="nombre" value="Nombre"/>
							</div>
							<div class="campo1">
							<label for="correo">Correo:</label>
							<input type="text" id="correo" name="correo" value="Correo"/>
							</div>
							<div class="campo1">
							<label for="direccion">Dirección:</label>
							<input type="text" id="direccion" name="direccion" value="Dirección"/>
							</div>
							<div class="campo1">
							<label for="mensaje">Mensaje:</label>
							<textarea  id="mensaje" name="mensaje" value="Mensaje"></textarea>
							</div>
							
							<input type="submit" value=" " class="enviar" />
						</fieldset>
					</form>
				</div>
			</body>
		</html>
