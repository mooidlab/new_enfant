/*
 * Jvalidate (for jQuery)
 * version: 1.0 (13/12/2011)
 * @requires jQuery v1.4.2 or later
 *
 * Licensed under the MIT:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2012, 2013 Guillermo Hernandez [ ghmolina@ennovati.mx ]
 * EnnovaTI.mx
 */
(function($) {
	$.fn.jvalidate = function(options) {
		// "This" contains the form to validate
		var formThis = this;
		// Create default options, extending them with any options that were provided
		var settings = $.extend({
			// Rules in the form [ OBLIGATORY , 'valFunct', 'FancyName' , 'EqualToInputName', 'OrInputName', minsize, maxsize ]
			rules : {},
			inputRequired_msg : 'El campo \'%s\' es obligatorio',
			inputMinLength_msg : 'El campo \'%s\' no puede contener menos de %d caracteres',
			inputMaxLength_msg : 'El campo \'%s\' no puede contener más de %d caracteres',
			inputBothEqual_msg : 'Los campos \'%s\' y \'%d\' tienen que ser iguales',
			inputEitherSel_msg : 'Alguno de los campos \'%s\', \'%d\' debe de estar lleno',
			inputBadSyntax_msg : 'El campo \'%s\' contiene errores de sintaxis o está mal escrito',
			show : function(msg) {
				// The Function to show messages
				$.facebox(msg);
			},
			hide : function() {
				// The Function to hide messages
				$(document).trigger('close.facebox');
			},
			submit : function() {
				// If everything is ok, what to do?
				return true;
			},
			// Default Functions to validate data
			'email' : function(txt) {
				if(txt == '')
					return true;
				var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
				return emailPattern.test(txt);
			},
			'input' : function(txt) {
				if(txt == '')
					return true;
				var noconPattern = /^[\w áéíóúÁÉÍÓÚ\.\-\;\:\#\@\+\%\(\)\$\*\?]*$/;
				if(!noconPattern.test(txt)) {
					return false
				}
				return true;
			},
			'textarea' : function(txt) {
				if(txt == '')
					return true;
				var noconPattern = /^[\w áéíóúÁÉÍÓÚ\.\-\;\:\#\@\+\%\(\)\$\*\?\n]*$/;
				if(!noconPattern.test(txt)) {
					return false
				}
				return true;
			},
			'file' : function(txt) {
				if(txt == '')
					return true;
				var ext = (txt.substring(txt.lastIndexOf("."))).toLowerCase();
				if(ext != ".jpg" || ext != ".jpeg" || ext != ".swf" || ext != ".png")
					return false;
				return true;
			},
			'url' : function(txt) {
				if(txt == '')
					return true;
				var urlPattern = /(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi;
				return urlPattern.test(txt);
			},
			'date' : function(txt) {
				if(txt == '')
					return true;
				var datePattern = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/;
				return datePattern.test(txt);
			},
			'alpha' : function(txt) {
				if(txt == '')
					return true;
				var alphaPattern = /^[a-zA-Z]*$/;
				return alphaPattern.test(txt);
			},
			'alphanum' : function(txt) {
				if(txt == '')
					return true;
				var alphanumPattern = /^[a-zA-Z0-9 ]*$/;
				return alphanumPattern.test(txt);
			},
			'decimalNum' : function(txt) {
				if(txt == '')
					return true;
				var numDecPattern = /^[0-9]+(\.[0-9][0-9]?)?$/;
				return numDecPattern.test(txt);
			}
		}, options);
		/* Assign a trigger to each element of the form */
		// We check for all the children that matches an input, textarea or select
		this.find("input:not(:submit), select, textarea").each(function() {
			// "$(this)" nos contains the child of the form
			$(this).change(function() {
				settings.hide.call($);
				inputName = $(this).attr("name");
				inputValue = $(this).val();
				// If the input doesn't exist on rules then it's true
				if(settings.rules[inputName]) {
					// Guardo todos los valores
					obligatory = settings.rules[inputName][0];
					valFunction = settings.rules[inputName][1];
					FancyName = settings.rules[inputName][2];
					equalToName = settings.rules[inputName][3];
					OrInputName = settings.rules[inputName][4];
					minsize = settings.rules[inputName][5];
					maxsize = settings.rules[inputName][6];
					// Obligatory?
					if(obligatory && inputValue.length == 0) {
						$(this).removeClass("ok");
						$(this).addClass("fail");
						// Generate Error
						settings.show.call($, settings.inputRequired_msg.replace(/%s/g, FancyName));
						// Return False
						return false;
					} else if(obligatory) {
						$(this).removeClass("fail");
						$(this).addClass("ok");
					} else {
						$(this).removeClass("fail");
						$(this).removeClass("ok");
					}
					// MAX size
					if(maxsize != -1 && inputValue.length > 0 && inputValue.length > maxsize) {
						$(this).removeClass("ok");
						$(this).addClass("fail");
						// Generate Error
						var txt = settings.inputMaxLength_msg.replace(/%s/g, FancyName);
						txt = txt.replace(/%d/g, maxsize);
						settings.show.call($, txt);
						// Return False
						return false;
					} else if(maxsize != -1 && inputValue.length > 0) {
						$(this).removeClass("fail");
						$(this).addClass("ok");
					}
					// MIN size
					if(minsize != -1 && inputValue.length > 0 && inputValue.length < minsize) {
						$(this).removeClass("ok");
						$(this).addClass("fail");
						// Generate Error
						var txt = settings.inputMinLength_msg.replace(/%s/g, FancyName);
						txt = txt.replace(/%d/g, minsize);
						settings.show.call($, txt);
						// Return False
						return false;
					} else if(minsize != -1 && inputValue.length > 0) {
						$(this).removeClass("fail");
						$(this).addClass("ok");
					}
					// This OR
					if(OrInputName != '') {
						compareObj = formThis.find('input[name="' + OrInputName + '"]')
						compareVal = compareObj.val();
						compareNam = compareObj.attr("name");
						if(compareVal != '' || inputValue != '') {
							$(this).removeClass("fail");
							compareObj.removeClass("fail");
						} else {
							compareObj.removeClass("ok");
							compareObj.addClass("fail");
							$(this).removeClass("ok");
							$(this).addClass("fail");
							// Generate Error
							var txt = settings.inputEitherSel_msg.replace(/%s/g, FancyName);
							txt = txt.replace(/%d/g, settings.rules[compareNam][2]);
							settings.show.call($, txt);
							// Return False
							return false;
						}
					}
					// Equal to
					if(equalToName != '') {
						compareObj = formThis.find('input[name="' + equalToName + '"]')
						compareVal = compareObj.val();
						compareNam = compareObj.attr("name");
						if(compareVal != inputValue) {
							compareObj.removeClass("ok");
							compareObj.addClass("fail");
							$(this).removeClass("ok");
							$(this).addClass("fail");
							// Generate Error
							var txt = settings.inputBothEqual_msg.replace(/%s/g, FancyName);
							txt = txt.replace(/%d/g, settings.rules[compareNam][2]);
							settings.show.call($, txt);
							// Return False
							return false;
						} else {
							$(this).removeClass("fail");
							compareObj.removeClass("fail");
							if(!compareObj.hasClass("fail")) {
								compareObj.addClass("ok");
							}
						}

					}
					// Function Validator
					if(!settings[valFunction](inputValue)) {
						$(this).removeClass("ok");
						$(this).addClass("fail");
						// Generate Error
						settings.show.call($, settings.inputBadSyntax_msg.replace(/%s/g, FancyName));
						// Return False
						return false
					} else if(inputValue != '') {
						$(this).removeClass("fail");
						$(this).addClass("ok");
					}
				}
				return true;
			});
		});
		/* Asign a trigger on submit */
		this.submit(function() {
			var submitQ = true;
			// $(this) now contain the form element
			$(this).find("input:not(:submit), select, textarea").each(function() {
				// "$(this)" now contain the child of the form
				if(!$(this).triggerHandler("change")) {
					submitQ = false;
					// Return in this case breaks the each
					return false;
				}
			});
			// Everything was ok?
			if(submitQ) {
				return settings.submit.call($);
			} else {
				return false;
			}
		});
	};
})(jQuery);
