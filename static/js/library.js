/*
*	RUTINAS DE VALIDACION DE FORMULARIOS
*	valida_info_individual
*	valida_info_completa
*	valida_info_completa_function
*/


function valida_info_individual(MATRIZ, LOCALIDAD, LENGUAJE){
/*
	MATRIZ    contine el nombre de la matriz en donde esta la informacion de configuracion
	LOCALIDAD contine de la localidad a checar

	FORMA       contiene el nombre de la forma
	NOMBRE      contiene el nombre del campo
	OBLIGATIRIO boolean si es obligatorio el campo
	TIPOCAMPO   
			*	val_MAIL
			*	val_INPUT
			*	val_TEXTAREA
			*	val_FILE
	NOMBREREAL  el nombre real del campo
        IGUALA      Localidad del campo que necesita ser igual a este, si es -1 entonces no aplica
        ESTEOESTE   Localidad del campo que si esta vacio entonces ese necesita estar lleno, si es -1 entonces no aplica
        MIN         Longitud Minima, si es 0 no hay
        MAX         Longitud Maxima, si es 0 no hay

        Nota:
                No se puede poner IGUAL A y ESTEOESTE al mismo tiempo
				
	DIV    		ID de la division que se va a prender si es que existe un error,
				null si no se quiere ese efecto
				
	JQUERY    	Boolean que contiene si mostrar un mensaje de error con effecto  $().fadeOut() de jQuery
				o si applicar la propiedad CSS  display: block 
				
	LENGUAJE	Matriz que contiene el idioma a ocupar
                
	Sintaxis:
		nombrematriz = new Array();
		nombrematriz = new Array(FORMA, NOMBRE, OBLIGATORIO, TIPOCAMPO, NOMBREREAL, IGUALA, ESTEOESTE, MIN, MAX, DIV, JQUERY);
				
*/
	var valor = F_TRIM(eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".value"));
         
        if(MATRIZ[LOCALIDAD][7] != 0 && valor.length > 0 && valor.length < MATRIZ[LOCALIDAD][7]){
			notificationManager( LENGUAJE[0] +"'" + MATRIZ[LOCALIDAD][4] + "' "+LENGUAJE[2]+" " + MATRIZ[LOCALIDAD][7] + " " +LENGUAJE[4], MATRIZ[LOCALIDAD][9], MATRIZ[LOCALIDAD][10]);
            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".style.backgroundColor = '#F6CECE'");
			eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".focus()");
            return false;
        }else{
            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".style.backgroundColor = '#CCCCCC'");
        }
        
        if(MATRIZ[LOCALIDAD][8] != 0 && valor.length > 0 && valor.length > MATRIZ[LOCALIDAD][8]){
            notificationManager(LENGUAJE[0]+"'" + MATRIZ[LOCALIDAD][4] + "' "+LENGUAJE[3]+" " + MATRIZ[LOCALIDAD][8] + " " +LENGUAJE[4], MATRIZ[LOCALIDAD][9], MATRIZ[LOCALIDAD][10]);
            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".style.backgroundColor = '#F6CECE'");
			eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".focus()");
            return false;
        }else{
            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".style.backgroundColor = '#CCCCCC'");
        }
        
	if (MATRIZ[LOCALIDAD][3](valor)){
		if(MATRIZ[LOCALIDAD][2] && valor.length <= 0 ){
			notificationManager(LENGUAJE[0]+"'" + MATRIZ[LOCALIDAD][4] + "' "+LENGUAJE[5], MATRIZ[LOCALIDAD][9], MATRIZ[LOCALIDAD][10]);
			eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".style.backgroundColor = '#F6CECE'");
			eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".focus()");
			return false;
		}else{
                    if(MATRIZ[LOCALIDAD][5] != -1){
                        var LOCALIDAD2 = MATRIZ[LOCALIDAD][5];
                        var valor2 = F_TRIM(eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD2][1] + ".value"));
                        if(valor != valor2){
							notificationManager(LENGUAJE[0]+"'" + MATRIZ[LOCALIDAD][4] + "' "+LENGUAJE[8]+" '" + MATRIZ[LOCALIDAD2][4]+ "' "+LENGUAJE[6], MATRIZ[LOCALIDAD][9], MATRIZ[LOCALIDAD][10]);
                            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] +  ".style.backgroundColor = '#F6CECE'");
                            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD2][1] + ".style.backgroundColor = '#F6CECE'");
							eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".focus()");
                            return false;
                        }else{
                            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] +  ".style.backgroundColor = '#CCCCCC'");
                            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD2][1] + ".style.backgroundColor = '#CCCCCC'");
                            hideNotificationManager(MATRIZ[LOCALIDAD][9], MATRIZ[LOCALIDAD][10]);
							return true;
                        }
                    }else if(MATRIZ[LOCALIDAD][6] != -1){
                        var LOCALIDAD2 = MATRIZ[LOCALIDAD][6];
                        var valor2 = F_TRIM(eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD2][1] + ".value"));
                        if(valor.length > 0 || valor2.length > 0){
                            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] +  ".style.backgroundColor = '#CCCCCC'");
                            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD2][1] + ".style.backgroundColor = '#CCCCCC'");
							hideNotificationManager(MATRIZ[LOCALIDAD][9], MATRIZ[LOCALIDAD][10]);
                            return true;
                        }else{
							notificationManager(LENGUAJE[0]+"'" + MATRIZ[LOCALIDAD][4] + "' "+LENGUAJE[7]+" '" + MATRIZ[LOCALIDAD2][4]+ "' "+LENGUAJE[5], MATRIZ[LOCALIDAD][9], MATRIZ[LOCALIDAD][10]);
                            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] +  ".style.backgroundColor = '#F6CECE'");
                            eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD2][1] + ".style.backgroundColor = '#F6CECE'");
							eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".focus()");
                            return false;
                        }
                    }else{
                        eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".style.backgroundColor = '#CCCCCC'");
						hideNotificationManager(MATRIZ[LOCALIDAD][9], MATRIZ[LOCALIDAD][10]);
						return true;
                    }
		}
	}else{
		notificationManager(LENGUAJE[0]+"'" + MATRIZ[LOCALIDAD][4] + "' "+LENGUAJE[9], MATRIZ[LOCALIDAD][9], MATRIZ[LOCALIDAD][10]);
        eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] +  ".style.backgroundColor = '#F6CECE'");
		eval("document." + MATRIZ[LOCALIDAD][0] + "." + MATRIZ[LOCALIDAD][1] + ".focus()");
		return false;
	}
}
	
function valida_info_completa(MATRIZ, LENGUAJE){
	for(k = 0 ; k < MATRIZ.length; k++){
		if(!valida_info_individual(MATRIZ, k, LENGUAJE)){
			return false;
		}
	}
	eval("document." + MATRIZ[0][0] + ".submit()");		
}

function valida_info_completa_function(MATRIZ, LENGUAJE, FUNCTION, PARAMETERS){
	for(k = 0 ; k < MATRIZ.length; k++){
		if(!valida_info_individual(MATRIZ, k, LENGUAJE)){
			return false;
		}
	}
	FUNCTION(eval(PARAMETERS));
}


/*
*	RUTINAS MANEJO DE NOTIFICACIONES Y ERRORES
*	notificationManager
*	hideNotificationManager
*/


function notificationManager(STRNOTIFICATION, DIV, JQUERY){
/*
	STRNOTIFICATION		String que contiene la descripcion del error.
	DIV   			 	ID de la division que se va a prender si es que existe un error, null si no se quiere ese efecto.
	JQUERY				Boolean que contiene si mostrar un mensaje de error con effecto
						$().fadeIn() - $().fadeOut() de jQuery o si applicar la propiedad CSS display: block 
*/
	var html = "<h1>Hey!</h1><br><p>"+STRNOTIFICATION+"</p>";
	if(DIV == null && !JQUERY){
		//No quieres nada fancy? alert
		window.alert(STRNOTIFICATION);
	}else if(DIV == null && JQUERY){
		//Despliego con Jquery && FaceBox
		jQuery.facebox(html);
	}else if(DIV != null && JQUERY){
		/*
			Despliego con Jquery
				El DIV necesita tener la propiedad CSS: display:none;
		*/
		document.getElementById(DIV).innerHTML = html;
		$("#"+DIV).fadeIn("slow");
		$("#"+DIV).click(function () {
			$("#"+DIV).fadeOut("slow");
		});
	}else if(DIV != null && !JQUERY){
		/*
			Despliego solamente el DIV con CSS
				El DIV necesita tener la propiedad CSS: display:none;
		*/
		document.getElementById(DIV).innerHTML = html;
		document.getElementById(DIV).style.display = "block";
	}else{
		//Ninguna opcion? alert
		window.alert(STRNOTIFICATION);
	}
}

function hideNotificationManager(DIV, JQUERY){
/*
	DIV			Si esta null no hace nada, sino applica una accion
	JQUERY		Boolean que contiene si ocultar con effecto Effect.Fade de Script.aculo.us
					o si applicar la propiedad CSS display: none
*/
	if(DIV == null && !JQUERY){
		// DO NOTHING
	}else if(DIV == null && JQUERY){
		//Quito la ventanita con Jquery && FaceBox
		jQuery(document).trigger('close.facebox');
	}else if(DIV != null && JQUERY && (document.getElementById(DIV).style.display != "none")){
		/*
			Quito con Jquery
				El DIV necesita tener la propiedad CSS: display:block;
		*/
		$("#"+DIV).fadeOut("slow");
	}else if(DIV != null && !JQUERY){
		/*
			Quito solamente el DIV con CSS
				El DIV necesita tener la propiedad CSS: display:block;
		*/
		document.getElementById(DIV).style.display = "none";
	}else{
		//Ninguna opcion? DO NOTHING
	}
}


/*
*	RUTINAS DE MANEJO DE STRINGS
*	F_LTRIM
*	F_RTRIM
*	F_TRIM
*	quitaAcentos
*	quitaEspeciales
*	cleanStringUrl
*/

function F_LTRIM(K_adena) {
   return K_adena.replace(/^\s+/, "");
}

function F_RTRIM(K_adena) {
   return K_adena.replace(/\s+$/, "");
}

function F_TRIM(K_adena) {
   return F_LTRIM(F_RTRIM(K_adena));
}

function quitaAcentos($string){
	$search 	= explode(",","�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�");
	$replace 	= explode(",","a,e,i,o,u,n,A,E,I,O,P,N,a,e,i,o,u,A,E,I,O,U");
	return str_replace($search, $replace, $string); 
}
function quitaEspeciales($string){
	$search 	= explode(",","!,�,$,%,&,/,(,),=,?,�,^,*,�,�,_,:,;,>,',�,`,+,�,�,<,|,#,�,�");
	$replace 	= explode(",","-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-,-");
	return str_replace($search, $replace, $string); 
}

function cleanStringUrl(cadena){
	cadena = cadena.toLowerCase();
	cadena = F_TRIM(cadena);
	cadena = quitaEspeciales(cadena);
	cadena = cadena.replace('#([^.a-z0-9]+)#i', '-');
	cadena = cadena.replace('#-{2,}#','-');
    cadena = cadena.replace('#-$#','');
    cadena = cadena.replace('#^-#','');
	return cadena;
}

/*
*	RUTINAS DE VALIDACION DE CAMPOS
*	val_MAIL
*	val_INPUT
*	val_TEXTAREA
*	val_FILE
*/


function val_MAIL(txt){
    //  Caracteres NECESARIOS @.
    //  Condiciones: Solo se puede escribir 1na Arroba, y varios puntos	
	if(txt == "")
		return true;	
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
	return emailPattern.test(txt);
}

function val_INPUT(txt){
	return true;
}

function val_TEXTAREA(txt){
	return true;
}

function val_FILE(archivo){ //JPG, JPEG, SWF, PNG
	if(archivo =="")
		return true;
	var extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase(); 
	if(extension!=".jpg" || extension!=".jpeg" || extension!=".swf" || extension!=".png")
		return false;
	return true;
}

/*
*	RUTINAS VARIAS
*
*  	MD5 (Message-Digest Algorithm) - http://www.webtoolkit.info/
*
**/
 
var MD5 = function (string) {
 
	function RotateLeft(lValue, iShiftBits) {
		return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
	}
 
	function AddUnsigned(lX,lY) {
		var lX4,lY4,lX8,lY8,lResult;
		lX8 = (lX & 0x80000000);
		lY8 = (lY & 0x80000000);
		lX4 = (lX & 0x40000000);
		lY4 = (lY & 0x40000000);
		lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
		if (lX4 & lY4) {
			return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
		}
		if (lX4 | lY4) {
			if (lResult & 0x40000000) {
				return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
			} else {
				return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
			}
		} else {
			return (lResult ^ lX8 ^ lY8);
		}
 	}
 
 	function F(x,y,z) { return (x & y) | ((~x) & z); }
 	function G(x,y,z) { return (x & z) | (y & (~z)); }
 	function H(x,y,z) { return (x ^ y ^ z); }
	function I(x,y,z) { return (y ^ (x | (~z))); }
 
	function FF(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function GG(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function HH(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function II(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};
 
	function ConvertToWordArray(string) {
		var lWordCount;
		var lMessageLength = string.length;
		var lNumberOfWords_temp1=lMessageLength + 8;
		var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
		var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
		var lWordArray=Array(lNumberOfWords-1);
		var lBytePosition = 0;
		var lByteCount = 0;
		while ( lByteCount < lMessageLength ) {
			lWordCount = (lByteCount-(lByteCount % 4))/4;
			lBytePosition = (lByteCount % 4)*8;
			lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
			lByteCount++;
		}
		lWordCount = (lByteCount-(lByteCount % 4))/4;
		lBytePosition = (lByteCount % 4)*8;
		lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
		lWordArray[lNumberOfWords-2] = lMessageLength<<3;
		lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
		return lWordArray;
	};
 
	function WordToHex(lValue) {
		var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
		for (lCount = 0;lCount<=3;lCount++) {
			lByte = (lValue>>>(lCount*8)) & 255;
			WordToHexValue_temp = "0" + lByte.toString(16);
			WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
		}
		return WordToHexValue;
	};
 
	function Utf8Encode(string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	};
 
	var x=Array();
	var k,AA,BB,CC,DD,a,b,c,d;
	var S11=7, S12=12, S13=17, S14=22;
	var S21=5, S22=9 , S23=14, S24=20;
	var S31=4, S32=11, S33=16, S34=23;
	var S41=6, S42=10, S43=15, S44=21;
 
	string = Utf8Encode(string);
 
	x = ConvertToWordArray(string);
 
	a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;
 
	for (k=0;k<x.length;k+=16) {
		AA=a; BB=b; CC=c; DD=d;
		a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
		d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
		c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
		b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
		a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
		d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
		c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
		b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
		a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
		d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
		c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
		b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
		a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
		d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
		c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
		b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
		a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
		d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
		c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
		b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
		a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
		d=GG(d,a,b,c,x[k+10],S22,0x2441453);
		c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
		b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
		a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
		d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
		c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
		b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
		a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
		d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
		c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
		b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
		a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
		d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
		c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
		b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
		a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
		d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
		c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
		b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
		a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
		d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
		c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
		b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
		a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
		d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
		c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
		b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
		a=II(a,b,c,d,x[k+0], S41,0xF4292244);
		d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
		c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
		b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
		a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
		d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
		c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
		b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
		a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
		d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
		c=II(c,d,a,b,x[k+6], S43,0xA3014314);
		b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
		a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
		d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
		c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
		b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
		a=AddUnsigned(a,AA);
		b=AddUnsigned(b,BB);
		c=AddUnsigned(c,CC);
		d=AddUnsigned(d,DD);
	}
 
	var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);
 
	return temp.toLowerCase();
}