var variables = [];
var base_url  = 'http://enfant.com.mx/';
/**********************************************************
    FUNCIONES PARA MOSTRAR TWITTER
**********************************************************/
function display_twitter(api_params, document_target){
	//api_params      -> STRING -CONTIENE LA URL CON LOS PARAMETROS PARA TWITTER
	//document_target -> STRING -CONTIENE EL SELECTOR (CSS) DONDE CARGAREMOS EL RESULTADO DE BUSQUEDA
	
	$.ajax({
		url         : 'https://api.twitter.com/1.1/statuses/user_timeline.json?'+api_params,
		type        : 'POST /oauth2/token HTTP/1.1',
		 headers: {
		"Host": "api.twitter.com",
		"User-Agent": "My Twitter App v1.0.23",
		"Authorization": "Basic eHZ6MWV2RlM0d0VFUFRHRUZQSEJvZzpMOHFxOVBaeVJnNmllS0dFS2hab2xHQzB2SldMdzhpRUo4OERSZHlPZw==",
		"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
		"Content-Length": "29",
		"Accept-Encoding": "gzip",
		"grant_type":"client_credentials"
    	},
		dataType    : 'json',
		corssDomain : true,
		data        : {}
	}).done(function (json) {
		console.log(json);
	})
}